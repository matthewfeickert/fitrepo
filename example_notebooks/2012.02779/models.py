import sys
sys.path.append('../')
from fitmaker.fitlib.theory import internal_parameter
# full U2^2xU3^3 model (old = on-shell approx for decays)
from fitmaker.theories.SMEFT_fit_full import SMEFT_old as SMEFT_full_old, SMEFT as SMEFT_full

class SMEFT_top(SMEFT_full):
    # top operators only
    @internal_parameter()
    def CcH(self):
        return 0.
    
    @internal_parameter()
    def CsH(self):
        return 0.
    
    @internal_parameter()
    def CtaH(self):
        return 0.

    @internal_parameter()
    def CmuH(self):
        return 0.
        
    @internal_parameter()
    def CbH(self):
        return 0.
    
    @internal_parameter()
    def CHWB(self):
        return 0.

    @internal_parameter()
    def CHD(self):
        return 0.
        
    @internal_parameter()
    def Cll(self):
        return 0.

    @internal_parameter()
    def CHl3(self):
        return 0.

    @internal_parameter()
    def CHl1(self):
        return 0.

    @internal_parameter()
    def CHe(self):
        return 0.

    @internal_parameter()
    def CHq3(self):
        return 0.

    @internal_parameter()
    def CHq1(self):
        return 0.

    @internal_parameter()
    def CHu(self):
        return 0.

    @internal_parameter()
    def CHd(self):
        return 0.

    @internal_parameter()
    def CW(self):
        return 0.

    @internal_parameter()
    def CHbox(self):
        return 0.

    @internal_parameter()
    def CHG(self):
        return 0.

    @internal_parameter()
    def CHW(self):
        return 0.

    @internal_parameter()
    def CHB(self):
        return 0.

    # @internal_parameter()
    # def CG(self):
    #     return 0.
    
    
class SMEFT_hybrid(SMEFT_full):
    '''
    U(3)^5 subset of model + b, t, tau, mu Yukawas.'''
    @internal_parameter()
    def CcH(self):
        return 0.
    
    @internal_parameter()
    def CsH(self):
        return 0.
    
    @internal_parameter()
    def CtG(self):
        return 0.
    
    @internal_parameter()
    def CtB(self):
        return 0.
    
    @internal_parameter()
    def CtW(self):
        return 0.

    @internal_parameter(depends_on=('CHu',))
    def CHt(self):
        return self['CHu']

    @internal_parameter(depends_on=('CHq3',))
    def CHQ3(self):
        return self['CHq3']

    @internal_parameter(depends_on=('CHq1',))
    def CHQ1(self):
        return self['CHq1']

    @internal_parameter()
    def CQq31(self):
        return 0.
        
    @internal_parameter()
    def CQq38(self):
        return 0.
    
    @internal_parameter()
    def CQq18(self):
        return 0.
    
    @internal_parameter()
    def CQu8(self):
        return 0.
    
    @internal_parameter()
    def CQd8(self):
        return 0.
    
    @internal_parameter()
    def Ctq8(self):
        return 0.
    
    @internal_parameter()
    def Ctu8(self):
        return 0.
    
    @internal_parameter()
    def Ctd8(self):
        return 0.
    
class SMEFT_hybrid_OS(SMEFT_full_old):
    '''
    U(3)^5 subset of model + b, t, tau, mu Yukawas.'''
    @internal_parameter()
    def CcH(self):
        return 0.
    
    @internal_parameter()
    def CsH(self):
        return 0.
    
    @internal_parameter()
    def CtG(self):
        return 0.
    
    @internal_parameter()
    def CtB(self):
        return 0.
    
    @internal_parameter()
    def CtW(self):
        return 0.

    @internal_parameter(depends_on=('CHu',))
    def CHt(self):
        return self['CHu']

    @internal_parameter(depends_on=('CHq3',))
    def CHQ3(self):
        return self['CHq3']

    @internal_parameter(depends_on=('CHq1',))
    def CHQ1(self):
        return self['CHq1']

    @internal_parameter()
    def CQq31(self):
        return 0.
        
    @internal_parameter()
    def CQq38(self):
        return 0.
    
    @internal_parameter()
    def CQq18(self):
        return 0.
    
    @internal_parameter()
    def CQu8(self):
        return 0.
    
    @internal_parameter()
    def CQd8(self):
        return 0.
    
    @internal_parameter()
    def Ctq8(self):
        return 0.
    
    @internal_parameter()
    def Ctu8(self):
        return 0.
    
    @internal_parameter()
    def Ctd8(self):
        return 0.
        
class SMEFT_univ(SMEFT_full):
    '''
    U(3)^5 subset of model. Agrees with SMEFT_aEW_MZ_GF_5F_linear counterpart 
    at per-mille level.'''
    @internal_parameter()
    def CcH(self):
        return 0.

    @internal_parameter()
    def CsH(self):
        return 0.

    @internal_parameter()
    def CtaH(self):
        return 0.

    @internal_parameter()
    def CmuH(self):
        return 0.
        
    @internal_parameter()
    def CbH(self):
        return 0.

    @internal_parameter()
    def CtH(self):
        return 0.

    @internal_parameter()
    def CtG(self):
        return 0.
    
    @internal_parameter()
    def CtB(self):
        return 0.
    
    @internal_parameter()
    def CtW(self):
        return 0.

    @internal_parameter(depends_on=('CHu',))
    def CHt(self):
        return self['CHu']

    @internal_parameter(depends_on=('CHq3',))
    def CHQ3(self):
        return self['CHq3']

    @internal_parameter(depends_on=('CHq1',))
    def CHQ1(self):
        return self['CHq1']


    @internal_parameter()
    def CQq31(self):
        return 0.
        
    @internal_parameter()
    def CQq38(self):
        return 0.
    
    @internal_parameter()
    def CQq18(self):
        return 0.
    
    @internal_parameter()
    def CQu8(self):
        return 0.
    
    @internal_parameter()
    def CQd8(self):
        return 0.
    
    @internal_parameter()
    def Ctq8(self):
        return 0.
    
    @internal_parameter()
    def Ctu8(self):
        return 0.
    
    @internal_parameter()
    def Ctd8(self):
        return 0.
    
    
class SMEFT_Higgs_tt4F(SMEFT_full):
    '''
    SMEFT with only operators constrained by Higgs measurements + 4F constrained by ttbar:
    ['CHbox', 'CHG', 'CHW', 'CHB', 'CG', 'CtaH', 'CtH', 'CbH', 'CmuH', 'CtG']
    '''
    @internal_parameter()
    def Cll(self):
        return 0.
        
    @internal_parameter()
    def CHWB(self):
        return 0.
        
    @internal_parameter()
    def CHD(self):
        return 0.
        
    @internal_parameter()
    def CHl3(self):
        return 0.
        
    @internal_parameter()
    def CHl1(self):
        return 0.
        
    @internal_parameter()
    def CHe(self):
        return 0.
        
    @internal_parameter()
    def CHd(self):
        return 0.
        
    @internal_parameter()
    def CHq3(self):
        return 0.
        
    @internal_parameter()
    def CHq1(self):
        return 0.
        
    @internal_parameter()
    def CHu(self):
        return 0.
        
    @internal_parameter()
    def CW(self):
        return 0.

    @internal_parameter()
    def CsH(self):
        return 0.
    
    @internal_parameter()
    def CcH(self):
        return 0.
    
    @internal_parameter()
    def CHQ3(self):
        return 0.
    
    @internal_parameter()
    def CHQ1(self):
        return 0.
    
    @internal_parameter()
    def CHt(self):
        return 0.
    
    @internal_parameter()
    def CtW(self):
        return 0.
    
    @internal_parameter()
    def CtB(self):
        return 0.
    
    @internal_parameter()
    def CQq31(self):
        return 0.

class SMEFT_Higgs(SMEFT_Higgs_tt4F):
    '''
    SMEFT with only operators constrained by Higgs measurements:
    ['CHbox', 'CHG', 'CHW', 'CHB', 'CG', 'CtaH', 'CtH', 'CbH', 'CmuH', 'CtG']
    '''

    @internal_parameter()
    def CQq38(self):
        return 0.
    
    @internal_parameter()
    def CQq18(self):
        return 0.
    
    @internal_parameter()
    def CQu8(self):
        return 0.
    
    @internal_parameter()
    def CQd8(self):
        return 0.
    
    @internal_parameter()
    def Ctq8(self):
        return 0.
    
    @internal_parameter()
    def Ctu8(self):
        return 0.
    
    @internal_parameter()
    def Ctd8(self):
        return 0.
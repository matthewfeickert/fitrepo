from ...fitlib.constants import pi, GeV2pb 
from ...fitlib.theory import internal_parameter, prediction
from ..EFT import EFTBase, EFTPrediction, EFTInternalParameter
from .inputs import SMEFT_MW_MZ_GF
from . import data_path

json_path = data_path+'decays/'

class V_Decays(EFTBase, SMEFT_MW_MZ_GF):
    '''
    SMEFT Warsaw basis for W and Z-boson decays and BRs, using SMEFT_MW_MZ_GF. 
    '''
    # relative impacts only
    # W boson
    ## total width 
    mu_Gam_W = EFTInternalParameter.from_json(json_path+'mu_Gam_W.json')

    ## partial widths
    ### leptonic including taus
    mu_Gam_W_lv = EFTInternalParameter.from_json(json_path+'mu_Gam_W_lv.json')
    ### leptonic excluding taus
    mu_Gam_W_emuv = EFTInternalParameter.from_json(json_path+'mu_Gam_W_emunu.json')
    ### individual leptonic
    mu_Gam_W_ev = EFTInternalParameter.from_json(json_path+'mu_Gam_W_enu.json')
    mu_Gam_W_mv = EFTInternalParameter.from_json(json_path+'mu_Gam_W_munu.json')
    mu_Gam_W_tv = EFTInternalParameter.from_json(json_path+'mu_Gam_W_tanu.json')
    ### hadronic
    mu_Gam_W_qq = EFTInternalParameter.from_json(json_path+'mu_Gam_W_qq.json')

    ## branching ratios
    ### leptonic including taus
    mu_BR_W_lv = mu_Gam_W_lv.divide_by(mu_Gam_W)
    ### leptonic excluding taus
    mu_BR_W_emuv = mu_Gam_W_emuv.divide_by(mu_Gam_W)
    ### individual leptonic
    mu_BR_W_ev = mu_Gam_W_ev.divide_by(mu_Gam_W)
    mu_BR_W_mv = mu_Gam_W_mv.divide_by(mu_Gam_W)
    mu_BR_W_tv = mu_Gam_W_tv.divide_by(mu_Gam_W)
    ### hadronic
    mu_BR_W_qq = mu_Gam_W_qq.divide_by(mu_Gam_W)
    
    # Z boson
    ## total width 
    mu_Gam_Z = EFTInternalParameter.from_json(json_path+'mu_Gam_Z.json')

    ## partial widths
    ### leptonic including taus
    mu_Gam_Z_ll = EFTInternalParameter.from_json(json_path+'mu_Gam_Z_ll.json')
    ### leptonic excluding taus
    mu_Gam_Z_eemumu = EFTInternalParameter.from_json(json_path+'mu_Gam_Z_eemumu.json')
    ### individual leptonic
    mu_Gam_Z_ee = EFTInternalParameter.from_json(json_path+'mu_Gam_Z_ee.json')
    mu_Gam_Z_mumu = EFTInternalParameter.from_json(json_path+'mu_Gam_Z_mumu.json')
    mu_Gam_Z_tata = EFTInternalParameter.from_json(json_path+'mu_Gam_Z_tata.json')
    ### hadronic
    mu_Gam_Z_qq = EFTInternalParameter.from_json(json_path+'mu_Gam_Z_qq.json')
    ### neutrinos
    mu_Gam_Z_vv = EFTInternalParameter.from_json(json_path+'mu_Gam_Z_vv.json')

    ## branching ratios
    ### leptonic including taus
    mu_BR_Z_ll = mu_Gam_Z_ll.divide_by(mu_Gam_Z)
    ### leptonic excluding taus
    mu_BR_Z_eemumu = mu_Gam_Z_eemumu.divide_by(mu_Gam_Z)
    ### individual leptonic
    mu_BR_Z_mumu = mu_Gam_Z_ee.divide_by(mu_Gam_Z)
    mu_BR_Z_mumu = mu_Gam_Z_mumu.divide_by(mu_Gam_Z)
    mu_BR_Z_tata = mu_Gam_Z_tata.divide_by(mu_Gam_Z)
    ### hadronic
    mu_BR_Z_qq = mu_Gam_Z_qq.divide_by(mu_Gam_Z)
    ### neutrinos
    mu_BR_Z_vv = mu_Gam_Z_vv.divide_by(mu_Gam_Z)

class V_Decays_lin(EFTBase, SMEFT_MW_MZ_GF):
    '''
    SMEFT Warsaw basis for W and Z-boson decays and BRs, using SMEFT_MW_MZ_GF, linear only. 
    '''
    # W boson
    ## total width 
    mu_Gam_W = EFTInternalParameter.from_json(json_path+'mu_Gam_W.json', linear_only=True)

    ## partial widths
    ### leptonic including taus
    mu_Gam_W_lv = EFTInternalParameter.from_json(json_path+'mu_Gam_W_lv.json', linear_only=True)
    ### leptonic excluding taus
    mu_Gam_W_emuv = EFTInternalParameter.from_json(json_path+'mu_Gam_W_emunu.json', linear_only=True)
    ### individual leptonic
    mu_Gam_W_ev = EFTInternalParameter.from_json(json_path+'mu_Gam_W_enu.json', linear_only=True)
    mu_Gam_W_mv = EFTInternalParameter.from_json(json_path+'mu_Gam_W_munu.json', linear_only=True)
    mu_Gam_W_tv = EFTInternalParameter.from_json(json_path+'mu_Gam_W_tanu.json', linear_only=True)
    ### hadronic
    mu_Gam_W_qq = EFTInternalParameter.from_json(json_path+'mu_Gam_W_qq.json', linear_only=True)

    ## branching ratios
    ### leptonic including taus
    mu_BR_W_lv = mu_Gam_W_lv.divide_by(mu_Gam_W, linear_only=True)
    ### leptonic excluding taus
    mu_BR_W_emuv = mu_Gam_W_emuv.divide_by(mu_Gam_W, linear_only=True)
    ### individual leptonic
    mu_BR_W_ev = mu_Gam_W_ev.divide_by(mu_Gam_W, linear_only=True)
    mu_BR_W_mv = mu_Gam_W_mv.divide_by(mu_Gam_W, linear_only=True)
    mu_BR_W_tv = mu_Gam_W_tv.divide_by(mu_Gam_W, linear_only=True)
    ### hadronic
    mu_BR_W_qq = mu_Gam_W_qq.divide_by(mu_Gam_W, linear_only=True)
    
    # Z boson
    ## total width 
    mu_Gam_Z = EFTInternalParameter.from_json(json_path+'mu_Gam_Z.json', linear_only=True)

    ## partial widths
    ### leptonic including taus
    mu_Gam_Z_ll = EFTInternalParameter.from_json(json_path+'mu_Gam_Z_ll.json', linear_only=True)
    ### leptonic excluding taus
    mu_Gam_Z_eemumu = EFTInternalParameter.from_json(json_path+'mu_Gam_Z_eemumu.json', linear_only=True)
    ### individual leptonic
    mu_Gam_Z_ee = EFTInternalParameter.from_json(json_path+'mu_Gam_Z_ee.json', linear_only=True)
    mu_Gam_Z_mumu = EFTInternalParameter.from_json(json_path+'mu_Gam_Z_mumu.json', linear_only=True)
    mu_Gam_Z_tata = EFTInternalParameter.from_json(json_path+'mu_Gam_Z_tata.json', linear_only=True)
    ### hadronic
    mu_Gam_Z_qq = EFTInternalParameter.from_json(json_path+'mu_Gam_Z_qq.json', linear_only=True)
    ### neutrinos
    mu_Gam_Z_vv = EFTInternalParameter.from_json(json_path+'mu_Gam_Z_vv.json', linear_only=True)

    # branching ratios
    ### leptonic including taus
    mu_BR_Z_ll = mu_Gam_Z_ll.divide_by(mu_Gam_Z, linear_only=True)
    ### leptonic excluding taus
    mu_BR_Z_eemumu = mu_Gam_Z_eemumu.divide_by(mu_Gam_Z, linear_only=True)
    ### individual leptonic
    mu_BR_Z_ee = mu_Gam_Z_ee.divide_by(mu_Gam_Z, linear_only=True)
    mu_BR_Z_mumu = mu_Gam_Z_mumu.divide_by(mu_Gam_Z, linear_only=True)
    mu_BR_Z_tata = mu_Gam_Z_tata.divide_by(mu_Gam_Z, linear_only=True)
    ### hadronic
    mu_BR_Z_qq = mu_Gam_Z_qq.divide_by(mu_Gam_Z, linear_only=True)
    ### neutrinos
    mu_BR_Z_vv = mu_Gam_Z_vv.divide_by(mu_Gam_Z, linear_only=True)
    
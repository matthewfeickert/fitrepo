from ..SM.MW_MZ_GF_5F.inputs import MW_MZ_GF_5F_Fixed
from ...fitlib.theory import internal_parameter

class SMEFT_MW_MZ_GF(MW_MZ_GF_5F_Fixed):
    external_parameters = [
      # Higgs kinetic & T-param
      'cdp', 'cpdc', 
      # gauge-Higgs
      'cpWB', 'cpBB', 'cpW', 'cpG', 
      # quark-current
      'cpq3i', 'cpqMi', 'cpu', 'cpd',
      'cpQ3', 'cpQM', 'cpt',
      # lepton-current
      'c3pl1', 'c3pl2', 'c3pl3',
      'cpl1', 'cpl2', 'cpl3',
      'cpe', 'cpmu', 'cpta',
      # TGC
      'cWWW',
      # GFermi
      'cll'
    ]

class SMEFT_MW_MZ_GF_U2_2_U3_3(SMEFT_MW_MZ_GF):
    external_parameters = [
      'c3pl', 'cpl', 'cpei'
    ]
    
    @internal_parameter(depends_on=('c3pl', ))
    def c3pl1(self):
        return self['c3pl']

    @internal_parameter(depends_on=('c3pl', ))
    def c3pl2(self):
        return self['c3pl']

    @internal_parameter(depends_on=('c3pl', ))
    def c3pl3(self):
        return self['c3pl']

    @internal_parameter(depends_on=('cpl', ))
    def cpl1(self):
        return self['cpl']
    
    @internal_parameter(depends_on=('cpl', ))
    def cpl2(self):
        return self['cpl']
    
    @internal_parameter(depends_on=('cpl', ))
    def cpl3(self):
        return self['cpl']
        
    @internal_parameter(depends_on=('cpei', ))
    def cpe(self):
        return self['cpei']
        
    @internal_parameter(depends_on=('cpei', ))
    def cpmu(self):
        return self['cpei']        

    @internal_parameter(depends_on=('cpei', ))
    def cpta(self):
        return self['cpei']

class SMEFT_MW_MZ_GF_U3_5(SMEFT_MW_MZ_GF_U2_2_U3_3):
    
    # U(3)^5
    @internal_parameter(depends_on=('cpq3i', ))
    def cpQ3(self):
        return self['cpq3i']    

    @internal_parameter(depends_on=('cpqMi', ))
    def cpQM(self):
        return self['cpqMi']  
    
    @internal_parameter(depends_on=('cpu', ))
    def cpt(self):
        return self['cpu']
        
        
class MW_MZ_GF_5F_U2_2_U3_3(MW_MZ_GF_5F_Fixed):
    '''
    Conventional Warsaw basis.
    '''
    external_parameters = [
      # Higgs kinetic & T-param
      'CHbox', 'CHD', 
      # gauge-Higgs
      'CHWB', 'CHB', 'CHW', 'CHG',
      # flavor diagonal lepton currents
      'CHl3', 'CHl1', 'CHe',       
      # flavor universal down quark currents
      'CHd',   
      # currents for 1st 2 gens.
      'CHq3', 'CHq1', 'CHu', 
      # currents for 3rd gen.
      'CHQ3', 'CHQ1', 'CHt',
      # # 3rd gen. Yukawas
      # 'CtaH', 'CtH', 'CbH',
      # # 2nd gen. Yukawas
      # 'CmuH', 'CcH', 'CsH',
      # # top-specific dipoles
      # 'CtG', 'CtW', 'CtB',
      # TGC
      'CW', #'CG',
      'Cll'
      #  # interfering two-heavy-two-light 4-quark operators
      # 'CQq38', 'CQq18', 'CQu8', 'CQd8', 'Ctq8', 'Ctu8', 'Ctd8',
      # 'CQq31',
      #  # interfering four-heavy 4-quark operators
      # 'CQQ8', 'CQt8', 'CQb8', 'CQQ1', 'CQt1', 'CQb1', 'Ctb8', 'Ctt'
    ]


class SMEFT_MW_MZ_GF_WB(MW_MZ_GF_5F_U2_2_U3_3, SMEFT_MW_MZ_GF_U2_2_U3_3):

    @internal_parameter(depends_on=('cdp', ))
    def CHbox(self):
        return self['cdp']

    @internal_parameter(depends_on=('cpdc', ))
    def CHD(self):
        return self['cpdc']

    @internal_parameter(depends_on=('cpWB', ))
    def CHWB(self):
        return self['cpWB']

    @internal_parameter(depends_on=('cpBB', ))
    def CHB(self):
        return self['cpBB']

    @internal_parameter(depends_on=('cpW', ))
    def CHW(self):
        return self['cpW']

    @internal_parameter(depends_on=('cpW', ))
    def CHG(self):
        return self['cpG']
        
    @internal_parameter(depends_on=('cpq3i', ))
    def CHq3(self):
        return self['cpq3i']

    @internal_parameter(depends_on=('cpq3i', 'cpqMi'))
    def CHq1(self):
        return self['cpq3i'] + self['cpqMi']

    @internal_parameter(depends_on=('cpu', ))
    def CHu(self):
        return self['cpu']

    @internal_parameter(depends_on=('cpd', ))
    def CHd(self):
        return self['cpd']

    @internal_parameter(depends_on=('c3pl', ))
    def CHl3(self):
        return self['c3pl']

    @internal_parameter(depends_on=('cpl', ))
    def CHl1(self):
        return self['cpl']

    @internal_parameter(depends_on=('cpei', ))
    def CHe(self):
        return self['cpei']

    @internal_parameter(depends_on=('cpQ3', ))
    def CHQ3(self):
        return self['cpQ3']

    @internal_parameter(depends_on=('cpQ3', 'cpQM'))
    def CHQ1(self):
        return self['cpQ3'] + self['cpQM']

    @internal_parameter(depends_on=('cpt', ))
    def CHt(self):
        return self['cpt']
        
    @internal_parameter(depends_on=('cWWW', ))
    def CW(self): # I think there's  aminus here
        return -self['cWWW']

    @internal_parameter(depends_on=('cll', ))
    def Cll(self):
        return self['cll']
    
"""
``fitmaker.theories.EFT``
================================

Core module for EFT theory implementations.

EFT theory base class
---------------------
EFTBase
    Base class for effective field theories.

FMCallable subclasses
---------------------
EFTMethod
    Subclass of `fitmaker.fitlib.core.FMCallable` implementing the special case 
    of functions that can be represented as a quadratic polynomial, as is the 
    case for observables related to amplitudes-squared with a single insertion 
    of, e.g., a dimension-6 operator. Function data is specified as a set of 
    monomial coefficients for the quadratic polynomial. All manner of 
    arithmetic operations are implemented such that one can take sums, products 
    and ratios of EFTMethod objects, returning the corresponding object 
    containing the monomial coefficients of the resulting expression expanded
    to quadratic order.
EFTInternalParameter
    Subclass of EFTMethod, designed to be used as part of `EFTBase` class 
    definitions, in defining internal parameters.
EFTPrediction
    Subclass of EFTMethod, designed to be used as part of `EFTBase` class 
    definitions, in defining predictions for obervables.

"""

import sys
import json
import re
import numpy as np
from numpy import sqrt
from ..fitlib import formatting
from ..fitlib.constants import  pi, GeV2pb
from ..fitlib.theory import (TheoryBase, internal_parameter, TheoryPrediction,
  prediction, TheoryPredictionError)
from ..fitlib.core import FMCallable, FMCallableError, FMCallableInitError
################################################################################
# Generic EFT method: callable object for quadratic polynomial function
class EFTMethod(FMCallable):
    """FMCallable subclass for Effective Field Theories.
    
    i.e. theories with a power-counting expansion where predictions have a 
    strict polynomial dependence. The callable is specified by the parameters 
    of a quadratic polynomial in the external or internal parameters of a theory.
    Meant to be used as methods of TheoryBase subclasses, specifically 
    `EFTBase`.
    
    Attributes inherited from fitmaker.core.FMCallable
    --------------------------------------------------
    name : str
        Function name taken from `func`.__name__.
    __name__ : str
        Same as `name`.
    func : Callable
        Callable object wrapped by this class.
    
    Attributes
    ----------
    _name : str
        Name of method.
    _doc : str
        Docstring for method.
    _params : list
        List of parameters on which the (polynomial) function depends. 
    _nparams : int
       Number of parameters on which the (polynomial) function depends. 
    _shape : tuple
        Shape of the `numpy.ndarray` returned by `func`. Inferred from the 
        shapes of `_constant`, `_linear` and `_quadratic`, with consistency 
        checks in place.
    _constant : numpy.array or float
        Constant monomial coefficient of the quadratic polynomial returned by 
        `func`.
    _linear : numpy.array
        Linear monomial coefficients of the quadratic polynomial returned by 
        `func`. 1D array (vector) ordered according to `_params`.
    _quadratic : numpy.array
        Quadratic monomial coefficients of the quadratic polynomial returned by 
        `func`. Square 2D array (matrix) ordered according to `_params`.
    _lambda_gen : float
        EFT cutoff value, 'Lambda', assumed in the definition of the monomial 
        coefficients. Applied specifically assuming dimension-6 operators, i.e. 
        the linear terms scale like 1/`_lambda_gen` and the quadratic terms 
        scale like 1/`_lambda_gen`**2. Allows for the use of different values 
        of 'Lambda'.
    _divide : numpy.array or float
        Value to rescale return value of `func` by must have the same shape as 
        the former.
    _fields : dict
        Dictionary representation of the relevant data to feed to the 
        constructor. Useful for arithmetic operation methods.
    
    """
    def __init__( self, name=None, doc='', params=[], constant=None, linear=None,
                  quadratic=None, lambda_gen=1., divide=1., **kwargs ):
        """
        Parameters
        ----------
        name : str, default=None
            Name of method.
        doc : str, default=''
            Docstring for method.
        params : list, default=[]
            List of parameters on which the (polynomial) function depends. 
            This parameter is required.
        constant : list or float, default=None
            Constant monomial coefficient of the quadratic polynomial returned 
            by `func`.
        linear : list, default=None
            Linear monomial coefficients of the quadratic polynomial returned by 
            `func`. 1D array (vector) ordered according to `_params`. First 
            dimensions must match the shape of `constant`, last dimension must 
            match length of params. If `constant` is not specified, the shape 
            of `constant` is inferred from the shape of `linear`.
        quadratic : list, default=None
            Quadratic monomial coefficients of the quadratic polynomial returned 
            by `func`. Square 2D array (matrix) ordered according to `_params`. 
            First dimension must match the shape of `constant`, last two 
            dimensions must match length of params. If neither `constant` nor 
            `linear` are specified, their shapes are inferred from the shape of 
            `quadratic`.
        lambda_gen : float, default=1.
            EFT cutoff value, 'Lambda', assumed in the definition of the 
            monomial coefficients. Applied specifically assuming dimension-6 
            operators, i.e. the linear terms scale like 1/`_lambda_gen` and the 
            quadratic terms scale like 1/`_lambda_gen`**2. Allows for the use 
            of different values of 'Lambda'.
        divide : numpy.array or float, default=1.
            Value to rescale return value of `func` by must have the same shape as 
            the former.
        
        Raises
        ------
        EFTMethodInitError
            1) When `params` argument is missing or has an unexpected shape (must 
            be a 1D list).
            2) If any of the data arguments (`constant`, `linear` or 
            `quadratic`) fail the expected shape checks.
            3) If `divide` fails the expected shape check.
        
        Notes
        -----
        Values of None for constant, linear & quadratic are understood as zero.
                  
        The inner callable, `func` returns a `numpy.array` of the resulting 
        quadratic polynomial described by the input data. As with the 
        FMCallable parent class its argument must be a 
        `fitmaker.fitlib.theory.TheoryBase` subclass, specifically an `EFTBase` 
        subclass. This because the instance must have a `Lambda` attribute, 
        otherwise the function call with raise `EFTMethodError`. Any Exceptions 
        raised when calling `func` will be caught and and `EFTMethodError` will
        be raised instead.
                  
        """
        if not params:
            msg = (
              'Required "params" keyword argument not specified'
            )
            raise EFTMethodInitError(msg)
        self._name = name
        self._doc = doc
        self._params = params
        self._nparams = len(params)

        if params and np.array(params).ndim!=1:
            msg = ('Wrong dimension for "params" keyword argument')
            raise EFTMethodInitError(msg)

        # process numerical arguments and infer shape of function output
        # piority order, for those defined, is: constant > linear > quadratic
        _got_size = False
        if constant is not None:
            constant = self.to_float_array_or_scalar(constant)
            if np.isscalar(constant):
                self._shape = () # not array
            else:
                self._shape = constant.shape # array
            _expected_shape_lin = tuple( list(self._shape)+[self._nparams] )
            _expected_shape_quad = tuple( list(_expected_shape_lin)+[self._nparams] )
            _got_size = True

        if linear is not None:
            linear = self.to_float_array_or_scalar(linear)
            if not _got_size:
                self._shape = linear.shape[:-1] # all but last axis
                _expected_shape_lin = tuple( list(self._shape)+[self._nparams] )
                _expected_shape_quad = tuple( list(_expected_shape_lin)+[self._nparams] )
                _got_size = True
            else:
                if linear.shape != _expected_shape_lin:
                    msg = (
                      'Shape of "linear", {}, does not match expected shape '
                      'inferred from "constant". Expected {}.'
                    ).format( linear.shape, _expected_shape_lin )
                    raise EFTMethodInitError(msg)

        if quadratic is not None:
            quadratic = self.to_float_array_or_scalar(quadratic)
            if not _got_size:
                self._shape = quadratic.shape[:-2] # all but last axis
                _expected_shape_lin = tuple( list(self._shape)+[self._nparams] )
                _expected_shape_quad = tuple( list(_expected_shape_lin)+[self._nparams] )
                _got_size = True
            else:
                if quadratic.shape != _expected_shape_quad:
                    msg = (
                      'Shape of "quadratic", {}, does not match expected shape '
                      'inferred from "constant" or "linear". Expected {}.'
                    ).format( quadratic.shape, _expected_shape_quad )
                    raise EFTMethodInitError(msg)

        if not _got_size:
            raise EFTMethodInitError(
              'None of "constant", "linear" or "quadratic" were specified.'
            )

        # set numerical parameters for function evaluation
        if constant is not None:
            self._constant = constant
        else:
            self._constant = 0. if not self._shape else np.zeros(self._shape)

        self._linear = linear if linear is not None else np.zeros(_expected_shape_lin)
        self._quadratic = quadratic if quadratic is not None else np.zeros(_expected_shape_quad)

        self._lambda_gen = float(lambda_gen)
        self._divide = self.to_float_array_or_scalar(divide)

        if not np.isscalar(self._divide):
            if self._divide.shape != self._shape:
                msg = (
                  'Wrong dimension for "divide" keyword argument.'
                  'must be scalar or have shape {}'
                ).format(self._shape)
                raise EFTMethodInitError(msg)

        # construct observable function
        def func(instance):
            # EFTBase subclass instance as argument
            # c/Lambda^2 array for specified coeffs
            # (default: all external parameters)
            if not params:
                # this is bad when externals get downgraded to internals: will break
                aparams = instance.ext_param_array(*params)
            else:
                aparams = instance.param_array(*params)

            # rescale for instance Lambda value & any gen-level Lambda value
            try:
                Lambda = instance.Lambda
            except AttributeError:
                raise EFTMethodError(
                  'Theory instance has no attribute "Lambda". See EFTBase '
                  'class in fitmaker/theories/EFT.py.'
                )

            aparams *= self._lambda_gen**2/Lambda**2

            result = 0. + self._constant # ensures a copy is made to not modify

            try:
                # linear terms: (c.(linear.T))/Lambda
                result += np.dot(aparams, self._linear.T)
                # quadratic terms: (c.quadratic.c)/Lambda^2
                result += np.dot(np.dot(aparams, self._quadratic), aparams.T)
            except Exception as e:
                # import traceback
                raise EFTMethodError(e)

            result/=divide # divide by something

            if not np.isscalar(result) and len(result) == 1:
                return result.item()
            else:
                return result

        if name is not None:
            func.__name__ = str(name)

        func.__doc__ = doc

        # keep record of init fields, casting numpy arrays to lists
        self._fields = kwargs
        self._fields.update(
          self.cast_fields({
            'name':name,
            'doc':doc,
            'params':params,
            'constant':constant,
            'linear':linear,
            'quadratic':quadratic,
            'lambda_gen':lambda_gen,
            'divide':divide
          })
        )

        super(EFTMethod, self).__init__(func, **kwargs)

    def data(self):
        """Return string representation of the instantiation data.
        
        Returns
        -------
        str representation of `_fields` attribute.
        
        """
        return str(self._fields)

    @staticmethod
    def to_float_array_or_scalar(obj):
        """Cast numerical data to float and arrays as appropriate.
        
        Parameters
        ----------
        obj : int, float or array-like
            Object to cast.
        
        Returns
        -------
        float or np.array
        
        """
        if np.isscalar(obj):
            return float(obj)
        else:
            return np.asarray(obj).astype(float)

    @staticmethod
    def to_list_or_scalar(obj):
        """Cast array data to lists as appropriate.
        
        Parameters
        ----------
        obj : int, float or array-like
            Object to cast.
        
        Returns
        -------
        float or list
        
        """
        
        if np.isscalar(obj):
            return obj
        else:
            return np.asarray(obj).tolist()

    @classmethod
    def cast_fields(cls, fields):
        """Cast values of a dictionary to native python types. 
        
        Converts using `EFTMethod.to_list_or_scalar`. Anything that can be cast 
        to list is converted and all else remains unchanged. Designed for use 
        on `fields` argument of `EFTMethod` constructor.
        
        Parameters
        ----------
        fields : dict
            Dictionary to cast values of.
        
        Returns
        -------
        dict        
            Dictionary with converted values.
        
        """
        cast_fields = {}

        for k,v in fields.items():
            try:
                cast_fields[k] = cls.to_list_or_scalar(v)
            except ValueError:
                cast_fields[k] = v

        return cast_fields
    
    def set_field(self, fld, val):
        """Set fields data of `EFTMethod` instance, casting values to native 
        python types. 
        
        Converts values using `EFTMethod.to_list_or_scalar`. Anything that can 
        be cast to list is converted and all else remains unchanged. Contents 
        of the `_fields` attribute and correspondingly names class attributes 
        are both updated.
        
        Parameters
        ----------
        fld : str
            Dictionary key
        val
            Value to assign to `key`.
        
        Raises
        ------
        EFTMethodError
            If either `fld` is not found in `_fields` or as a named attribute of 
            `EFTMethod` instance.
            
        """
        if fld in self._fields:
            self._fields[fld] = self.to_list_or_scalar(val)
        else:
            msg = f"Field {fld} not found in {self.__class__} instance."
            raise EFTMethodError(msg)
            
        if hasattr(self,'_'+fld):
            setattr(self, '_'+fld, val)
        else:
            msg = f"Attribute _{fld} not found in {self.__class__} instance."
            raise EFTMethodError(msg)
        
    def get_fields(self, add={}, update={}):
        """
        Return a dict containing an `EFTMethod` instance's `_fields` attribute.
        
        Allows for updating information before returning.
        
        Parameters
        ----------
        add : dict, default={}
            Dictionary specifying keys & values to add. If key is in `_fields`,
            value is appended using the "+" operator, else key, value are
            added to fields.
        update : dict, default={}
            Dictionary specifying key & values to replace using update().
        
        """
        fields = dict(self._fields)
        fields.update(update)
        for k,v in add:
            if k in fields:
                fields[k] += v
            else:
                fields[k] = v

        return fields
    
    def set_significant_figures(self, nsig):
        """Truncate internal data to `nsig` significant figures.
        
        Parameters
        ----------
        nsig : int
            Number of significant figures to truncate data to.
        
        """
        for fld in ['constant', 'linear', 'quadratic']:
            self.set_field(fld, formatting.signif(self._fields[fld], nsig))
                
    def to_dict(self, ignore_keys=[]):
        """Return `EFTMethod` data as a regular python dict.
        
        Parameters
        ----------
        ignore_keys : list
            Any keys to omit in the returned dictionary.
        
        """
        fields = self.get_fields()
        
        for key in ignore_keys:
            fields.pop(key, None)
        
        return fields

        
    def get_json_repr(self, indent=2):
        """Return json string representation of `EFTMethod` data.
        
        Parameters
        ----------
        indent : int, default=2
            Indentation size for `json.dumps`.
        
        Returns
        -------
        str
            Raw json formatted string of `_fields` attribute.
        
        """
        return json.dumps(self.to_dict(), indent=indent)
        
    def to_json(self, indent=2):
        """Convert instance to json string, convert inner arrays to one line.
        
        Parameters
        ----------
        indent : int, default=2
            Indentation size for `json.dumps`.
        
        Returns
        -------
        str
            Pretty json formatted string of `_fields` attribute.
        
        """
        
        jstr = self.get_json_repr(indent=indent)
        return formatting.pretty_json_string(jstr)
    
    def write_json(self, outputfile, indent=2):
        """Convert instance to json string and write to file.
        
        Parameters
        ----------
        outputfile : str
            Path to write json string to.
        
        indent : int, default=2
            Indentation size for `json.dumps`.
        
        """
        with open(outputfile, 'w') as ofile:
            ofile.write( self.to_json(indent=indent) )
        
    @classmethod
    def from_json(cls, json_file, linear_only=False, rename={}):
        """
        Initialize using input data from a json file.
        
        json file should have a basic dict-like structure with keys 
        corresponding to the keyword arguments of `EFTMethod.__init__()`.
        Optional fields can be omitted.
        
        E.g., for a single-valued quantity that depends on n parameters, the 
        content of the json file could be:
        
        {
          "name":"example",
          "doc":"Documentation for example EFTMethod function",
          "params": [ "par_1", "par_2", ..., "par_n" ],
          "constant": 1.0, 
          "linear": [ 1., ...],
          "quadratic": [
            [ 1., ...],
            ...
          ], 
          "lambda_gen": 1000.0
          "divide":1.
        }
        
        For a list-valued quantity of length m: 
        * "constant" should be a list of length m (or unspecified).
        * "linear" would be a list of length n with m-length lists as elements.
        * "quadratic" would be a list of length n with 2D m x m lists as elements.
        
        Parameters
        ----------
        json_file : str
            Path to json file form which to load `EFTMethod` data.
        linear_only : bool, default=False
            Only include data for predictions up to linear order in the 
            quadratic polynomial (EFT expansion).
        rename : dict, default={}
            Dictionary specifying as keys any parameters in 'param' field to 
            rename, and as values, the corresponding name to replace it with. 
            E.g., rename={'MyParam':'MyNewParam'} will replace 'MyParam' in 
            the 'params' data field with 'MyNewParam'.
        
        Returns
        -------
        EFTMethod
            New instance reflecting data fields contained in `json_file`
        
        """
        try:
            with open(json_file, 'r') as jfile:
                fields = json.load(jfile)
        except IOError as e:
            msg =(
            "Unable to initialise EFTMethod from json file. {}"
            ).format(e)
            raise EFTMethodInitError(msg)

        if linear_only:
            fields.pop('quadratic', None)

        if rename:
            fields['params'] = [ rename.get(p,p) for p in fields['params'] ]

        return cls(**fields)

    @classmethod
    def outer_last(cls, arr1, arr2, symmetrise=False):
        """
        Return the outer product of the last axis of two ndarrays, element-wise
        in the other axes.
        
        Arrays must either have same shape or one of the arrays must be 1D with
        shape equal to the last level of the other array.
        
        Parameters
        ----------
        arr1, arr2 - numpy.ndarray
            Both of shape (A,B,..,N) OR one of shape (A,B,..,N) and the other 
            of shape (N,)
        symmetrise : bool, default=False
            Return a symmetrised version of the result.
        
        Returns
        -------
        numpy.ndarray 
            Of shape (A,B,...N,N) where the last two axes are the N x N matrix 
            formed from the outer product of elements in the last axes of
            `arr1` and `arr2`.
        
        Raises
        ------
        ValueError
            If the array shapes are incompatible.
        
        Notes
        -----
        Only implemented for up to 8 dimensional arrays.
        
        """

        if arr1.shape == arr2.shape:
            inds = 'abcdefgh'[:arr1.ndim-1] # up to 8-dimensional!
            sum_str = '{0}i,{0}j->{0}ij'.format(inds)
            result = np.einsum(sum_str, arr1, arr2)

        elif arr1.ndim==1 and len(arr1)==arr2.shape[-1]:
            inds = 'abcdefgh'[:arr2.ndim-1] # up to 8-dimensional!
            sum_str = 'i,{0}j->{0}ij'.format(inds)            
            result = np.einsum(sum_str, arr1, arr2)

        elif arr2.ndim==1 and len(arr2)==arr1.shape[-1]:
            inds = 'abcdefgh'[:arr1.ndim-1] # up to 8-dimensional!
            sum_str = '{0}i,j->{0}ij'.format(inds)
            result = np.einsum(sum_str, arr1, arr2)

        else:
            raise ValueError('two array shapes must match')

        if symmetrise:
            return ( result + cls.outer_last(arr2, arr1) )/2.
        else:
            return result

    @classmethod
    def get_fields_scalar2list(cls, instance, add={}, update={}):
        """
        Return input fields of an instance with scalar return value as a 1D 
        array.
        
        Converts the data for an `EFTMethod` with scalar return value to return 
        a single-element numpy.ndarray.
        
        Parameters
        ----------
        instance : EFTMethod or subclass
            Instance that returns scalar value, checked by requiring `_shape` 
            is null.
        
        Parameters passed to `get_fields()`
        -----------------------------------
        add : dict, default={}
            Dictionary specifying keys & values to add. If key is in `_fields`,
            value is appended using the "+" operator, else key, value are
            added to fields.
        update : dict, default={}
            Dictionary specifying keys & values to replace using update().
        
        Returns
        -------
        fields : dict
            Resulting `EFTMethod` data.
        
        """
        fields = instance.get_fields(add=add, update=update)
        if not instance._shape:
            # only do something if the shape is null

            fields['constant'] = cls.to_list_or_scalar(np.array([instance._constant]))
            fields['linear'] = cls.to_list_or_scalar(instance._linear[np.newaxis,:])
            fields['quadratic'] = cls.to_list_or_scalar(instance._quadratic[np.newaxis,:])

        return fields

    @classmethod
    def as_scalar2list(cls, instance, add_to_fields={}, update_fields={}):
        """
        Initialiser method for a new EFTMethod instance with a scalar return 
        value converted to a 1D array.        
        
        Converts the data for an `EFTMethod` with scalar return value to return 
        a single-element numpy.ndarray. All arguments are passed to 
        `get_fields_scalar2list()`.
        
        Parameters
        ----------
        instance : EFTMethod or subclass
            Instance that returns scalar value, checked by requiring `_shape` 
            is null.
        add_to_fields : dict, default={}
            Dictionary specifying keys & values to add. If key is in `_fields`,
            value is appended using the "+" operator, else key, value are
            added to fields.
        update_fields : dict, default={}
            Dictionary specifying keys & values to replace using update().
        
        Returns
        -------
        EFTMethod
            Resulting `EFTMethod` instance.
        
        """
        fields = cls.get_fields_scalar2list(
          instance,
          add=add_to_fields,
          update=update_fields
        )
        return cls(**fields)

    def scalar2list(self, add_to_fields={}, update_fields={}):
        """Instance method for `as_scalar2list()`.
        
        All arguments are passed to `as_scalar2list()`.
        
        Parameters
        ----------
        add_to_fields : dict, default={}
            Dictionary specifying keys & values to add. If key is in `_fields`,
            value is appended using the "+" operator, else key, value are
            added to fields.
        update_fields : dict, default={}
            Dictionary specifying keys & values to replace using update().
        
        Returns
        -------
        EFTMethod
            Resulting `EFTMethod` instance.
        
        """
        return self.__class__.as_scalar2list(
          self,
          add_to_fields=add_to_fields,
          update_fields=update_fields
        )
        
    ### normalize by 'constant' field
    @classmethod
    def get_fields_normalised_by_constant(
      cls, 
      instance, 
      add={}, 
      update={},
      linear_only=False
    ):
        """
        Return input fields corresponding to an instance with data fields 
        normalised by 'constant' field. 
    
        Special case of scalar constant value treated explicitly, all others 
        use np.newaxis to divide the linear and quadratic fields by the 
        constant field along the first axis. 
        See https://numpy.org/doc/stable/user/basics.broadcasting.html
        
        Parameters
        ----------
        instance : EFTMethod or subclass
            Instance to return normalised data for.
        linear_only : bool, default=False
            Flag to consistently preserve 'linear only' nature of resulting 
            data.
        
        Parameters passed to `get_fields()`
        -----------------------------------
        add : dict, default={}
            Dictionary specifying keys & values to add. If key is in `_fields`,
            value is appended using the "+" operator, else key, value are
            added to fields.
        update : dict, default={}
            Dictionary specifying keys & values to replace using update().
        
        Returns
        -------
        fields : dict
            Resulting `EFTMethod` data.
    
        """
        fields = instance.get_fields(add=add, update=update)

        const = instance._constant
        constant = const/const
        fields['constant'] = cls.to_list_or_scalar(constant)

        lin = instance._linear
        linear = lin/const if np.isscalar(const) else lin/const[:,np.newaxis]
        fields['linear'] = cls.to_list_or_scalar(linear)
        
        if not linear_only:
            quad = instance._quadratic
            quadratic = quad/const if np.isscalar(const) else quad/const[:,np.newaxis,np.newaxis]
            fields['quadratic'] = cls.to_list_or_scalar(quadratic)
        else:
            fields['quadratic'] = None

        return fields

    @classmethod
    def as_normalised_by_constant(
      cls, instance, 
      add_to_fields={}, 
      update_fields={}, 
      linear_only=False
    ):
        """
        Initialiser method for a new EFTMethod instance corresponding to the 
        original instance normalized by its 'constant' field.
    
        All arguments are passed to `get_fields_normalised_by_constant()`.
    
        Parameters
        ----------
        instance : EFTMethod or subclass
            Instance to return normalised data for.
        linear_only : bool, default=False
            Flag to consistently preserve 'linear only' nature of resulting 
            data.
        add_to_fields : dict, default={}
            Dictionary specifying keys & values to add. If key is in `_fields`,
            value is appended using the "+" operator, else key, value are
            added to fields.
        update_fields : dict, default={}
            Dictionary specifying keys & values to replace using update().
        
        Returns
        -------
        EFTMethod
            Resulting `EFTMethod` instance.
    
        """
        fields = cls.get_fields_normalised_by_constant(
          instance,
          add=add_to_fields,
          update=update_fields,
          linear_only=linear_only
        )
        return cls(**fields)

    def normalised_by_constant(
      self, 
      add_to_fields={}, 
      update_fields={}, 
      linear_only=False
    ):
        """Instance method for normalised_by_constant.
    
        All arguments are passed to `as_normalised_by_constant()`.
    
        Parameters
        ----------
        linear_only : bool, default=False
            Flag to consistently preserve 'linear only' nature of resulting 
            data.
        add_to_fields : dict, default={}
            Dictionary specifying keys & values to add. If key is in `_fields`,
            value is appended using the "+" operator, else key, value are
            added to fields.
        update_fields : dict, default={}
            Dictionary specifying keys & values to replace using update().
        
        Returns
        -------
        EFTMethod
            Resulting `EFTMethod` instance.
    
        """
        return self.__class__.as_normalised_by_constant(
          self,
          add_to_fields=add_to_fields,
          update_fields=update_fields,
          linear_only=linear_only
        )

    ### return subset of data
    @classmethod
    def get_fields_subset(
      cls, 
      instance, 
      indices, 
      add={}, 
      update={}, 
      linear_only=False
    ):
        """
        Return input fields corresponding to a subset of the instance 
        return value, specified by indices used to index the arrays. Indices 
        must be a 1D iterable of tuples indicating the positions of the data to 
        include in the result.
        
        Parameters
        ----------
        instance : EFTMethod or subclass
            Instance to return subset data for.
        indices : list of tuples
            Indices of list-valued return value of `EFTMethod` that you want to 
            retain. Tuples are used to index the `numpy.ndarray`s representing 
            the monomial data (`_constant`, `_linear` and `_quadratic`).
        linear_only : bool, default=False
            Flag to consistently preserve 'linear only' nature of resulting 
            data.
        
        Parameters passed to `get_fields()`
        -----------------------------------
        add : dict, default={}
            Dictionary specifying keys & values to add. If key is in `_fields`,
            value is appended using the "+" operator, else key, value are
            added to fields.
        update : dict, default={}
            Dictionary specifying keys & values to replace using update().
        
        Returns
        -------
        fields : dict
            Resulting `EFTMethod` data.
    
        """
        fields = instance.get_fields(add=add, update=update)

        const = instance._constant
        constant = const if np.isscalar(const) else np.array([const[i] for i in indices])
        fields['constant'] = cls.to_list_or_scalar(constant)

        lin = instance._linear
        linear = np.array([lin[i] for i in indices])
        fields['linear'] = cls.to_list_or_scalar(linear)


        if not linear_only:
            quad = instance._quadratic
            quadratic = np.array([quad[i] for i in indices])
            fields['quadratic'] = cls.to_list_or_scalar(quadratic)
        else:
            fields['quadratic'] = None
            
        return fields

    @classmethod
    def as_subset(
      cls, 
      instance, 
      indices, 
      add_to_fields={}, 
      update_fields={}, 
      linear_only=False
    ):
        """
        Initialiser method for a new EFTMethod instance corresponding to the
        subset of another.
    
        All arguments are passed to `get_fields_subset()`.
    
        Parameters
        ----------
        instance : EFTMethod or subclass
            Instance to return subset data for.
        indices : list of tuples
            Indices of list-valued return value of `EFTMethod` that you want to 
            retain. Tuples are used to index the `numpy.ndarray`s representing 
            the monomial data (`_constant`, `_linear` and `_quadratic`).
        linear_only : bool, default=False
            Flag to consistently preserve 'linear only' nature of resulting 
            data.
        add_to_fields : dict, default={}
            Dictionary specifying keys & values to add. If key is in `_fields`,
            value is appended using the "+" operator, else key, value are
            added to fields.
        update_fields : dict, default={}
            Dictionary specifying keys & values to replace using update().
        
        Returns
        -------
        EFTMethod
            Resulting `EFTMethod` instance.
    
        """
        fields = cls.get_fields_subset(
          instance,
          indices,
          add=add_to_fields,
          update=update_fields, 
          linear_only=linear_only
        )
        return cls(**fields)

    def subset(
      self, 
      indices, 
      add_to_fields={}, 
      update_fields={}, 
      linear_only=False
    ):
        """Instance method for subset.
    
        All arguments are passed to `as_subset()`.
    
        Parameters
        ----------
        indices : list of tuples
            Indices of list-valued return value of `EFTMethod` that you want to 
            retain. Tuples are used to index the `numpy.ndarray`s representing 
            the monomial data (`_constant`, `_linear` and `_quadratic`).
        linear_only : bool, default=False
            Flag to consistently preserve 'linear only' nature of resulting 
            data.
        add_to_fields : dict, default={}
            Dictionary specifying keys & values to add. If key is in `_fields`,
            value is appended using the "+" operator, else key, value are
            added to fields.
        update_fields : dict, default={}
            Dictionary specifying keys & values to replace using update().
        
        Returns
        -------
        EFTMethod
            Resulting `EFTMethod` instance.
    
        """
        return self.__class__.as_subset(
          self,
          indices,
          add_to_fields=add_to_fields,
          update_fields=update_fields, 
          linear_only=linear_only
        )

    # arithmetic operations
    ## unary operations
    ### power
    @classmethod
    def get_fields_power(
      cls, 
      instance, 
      n, 
      add={}, 
      update={}, 
      linear_only=False
    ):

        """
        Calculate input data for nth power of instance prediction, expanded to
        the same order as instance (linear or quadratic).
    
        Parameters
        ----------
        instance : EFTMethod or subclass
            Instance to take nth power of.
        n : int
            Exponent.
        linear_only : bool, default=False
            Flag to consistently preserve 'linear only' nature of resulting 
            data.
        
        Parameters passed to `get_fields()`
        -----------------------------------
        add : dict, default={}
            Dictionary specifying keys & values to add. If key is in `_fields`,
            value is appended using the "+" operator, else key, value are
            added to fields.
        update : dict, default={}
            Dictionary specifying keys & values to replace using update().
        
        Returns
        -------
        fields : dict
            Resulting `EFTMethod` data.
    
        """
        fields = instance.get_fields(add=add, update=update)

        const = instance._constant
        # nth power of constant
        constant = const**n
        fields['constant'] = cls.to_list_or_scalar(constant)

        lin = instance._linear
        # linear component of nth power of linear terms
        linear = ( lin.T*const**(n-1)*n ).T
        fields['linear'] = cls.to_list_or_scalar(linear)

        # quadratic component of nth power of linear and quadratic terms
        if not linear_only:
            quad = instance._quadratic
            # element-wise outer product of linear
            linsq = cls.outer_last(lin, lin)
            quadratic = (
              linsq.T*const**(n-2)*(n*(n-1)/2.) +
              quad.T*const**(n-1)*n
            ).T

            fields['quadratic'] = cls.to_list_or_scalar(quadratic)
        else:
            fields['quadratic'] = None

        return fields

    @classmethod
    def as_power(
      cls, 
      instance, 
      n, 
      add_to_fields={}, 
      update_fields={}, 
      linear_only=False
    ):
        """
        Initialiser method for a new EFTMethod instance corresponding to the
        nth power of another.
    
        All arguments are passed to `get_fields_power()`.
    
        Parameters
        ----------
        instance : EFTMethod or subclass
            Instance to take nth power of.
        n : int
            Exponent.
        linear_only : bool, default=False
            Flag to consistently preserve 'linear only' nature of resulting 
            data.
        add_to_fields : dict, default={}
            Dictionary specifying keys & values to add. If key is in `_fields`,
            value is appended using the "+" operator, else key, value are
            added to fields.
        update_fields : dict, default={}
            Dictionary specifying keys & values to replace using update().
        
        Returns
        -------
        EFTMethod
            Resulting `EFTMethod` instance.
    
        """
        fields = cls.get_fields_power(
          instance,
          n,
          add=add_to_fields,
          update=update_fields, 
          linear_only=linear_only
        )
        return cls(**fields)

    def power(
      self, 
      n, 
      add_to_fields={}, 
      update_fields={}, 
      linear_only=False
    ):
        """Instance method for power.
        
        All arguments are passed to `as_power()`.
    
        Parameters
        ----------
        n : int
            Exponent.
        linear_only : bool, default=False
            Flag to consistently preserve 'linear only' nature of resulting 
            data.
        add_to_fields : dict, default={}
            Dictionary specifying keys & values to add. If key is in `_fields`,
            value is appended using the "+" operator, else key, value are
            added to fields.
        update_fields : dict, default={}
            Dictionary specifying keys & values to replace using update().
        
        Returns
        -------
        EFTMethod
            Resulting `EFTMethod` instance.
    
        """
        return self.__class__.as_power(
          self,
          n,
          add_to_fields=add_to_fields,
          update_fields=update_fields, 
          linear_only=linear_only
        )

    ### inverse
    @classmethod
    def get_fields_inverse(
      cls, 
      instance, 
      add={}, 
      update={}, 
      linear_only=False
    ):
        """
        Calculate input data for inverse of instance prediction, expanded to
        the same order as instance (linear or quadratic).
        
        Alias for the special case of `get_fields_power(instance, -1)`.
        
        Parameters
        ----------
        instance : EFTMethod or subclass
            Instance to compute inverse of.
        linear_only : bool, default=False
            Flag to consistently preserve 'linear only' nature of resulting 
            data.
        
        Parameters passed to `get_fields()`
        -----------------------------------
        add : dict, default={}
            Dictionary specifying keys & values to add. If key is in `_fields`,
            value is appended using the "+" operator, else key, value are
            added to fields.
        update : dict, default={}
            Dictionary specifying keys & values to replace using update().
        
        Returns
        -------
        fields : dict
            Resulting `EFTMethod` data.
    
        """
        return cls.get_fields_power(
          instance, 
          -1, 
          add=add, 
          update=update,
          linear_only=linear_only
        )

    @classmethod
    def as_inverse(
      cls, 
      instance, 
      add_to_fields={}, 
      update_fields={},
      linear_only=False
    ):
        """
        Initialiser method for a new EFTMethod instance corresponding to the
        inverse of another.
        
        All arguments are passed to `get_fields_inverse()`.
    
        Parameters
        ----------
        instance : EFTMethod or subclass
            Instance to compute inverse of.
        linear_only : bool, default=False
            Flag to consistently preserve 'linear only' nature of resulting 
            data.
        add_to_fields : dict, default={}
            Dictionary specifying keys & values to add. If key is in `_fields`,
            value is appended using the "+" operator, else key, value are
            added to fields.
        update_fields : dict, default={}
            Dictionary specifying keys & values to replace using update().
        
        Returns
        -------
        EFTMethod
            Resulting `EFTMethod` instance.
    
        """
        fields = cls.get_fields_inverse(
          instance,
          add=add_to_fields,
          update=update_fields,
          linear_only=linear_only
        )
        return cls(**fields)

    def inverse(
      self, 
      add_to_fields={}, 
      update_fields={},
      linear_only=False
    ):
        """Instance method for inverse.
        
        All arguments are passed to `as_inverse()`.
    
        Parameters
        ----------
        linear_only : bool, default=False
            Flag to consistently preserve 'linear only' nature of resulting 
            data.
        add_to_fields : dict, default={}
            Dictionary specifying keys & values to add. If key is in `_fields`,
            value is appended using the "+" operator, else key, value are
            added to fields.
        update_fields : dict, default={}
            Dictionary specifying keys & values to replace using update().
        
        Returns
        -------
        EFTMethod
            Resulting `EFTMethod` instance.
    
        """
        return self.__class__.as_inverse(
          self,
          add_to_fields=add_to_fields,
          update_fields=update_fields,
          linear_only=linear_only
        )

    ### addition of a constant
    @classmethod
    def get_fields_add_float(
      cls, 
      instance, 
      const, 
      add={}, 
      update={},
      linear_only=False
    ):
        """
        Calculate input data for addition of a float to an instance
        prediction.
        
        Parameters
        ----------
        instance : EFTMethod or subclass
            Instance to add `const` to.
        const : np.array or float
            Constant value to add to instance return value. If a `numpy.ndarray`
            is supplied, it must match the shape of `_const` attribute. If it 
            is not, while somehow being consistent with default `numpy` 
            broadcasting rules, it may lead to unwanted behaviour. 
        linear_only : bool, default=False
            Flag to consistently preserve 'linear only' nature of resulting 
            data.
        
        Parameters passed to `get_fields()`
        -----------------------------------
        add : dict, default={}
            Dictionary specifying keys & values to add. If key is in `_fields`,
            value is appended using the "+" operator, else key, value are
            added to fields.
        update : dict, default={}
            Dictionary specifying keys & values to replace using update().
        
        Returns
        -------
        fields : dict
            Resulting `EFTMethod` data.
    
        """
        fields = instance.get_fields(add=add, update=update)

        fields['constant'] = cls.to_list_or_scalar(instance._constant + const)

        return fields

    @classmethod
    def as_add_float(
      cls, 
      instance, 
      const, 
      add_to_fields={}, 
      update_fields={},
      linear_only=False
    ):
        """
        Initialiser method for a new EFTMethod instance corresponding to the
        addition of a constant float.
    
        All arguments are passed to `get_fields_add_float()`.
    
        Parameters
        ----------
        instance : EFTMethod or subclass
            Instance to add `const` to.
        const : np.array or float
            Constant value to add to instance return value. If a `numpy.ndarray`
            is supplied, it must match the shape of `_const` attribute. If it 
            is not, while somehow being consistent with default `numpy` 
            broadcasting rules, it may lead to unwanted behaviour. 
        linear_only : bool, default=False
            Flag to consistently preserve 'linear only' nature of resulting 
            data.
        add_to_fields : dict, default={}
            Dictionary specifying keys & values to add. If key is in `_fields`,
            value is appended using the "+" operator, else key, value are
            added to fields.
        update_fields : dict, default={}
            Dictionary specifying keys & values to replace using update().
        
        Returns
        -------
        EFTMethod
            Resulting `EFTMethod` instance.
    
        """
        fields = cls.get_fields_add_float(
          instance,
          const,
          add=add_to_fields,
          update=update_fields,
          linear_only=linear_only
        )
        return cls(**fields)

    def add_float(
      self, 
      const, 
      add_to_fields={}, 
      update_fields={},
      linear_only=False
    ):
        """Instance method for add_float.
        
        All arguments are passed to `as_add_float()`.
    
        Parameters
        ----------
        const : np.array or float
            Constant value to add to instance return value. If a `numpy.ndarray`
            is supplied, it must match the shape of `_const` attribute. If it 
            is not, while somehow being consistent with default `numpy` 
            broadcasting rules, it may lead to unwanted behaviour. 
        linear_only : bool, default=False
            Flag to consistently preserve 'linear only' nature of resulting 
            data.
        add_to_fields : dict, default={}
            Dictionary specifying keys & values to add. If key is in `_fields`,
            value is appended using the "+" operator, else key, value are
            added to fields.
        update_fields : dict, default={}
            Dictionary specifying keys & values to replace using update().
        
        Returns
        -------
        EFTMethod
            Resulting `EFTMethod` instance.
    
        """
        return self.__class__.as_add_float(
          self,
          const,
          add_to_fields=add_to_fields,
          update_fields=update_fields,
          linear_only=linear_only
        )

    ### multiplication by a constant
    @classmethod
    def get_fields_multiply_float(
      cls, 
      instance, 
      factor, 
      add={}, 
      update={},
      linear_only=False
    ):
        """
        Calculate input data for multiplication by a float of an instance
        prediction.
        
        Parameters
        ----------
        instance : EFTMethod or subclass
            Instance to multiply by `factor`.
        factor : np.array or float
            Constant value to multiply instance return value by. If a 
            `numpy.ndarray` is supplied, it must match the shape of `_const` 
            attribute. If it is not, while somehow being consistent with 
            default `numpy` broadcasting rules, it may lead to unwanted 
            behaviour. 
        linear_only : bool, default=False
            Flag to consistently preserve 'linear only' nature of resulting 
            data.
        
        Parameters passed to `get_fields()`
        -----------------------------------
        add : dict, default={}
            Dictionary specifying keys & values to add. If key is in `_fields`,
            value is appended using the "+" operator, else key, value are
            added to fields.
        update : dict, default={}
            Dictionary specifying keys & values to replace using update().
        
        Returns
        -------
        fields : dict
            Resulting `EFTMethod` data.
    
        """
        fields = instance.get_fields(add=add, update=update)

        fields['constant'] = cls.to_list_or_scalar(factor*instance._constant)
        # transpose to perform product on innermost axis
        # Allows multipliocation by float array of the same shape as 'constant'
        fields['linear'] = cls.to_list_or_scalar((factor*instance._linear.T).T)
        
        if not linear_only:
            fields['quadratic'] = cls.to_list_or_scalar((factor*instance._quadratic.T).T)
        else:
            fields['quadratic'] = None

        return fields

    @classmethod
    def as_multiply_float(
      cls, 
      instance, 
      factor, 
      add_to_fields={}, 
      update_fields={},
      linear_only=False
    ):
        """
        Initialiser method for a new EFTMethod instance corresponding to the
        multiplication of another by a constant float.
    
        All arguments are passed to `get_fields_multiply_float()`.
    
        Parameters
        ----------
        instance : EFTMethod or subclass
            Instance to multiply by `factor`.
        factor : np.array or float
            Constant value to multiply instance return value by. If a 
            `numpy.ndarray` is supplied, it must match the shape of `_const` 
            attribute. If it is not, while somehow being consistent with 
            default `numpy` broadcasting rules, it may lead to unwanted 
            behaviour. 
        linear_only : bool, default=False
            Flag to consistently preserve 'linear only' nature of resulting 
            data.
        add_to_fields : dict, default={}
            Dictionary specifying keys & values to add. If key is in `_fields`,
            value is appended using the "+" operator, else key, value are
            added to fields.
        update_fields : dict, default={}
            Dictionary specifying keys & values to replace using update().
        
        Returns
        -------
        EFTMethod
            Resulting `EFTMethod` instance.
    
        """
        fields = cls.get_fields_multiply_float(
          instance,
          factor,
          add=add_to_fields,
          update=update_fields,
          linear_only=linear_only
        )
        return cls(**fields)

    def multiply_float(
      self, 
      factor, 
      add_to_fields={}, 
      update_fields={},
      linear_only=False
    ):
        """Instance method for multiply_float.
    
        All arguments are passed to `as_multiply_float()`.
    
        Parameters
        ----------
        factor : np.array or float
            Constant value to multiply instance return value by. If a 
            `numpy.ndarray` is supplied, it must match the shape of `_const` 
            attribute. If it is not, while somehow being consistent with 
            default `numpy` broadcasting rules, it may lead to unwanted 
            behaviour. 
        linear_only : bool, default=False
            Flag to consistently preserve 'linear only' nature of resulting 
            data.
        add_to_fields : dict, default={}
            Dictionary specifying keys & values to add. If key is in `_fields`,
            value is appended using the "+" operator, else key, value are
            added to fields.
        update_fields : dict, default={}
            Dictionary specifying keys & values to replace using update().
        
        Returns
        -------
        EFTMethod
            Resulting `EFTMethod` instance.
    
        """
        return self.__class__.as_multiply_float(
          self,
          factor,
          add_to_fields=add_to_fields,
          update_fields=update_fields,
          linear_only=linear_only
        )

    ### division by a constant
    @classmethod
    def get_fields_divide_float(
      cls, 
      instance, 
      factor, 
      add={}, 
      update={},
      linear_only=False
    ):
        """
        Calculate input data for division by a float of an instance
        prediction.
        
        Alias for special case of `get_fields_multiply_float(instance,1./factor)`
        
        Parameters
        ----------
        instance : EFTMethod or subclass
            Instance to divide by `factor`.
        factor : np.array or float
            Constant value to divide instance return value by. If a 
            `numpy.ndarray` is supplied, it must match the shape of `_const` 
            attribute. If it is not, while somehow being consistent with 
            default `numpy` broadcasting rules, it may lead to unwanted 
            behaviour. 
        linear_only : bool, default=False
            Flag to consistently preserve 'linear only' nature of resulting 
            data.
        
        Parameters passed to `get_fields()`
        -----------------------------------
        add : dict, default={}
            Dictionary specifying keys & values to add. If key is in `_fields`,
            value is appended using the "+" operator, else key, value are
            added to fields.
        update : dict, default={}
            Dictionary specifying keys & values to replace using update().
        
        Returns
        -------
        fields : dict
            Resulting `EFTMethod` data.
    
        """
        return cls.get_fields_multiply_float(
          instance, 
          1./factor, 
          add=add, 
          update=update,
          linear_only=linear_only
        )

    @classmethod
    def as_divide_float(
      cls, 
      instance, 
      factor, 
      add_to_fields={}, 
      update_fields={},
      linear_only=False
    ):
        """
        Initialiser method for a new EFTMethod instance corresponding to the
        division of another by a constant float.
        
        All arguments are passed to `get_fields_divide_float()`.
    
        Parameters
        ----------
        instance : EFTMethod or subclass
            Instance to divide by `factor`.
        factor : np.array or float
            Constant value to divide instance return value by. If a 
            `numpy.ndarray` is supplied, it must match the shape of `_const` 
            attribute. If it is not, while somehow being consistent with 
            default `numpy` broadcasting rules, it may lead to unwanted 
            behaviour. 
        linear_only : bool, default=False
            Flag to consistently preserve 'linear only' nature of resulting 
            data.
        add_to_fields : dict, default={}
            Dictionary specifying keys & values to add. If key is in `_fields`,
            value is appended using the "+" operator, else key, value are
            added to fields.
        update_fields : dict, default={}
            Dictionary specifying keys & values to replace using update().
        
        Returns
        -------
        EFTMethod
            Resulting `EFTMethod` instance.
    
        """
        fields = cls.get_fields_divide_float(
          instance,
          factor,
          add=add_to_fields,
          update=update_fields,
          linear_only=linear_only
        )
        return cls(**fields)

    def divide_float(
      self, 
      factor, 
      add_to_fields={}, 
      update_fields={},
      linear_only=False
    ):
        """Instance method for divide_float.
        
        All arguments are passed to `as_divide_float()`.
    
        Parameters
        ----------
        factor : np.array or float
            Constant value to divide instance return value by. If a 
            `numpy.ndarray` is supplied, it must match the shape of `_const` 
            attribute. If it is not, while somehow being consistent with 
            default `numpy` broadcasting rules, it may lead to unwanted 
            behaviour. 
        linear_only : bool, default=False
            Flag to consistently preserve 'linear only' nature of resulting 
            data.
        add_to_fields : dict, default={}
            Dictionary specifying keys & values to add. If key is in `_fields`,
            value is appended using the "+" operator, else key, value are
            added to fields.
        update_fields : dict, default={}
            Dictionary specifying keys & values to replace using update().
        
        Returns
        -------
        EFTMethod
            Resulting `EFTMethod` instance.
    
        """
        return self.__class__.as_divide_float(
          self,
          factor,
          add_to_fields=add_to_fields,
          update_fields=update_fields,
          linear_only=linear_only
        )

    # Reshaping param dependence data
    @classmethod
    def get_fields_newparams(
      cls, 
      instance, 
      newparams, 
      add={}, 
      update={},
      linear_only=False
    ):
        """
        Return input data for instance as a function of a different parameter
        list, newparams. 
    
        Good for subsets, reordering of original params and to include new 
        parameters with null dependence for composing with other instances in 
        binary arithmetic operations.
        
        Parameters
        ----------
        instance : EFTMethod or subclass
            Instance to restructure data for, based on `newparams` parameter 
            list.
        newparams : list of str
            List of parameter names to generate restructures `EFTMethod` data 
            for. For any elements of `newparams` that are present as parameters 
            of the instance, the polynomial information is propagated, 
            accounting for any reorderings. For any elements of `newparams` 
            that are not present as parameters of the instance a null 
            dependence is inserted.
        linear_only : bool, default=False
            Flag to consistently preserve 'linear only' nature of resulting 
            data.
        
        Parameters passed to `get_fields()`
        -----------------------------------
        add : dict, default={}
            Dictionary specifying keys & values to add. If key is in `_fields`,
            value is appended using the "+" operator, else key, value are
            added to fields.
        update : dict, default={}
            Dictionary specifying keys & values to replace using update().
        
        Returns
        -------
        fields : dict
            Resulting `EFTMethod` data.
    
        """
        fields = instance.get_fields(add=add, update=update)

        # new parameters with null dependence
        null_params = [p for p in newparams if p not in instance._params]
        n_null = len(null_params)

        # append new parameters
        params = instance._params + null_params
        islice = [params.index(p) for p in newparams] # new index order
        fields['params'] = np.array(params)[islice].tolist() # reorder

        # append null linear dependences
        shape_lin = instance._linear.shape
        lin = np.zeros(
          list(shape_lin[:-1]) +
          [ n_null + shape_lin[-1]]
        )
        lin[...,:shape_lin[-1]] = instance._linear
        fields['linear'] =  cls.to_list_or_scalar(lin[...,islice]) # reorder

        # append null quadratic dependences
        if not linear_only:
            shape_quad = instance._quadratic.shape
            quad = np.zeros(
              list(shape_quad[:-2]) +
              [ n_null + shape_quad[-2], n_null + shape_quad[-1]]
            )
            quad[...,:shape_quad[-2],:shape_quad[-1]] = instance._quadratic
            fields['quadratic'] = cls.to_list_or_scalar(
                ( (quad.T)[ np.ix_(islice,islice) ] ).T  # reorder
            )
        else:
            fields['quadratic'] = None
            
        return fields

    @classmethod
    def as_newparams(
      cls, 
      instance, 
      newparams, 
      add_to_fields={}, 
      update_fields={},
      linear_only=False
    ):
        """
        Initialiser method for a new EFTMethod instance corresponding
        'transformed' to basis of newparams.

        All arguments are passed to `get_fields_newparams()`.
        
        Parameters
        ----------
        instance : EFTMethod or subclass
            Instance to restructure data for, based on `newparams` parameter 
            list.
        newparams : list of str
            List of parameter names to generate restructures `EFTMethod` data 
            for. For any elements of `newparams` that are present as parameters 
            of the instance, the polynomial information is propagated, 
            accounting for any reorderings. For any elements of `newparams` 
            that are not present as parameters of the instance a null 
            dependence is inserted.
        linear_only : bool, default=False
            Flag to consistently preserve 'linear only' nature of resulting 
            data.
        add_to_fields : dict, default={}
            Dictionary specifying keys & values to add. If key is in `_fields`,
            value is appended using the "+" operator, else key, value are
            added to fields.
        update_fields : dict, default={}
            Dictionary specifying keys & values to replace using update().
        
        Returns
        -------
        EFTMethod
            Resulting `EFTMethod` instance.
    
        """
        fields = cls.get_fields_newparams(
          instance,
          newparams,
          add=add_to_fields,
          update=update_fields,
          linear_only=linear_only
        )
        return cls(**fields)

    def newparams(
      self, 
      newparams, 
      add_to_fields={}, 
      update_fields={},
      linear_only=False
    ):
        """Instance method for newparams.

        All arguments are passed to `as_newparams()`.
        
        Parameters
        ----------
        newparams : list of str
            List of parameter names to generate restructures `EFTMethod` data 
            for. For any elements of `newparams` that are present as parameters 
            of the instance, the polynomial information is propagated, 
            accounting for any reorderings. For any elements of `newparams` 
            that are not present as parameters of the instance a null 
            dependence is inserted.
        linear_only : bool, default=False
            Flag to consistently preserve 'linear only' nature of resulting 
            data.
        add_to_fields : dict, default={}
            Dictionary specifying keys & values to add. If key is in `_fields`,
            value is appended using the "+" operator, else key, value are
            added to fields.
        update_fields : dict, default={}
            Dictionary specifying keys & values to replace using update().
        
        Returns
        -------
        EFTMethod
            Resulting `EFTMethod` instance.
    
        """
        return self.__class__.as_newparams(
          self,
          newparams,
          add_to_fields=add_to_fields,
          update_fields=update_fields,
          linear_only=linear_only
        )
    
    def reordered(self, newparams):
        """Returns new EFTMethod instance with reordered parameters.

        Special case of `EFTMethod.newparams`. ata fields are restructures 
        accordingly
        
        Parameters
        ----------        
        newparams : list of str
            List of parameters according to which the `_params` attribute is 
            reordered. Parameters not appearing in the instances `_params` 
            attribute are ignored.
        
        Returns
        -------
        EFTMethod
            Resulting `EFTMethod` instance.
        
        """
        order = [c for c in newparams if c in self._params]

        return self.newparams(order)
    
    def reorder(self, newparams):
        """Reorder parameter dependence of `EFTMethod` instance in-place.
        
        Makes use of `EFTMethod.reordered()`.
        
        Parameters
        ----------        
        newparams : list of str
            List of parameters according to which the `_params` attribute is 
            reordered. Parameters not appearing in the instances `_params` 
            attribute are ignored.
        
        """
        temp = self.reordered(newparams)
        for fld in ['params', 'constant', 'linear', 'quadratic']:
            self.set_field(fld, getattr(temp,'_'+fld))
        
    
    ## binary operations
    @classmethod
    def check_binary_argument_dimensions(cls, lhs, rhs):
        """
        Checks if two EFTMethod instances have compatible dimension for a binary
        operation. 
        
        Length of `_shape` attributes must be equal or one of the 
        two must be return a scalar value.
        
        Parameters
        ----------
        lhs, rhs : EFTMethod or subclass
            `ETFMethod` instances to compare.
        
        Returns
        -------
        bool
        
        Raises
        ------
        EFTMethodError
            If the check fails.
        
        """
        # check output dimensions match or one of (lhs, rhs) is a scalar
        if not () in (lhs._shape, rhs._shape):
            if not (len(lhs._shape) == len(rhs._shape)):
                raise EFTMethodError(
                  (
                    'Output shapes of lhs ({}) and rhs ({}) '
                    'do not match in {}.get_matched_params(lhs,rhs)'
                  ).format(lhs._shape, rhs._shape, cls.__name__)
                )

        return True
    
    @classmethod
    def check_binary_argument_shapes(cls, lhs, rhs):
        """
        Checks if two EFTMethod instances have compatible shapes for a binary
        operation. 
        
        `_shape` attributes must be equal or one of the two must be a scalar.
        Raises EFTMethod error if not.
        
        
        Parameters
        ----------
        lhs, rhs : EFTMethod or subclass
            `ETFMethod` instances to compare.
        
        Returns
        -------
        bool
        
        Raises
        ------
        EFTMethodError
            If the check fails.
        
        """

        # check output dimensions match or one of (lhs, rhs) is a scalar
        if not ( (lhs._shape == rhs._shape) or
                 ( () in (lhs._shape, rhs._shape)) ):
            raise EFTMethodError(
              (
                'Output shapes of lhs ({}) and rhs ({}) '
                'do not match in {}.get_matched_params(lhs,rhs)'
              ).format(lhs._shape, rhs._shape, cls.__name__)
            )

        return True

    @classmethod
    def get_matched_params(cls, lhs, rhs):
        """
        Returns a pair of EFTMethod instances ready for input into a binary
        operation. 
        
        Constructs the union of both param fields and generates new
        instances in the same "basis" with `newparams()` method. The union of 
        parameter fields is ordered according to the arguments provided: 
        starting from all of those present in `lhs` followed by those in `rhs` 
        that are not already present in `lhs`.
        
        Parameters
        ----------
        lhs, rhs : EFTMethod or subclass
            `ETFMethod` instances to transform.
        
        Returns
        -------
        EFTMethod
            Instance with transformed data for `lhs`
        EFTMethod
            Instance with transformed data for `rhs`
        
        """

        params_lhs, params_rhs  = lhs._params, rhs._params

        # union of parameters preserving the order
        params = list( dict.fromkeys(params_lhs+params_rhs) )
        

        return lhs.newparams(params), rhs.newparams(params)

    ### sum
    @classmethod
    def get_fields_sum(
      cls, 
      lhs, 
      rhs, 
      add={}, 
      update={},
      linear_only=False
    ):
        """
        Calculate input data for sum of two instance predictions.
        
        Expanded to the same order as the instance with the lowest expansion 
        order (linear or quadratic). All fields not related to the EFT 
        prediction value will be inherited from lhs. If rhs is not an EFTMethod 
        instance, method falls back to `get_fields_add_float()`.
    
        Parameters
        ----------
        lhs: EFTMethod or subclass
            Left hand side of addition operation.
        rhs : EFTMethod or subclass
            Right hand side of addition operation.
        linear_only : bool, default=False
            Flag to consistently preserve 'linear only' nature of resulting 
            data.
        
        Parameters passed to `get_fields()`
        -----------------------------------
        add : dict, default={}
            Dictionary specifying keys & values to add. If key is in `_fields`,
            value is appended using the "+" operator, else key, value are
            added to fields.
        update : dict, default={}
            Dictionary specifying keys & values to replace using update().
        
        Returns
        -------
        fields : dict
            Resulting `EFTMethod` data.
        
        """
        if isinstance(rhs, EFTMethod):

            cls.check_binary_argument_shapes(lhs, rhs)

            # new instances with matching param "basis"
            lhs_input, rhs_input = cls.get_matched_params(lhs, rhs)

            fields_lhs, fields_rhs = lhs_input.get_fields(), rhs_input.get_fields()
            fields = dict(fields_lhs)

            # numpy broadcasting rules should capture cases when both outputs
            # have the same shape and when one is a scalar ( shape==() )
            fields['constant'] = cls.to_list_or_scalar(
              lhs_input._constant + rhs_input._constant
            )

            fields['linear'] = cls.to_list_or_scalar(
              lhs_input._linear + rhs_input._linear
            )
            
            if not linear_only:
                fields['quadratic'] = cls.to_list_or_scalar(
                  lhs_input._quadratic + rhs_input._quadratic
                )
            else:
                fields['quadratic'] = None

            return fields
        else:
            return cls.get_fields_add_float(lhs, rhs, add=add, update=update)

    @classmethod
    def as_sum(
      cls, 
      lhs, 
      rhs, 
      add_to_fields={}, 
      update_fields={},
      linear_only=False
    ):
        """
        Initialiser method for a new EFTMethod instance corresponding the sum
        of two others.
    
        All arguments are passed to `get_fields_sum()`.
    
        Parameters
        ----------
        lhs: EFTMethod or subclass
            Left hand side of addition operation.
        rhs : EFTMethod or subclass
            Right hand side of addition operation.
        linear_only : bool, default=False
            Flag to consistently preserve 'linear only' nature of resulting 
            data.
        add : dict, default={}
            Dictionary specifying keys & values to add. If key is in `_fields`,
            value is appended using the "+" operator, else key, value are
            added to fields.
        update : dict, default={}
            Dictionary specifying keys & values to replace using update().
        
        Returns
        -------
        EFTMethod
            Resulting `EFTMethod` instance.
        
        """
        fields = cls.get_fields_sum(
          lhs,
          rhs,
          add=add_to_fields,
          update=update_fields,
          linear_only=linear_only
        )
        return cls(**fields)

    def sum(
      self, 
      rhs, 
      add_to_fields={}, 
      update_fields={},
      linear_only=False
    ):
        """Method for sum of instance with another EFTMethod instance.
    
        All arguments are passed to `get_fields_sum()`.
    
        Parameters
        ----------
        rhs : EFTMethod or subclass
            Instance to add.
        linear_only : bool, default=False
            Flag to consistently preserve 'linear only' nature of resulting 
            data.
        add : dict, default={}
            Dictionary specifying keys & values to add. If key is in `_fields`,
            value is appended using the "+" operator, else key, value are
            added to fields.
        update : dict, default={}
            Dictionary specifying keys & values to replace using update().
        
        Returns
        -------
        EFTMethod
            Resulting `EFTMethod` instance.
    
        """
        return self.__class__.as_sum(
          self,
          rhs,
          add_to_fields=add_to_fields,
          update_fields=update_fields,
          linear_only=linear_only
        )

    def add(self, rhs, **kwargs):
        """Alias for sum."""
        return self.sum(rhs, **kwargs)

    ### product
    @classmethod
    def get_fields_product(
      cls, 
      lhs, 
      rhs, 
      add={}, 
      update={},
      linear_only=False
    ):
        """
        Calculate input data for product of two instance predictions.
        
        Expanded to the same order as the instance with the lowest expansion 
        order (linear or quadratic). All fields not related to the EFT 
        prediction value will be inherited from lhs. If rhs is not an EFTMethod 
        instance, method falls back to `get_fields_multiply_float()`.
    
        Parameters
        ----------
        lhs: EFTMethod or subclass
            Left hand side of multiplication operation.
        rhs : EFTMethod or subclass
            Right hand side of multiplication operation.
        linear_only : bool, default=False
            Flag to consistently preserve 'linear only' nature of resulting 
            data.
        
        Parameters passed to `get_fields()`
        -----------------------------------
        add : dict, default={}
            Dictionary specifying keys & values to add. If key is in `_fields`,
            value is appended using the "+" operator, else key, value are
            added to fields.
        update : dict, default={}
            Dictionary specifying keys & values to replace using update().
        
        Returns
        -------
        fields : dict
            Resulting `EFTMethod` data.
        
        """
        # new instances with matching param "basis"
        if isinstance(rhs, EFTMethod):

            cls.check_binary_argument_shapes(lhs, rhs)

            lhs_input, rhs_input = cls.get_matched_params(lhs, rhs)

            fields_lhs, fields_rhs = lhs_input.get_fields(), rhs_input.get_fields()
            fields = dict(fields_lhs)

            const_l, const_r = lhs_input._constant, rhs_input._constant
            lin_l, lin_r = lhs_input._linear, rhs_input._linear
            quad_l, quad_r = lhs_input._quadratic, rhs_input._quadratic

            const = const_l*const_r
            fields['constant'] = cls.to_list_or_scalar(const)

            if rhs._shape == lhs._shape:
                lin_lr, lin_rl = ( lin_l.T*const_r ).T , ( lin_r.T*const_l ).T
                quad_lr, quad_rl = ( quad_l.T*const_r ).T, ( quad_r.T*const_l ).T

            elif rhs._shape == ():
                lin_lr, lin_rl = lin_l*const_r, np.outer(const_l, lin_r)
                einstr = '{0},jk->{0}jk'.format('abcdefgh'[:const_l.ndim])
                quad_lr, quad_rl = quad_l*const_r, np.einsum(einstr, const_l, quad_r)
            elif lhs._shape == ():
                lin_lr, lin_rl = np.outer( const_r, lin_l ), lin_r*const_l
                einstr = '{0},jk->{0}jk'.format('abcdefgh'[:const_r.ndim])
                quad_lr, quad_rl = np.einsum(einstr, const_r, quad_l), quad_r*const_l

            linear = lin_lr + lin_rl
            fields['linear'] = cls.to_list_or_scalar(linear)

            linsq = cls.outer_last(lin_l, lin_r, symmetrise=True)
            quadratic = linsq + quad_lr + quad_rl
                
            if not linear_only:
                fields['quadratic'] = cls.to_list_or_scalar(quadratic)
            else:
                fields['quadratic'] = None

            return fields

        else:
            return cls.get_fields_multiply_float(lhs, rhs, add=add, update=update)


    @classmethod
    def as_product(
      cls, 
      lhs, 
      rhs, 
      add_to_fields={}, 
      update_fields={},
      linear_only=False
    ):
        """
        Initialiser method for a new EFTMethod instance corresponding the product
        of two others.
        
        All arguments are passed to `get_fields_product()`.
        
        Parameters
        ----------
        lhs: EFTMethod or subclass
            Left hand side of multiplication operation.
        rhs : EFTMethod or subclass
            Right hand side of multiplication operation.
        linear_only : bool, default=False
            Flag to consistently preserve 'linear only' nature of resulting 
            data.
        add : dict, default={}
            Dictionary specifying keys & values to add. If key is in `_fields`,
            value is appended using the "+" operator, else key, value are
            added to fields.
        update : dict, default={}
            Dictionary specifying keys & values to replace using update().
        
        Returns
        -------
        EFTMethod
            Resulting `EFTMethod` instance.
    
        """
        fields = cls.get_fields_product(
          lhs,
          rhs,
          add=add_to_fields,
          update=update_fields,
          linear_only=linear_only
        )

        return cls(**fields)

    def product(
      self, 
      rhs, 
      add_to_fields={}, 
      update_fields={},
      linear_only=False
    ):
        """Method for product of instance with another EFTMethod instance.
        
        All arguments are passed to `as_product()`.
        
        Parameters
        ----------
        rhs : EFTMethod or subclass
            Instance to multiply by.
        linear_only : bool, default=False
            Flag to consistently preserve 'linear only' nature of resulting 
            data.
        add : dict, default={}
            Dictionary specifying keys & values to add. If key is in `_fields`,
            value is appended using the "+" operator, else key, value are
            added to fields.
        update : dict, default={}
            Dictionary specifying keys & values to replace using update().
        
        Returns
        -------
        EFTMethod
            Resulting `EFTMethod` instance.
        
        """
        return self.__class__.as_product(
          self,
          rhs,
          add_to_fields=add_to_fields,
          update_fields=update_fields,
          linear_only=linear_only
        )

    def multiply_by(self, rhs, **kwargs):
        """Alias for product."""
        return self.product(rhs, **kwargs)

    ### product
    @classmethod
    def get_fields_quotient(
      cls,
      lhs, 
      rhs, 
      add={}, 
      update={},
      linear_only=False
    ):
        """
        Calculate input data for quotient of two instance predictions.
    
        Alias for the special case of multiplying by the inverse, 
        `EFTMethod.get_fields_product(lhs, rhs.inverse())`.
    
        Parameters
        ----------
        lhs: EFTMethod or subclass
            Left hand side of quotient operation.
        rhs : EFTMethod or subclass
            Right hand side of quotient operation.
        linear_only : bool, default=False
            Flag to consistently preserve 'linear only' nature of resulting 
            data.
        
        Parameters passed to `get_fields()`
        -----------------------------------
        add : dict, default={}
            Dictionary specifying keys & values to add. If key is in `_fields`,
            value is appended using the "+" operator, else key, value are
            added to fields.
        update : dict, default={}
            Dictionary specifying keys & values to replace using update().
        
        Returns
        -------
        fields : dict
            Resulting `EFTMethod` data.
        
        """
        if isinstance(rhs, EFTMethod):
            return cls.get_fields_product(
              lhs,
              rhs.inverse(),
              add=add,
              update=update,
              linear_only=linear_only
            )
        else:
            return cls.get_fields_divide_float(
              lhs, 
              rhs, 
              add=add, 
              update=update,
              linear_only=linear_only
            )

    @classmethod
    def as_quotient(
      cls, 
      lhs, 
      rhs, 
      add_to_fields={}, 
      update_fields={},
      linear_only=False
    ):
        """
        Initialiser method for a new EFTMethod instance corresponding the quotient
        of two others.
    
        All arguments are passed to `get_fields_quotient()`.
        
        Parameters
        ----------
        lhs: EFTMethod or subclass
            Left hand side of quotient operation.
        rhs : EFTMethod or subclass
            Right hand side of quotient operation.
        linear_only : bool, default=False
            Flag to consistently preserve 'linear only' nature of resulting 
            data.
        add : dict, default={}
            Dictionary specifying keys & values to add. If key is in `_fields`,
            value is appended using the "+" operator, else key, value are
            added to fields.
        update : dict, default={}
            Dictionary specifying keys & values to replace using update().
        
        Returns
        -------
        EFTMethod
            Resulting `EFTMethod` instance.
    
        """
        fields = cls.get_fields_quotient(
          lhs,
          rhs,
          add=add_to_fields,
          update=update_fields,
          linear_only=linear_only
        )
        return cls(**fields)

    def quotient(
      self, 
      rhs, 
      add_to_fields={}, 
      update_fields={},
      linear_only=False
    ):
        """Method for quotient of instance with another EFTMethod instance.
        
        All arguments are passed to `as_quotient()`.
        
        Parameters
        ----------
        rhs : EFTMethod or subclass
            Instance to divide by.
        linear_only : bool, default=False
            Flag to consistently preserve 'linear only' nature of resulting 
            data.
        add : dict, default={}
            Dictionary specifying keys & values to add. If key is in `_fields`,
            value is appended using the "+" operator, else key, value are
            added to fields.
        update : dict, default={}
            Dictionary specifying keys & values to replace using update().
        
        Returns
        -------
        EFTMethod
            Resulting `EFTMethod` instance.
    
        """
        return self.__class__.as_quotient(
          self,
          rhs,
          add_to_fields=add_to_fields,
          update_fields=update_fields,
          linear_only=linear_only
        )

    def divide_by(self, rhs, **kwargs):
        """Alias for quotient."""
        return self.quotient(rhs, **kwargs)

    def ratio(self, rhs, **kwargs):
        """Alias for quotient."""
        return self.quotient(rhs, **kwargs)

    ### concatenate list of instances
    @classmethod
    def get_fields_concatenation(
      cls, 
      lhs, 
      rhs, 
      add={}, 
      update={},
      linear_only=False
    ):
        """
        Calculate input data for concatenation of two EFTMethod instances. 
    
        Concatenation corresponds to concatenating the data fields, creating a 
        new return value with first axis length equal to the sum of the first 
        axes of lhs and rhs. 
        Method only works if the return value has the same dimensions 
        (constant, 1D array, 2D array, etc.). All fields not related to the EFT 
        prediction value will be inherited from lhs.
    
        E.g. if `lhs` returns a 4-element list [l1,l2,l3,l4] and `rhs` returns 
        a 2-element list [r1,r2], the resulting `EFTMethod` will return 
        [l1,l2,l3,l4,r1,r2]. Single-valued `EFTMethod` instances are converted 
        to 1-element lists with `EFTMethod.scalar2list()`.
    
        Parameters
        ----------
        lhs : EFTMethod or subclass
            Left hand side of concatenation operation.
        rhs : EFTMethod or subclass
            Right hand side of concatenation operation.
        linear_only : bool, default=False
            Flag to consistently preserve 'linear only' nature of resulting 
            data.
        
        Parameters passed to `get_fields()`
        -----------------------------------
        add : dict, default={}
            Dictionary specifying keys & values to add. If key is in `_fields`,
            value is appended using the "+" operator, else key, value are
            added to fields.
        update : dict, default={}
            Dictionary specifying keys & values to replace using update().
        
        Returns
        -------
        fields : dict
            Resulting `EFTMethod` data.
    
        """
        
        cls.check_binary_argument_dimensions(lhs, rhs)

        # new instances with matching param "basis"
        lhs_input, rhs_input = cls.get_matched_params(lhs, rhs)

        fields_lhs, fields_rhs = lhs_input.scalar2list().get_fields(), rhs_input.scalar2list().get_fields()
        fields = dict(fields_lhs)
        
        fields['constant'] = ( fields_lhs['constant'] + fields_rhs['constant'] )

        fields['linear'] = ( fields_lhs['linear'] + fields_rhs['linear'] )
        
        if not linear_only:
            fields['quadratic'] = ( fields_lhs['quadratic'] + fields_rhs['quadratic'] )
        else:
            fields['quadratic'] = None

        return fields

    @classmethod
    def as_concatenation(
      cls, 
      lhs, 
      *rhs, 
      add_to_fields={}, 
      update_fields={},
      linear_only=False
    ):
        """
        Initialiser method for a new EFTMethod instance corresponding to the 
        concatenation of several instances `lhs` & `*rhs`.
        
        Elements of `rhs` will be successively concatenated in the order in 
        which they are passed.
    
        All arguments are passed to `get_fields_concatenation()`.
        
        Parameters
        ----------
        lhs: EFTMethod or subclass
            Left hand side of concatenation operation.
        rhs : tuple or list of EFTMethod or subclass
            Right hand side of concatenation operation.
        linear_only : bool, default=False
            Flag to consistently preserve 'linear only' nature of resulting 
            data.
        add : dict, default={}
            Dictionary specifying keys & values to add. If key is in `_fields`,
            value is appended using the "+" operator, else key, value are
            added to fields.
        update : dict, default={}
            Dictionary specifying keys & values to replace using update().
        
        Returns
        -------
        EFTMethod
            Resulting `EFTMethod` instance.
        
        """
        
        # loop over rhs instances, instantiating updated lhs each time
        for ir in rhs:
            ifields = cls.get_fields_concatenation(
              lhs,
              ir,
              add=add_to_fields,
              update=update_fields,
              linear_only=linear_only
            )
            lhs = cls(**ifields)
        
        return lhs

    def concatenate(
      self,
      *rhs, 
      add_to_fields={}, 
      update_fields={},
      linear_only=False
    ):
        """
        Method for concatenation of instance with several other EFTMethod 
        instances.
    
        All arguments are passed to `as_concatenation()`.
        
        Parameters
        ----------
        lhs: EFTMethod or subclass
            Left hand side of concatenation operation.
        rhs : tuple or list of EFTMethod or subclass
            Right hand side of concatenation operation.
        linear_only : bool, default=False
            Flag to consistently preserve 'linear only' nature of resulting 
            data.
        add : dict, default={}
            Dictionary specifying keys & values to add. If key is in `_fields`,
            value is appended using the "+" operator, else key, value are
            added to fields.
        update : dict, default={}
            Dictionary specifying keys & values to replace using update().
        
        Returns
        -------
        EFTMethod
            Resulting `EFTMethod` instance.
    
        """
        return self.__class__.as_concatenation(
          self,
          *rhs,
          add_to_fields=add_to_fields,
          update_fields=update_fields,
          linear_only=linear_only
        )


################################################################################
# EFTMethod exceptions
class EFTMethodError(FMCallableError):
    """Base Exception class for EFTMethod."""
    pass

class EFTMethodInitError(FMCallableInitError):
    """Exception class raised during initialisation of EFTMethod."""
    pass

################################################################################
# Internal parameter for quadratic EFTMethod functions
class EFTInternalParameter(EFTMethod):
    """
    Internal parameter method for Effective Field Theories.

    Adds the special `_depends_on` attribute so that it is recognised by 
    TheoryBase subclasses as an internal parameter. Designed to use with 
    `EFTBase` instances and subclasses. All EFTMethod attributes and methods 
    are inherited.
    
    Attributes
    ----------
    _depends_on : tuple
        Theory parameters on which this internal parameter depends. 
        Specified by the `params` argument of `__init__`.
    
    New methods
    -----------
    get_json_repr
        Overloads `EFTMethod` method to treat certain fields whose values are 
        identified with the default values by setting them to None, so they are 
        omitted from the representation.
    
    """
    def __init__(self, params=[], **kwargs):
        """
        Parameters
        ----------
        params : list of str, default=[]
            Explicit list of theory parameters on which the internal parameter 
            depends.
        kwargs : dict
            Other data fields fed to the parent class constructor. See 
            `EFTMethod` documentation.
        
        """
        self._depends_on = tuple(params)
        super(EFTInternalParameter, self).__init__(params=params, **kwargs)

    def get_json_repr(self, indent=2):
        """Convert instance to json string, convert inner arrays to one line.
        
        Overloads `EFTMethod.get_json_repr()` to set certain data arguments to
        None if they coincide with the default values, so that they are omitted 
        from the json representation.
        
        Parameters
        ----------
        indent : int, default=2
            Indentation parameter for `json.dump()`.
        
        """
        fields = self.to_dict()
        
        keys = ('name', 'doc', 'divide')
        defaults = (None, '', 1.)
        
        for key, default in zip(keys, defaults):
            try:
                if fields[key] == default: 
                    fields.pop(key)
            except KeyError:
                pass

        return json.dumps(fields, indent=indent)
        
################################################################################
# Prediction for quadratic EFTMethod functions
class EFTPrediction(EFTMethod, TheoryPrediction):
    """
    TheoryPrediction subclass for Effective Field Theories.
    Prediction function is an instance of EFTMethod.
    New constructors "from_coeffs" and "from_json" to define predictions as
    quadratic polynomials in external parameters.

    Adds the special 'observable' field (and corresponding attribute) so that 
    it is recognised by TheoryBase subclasses as an prediction. Designed to use 
    with `EFTBase` instances and subclasses. All EFTMethod attributes and 
    methods are inherited.
    
    Attributes
    ----------
    kwargs : dict
        Other data fields fed to the parent class constructor. See `EFTMethod` 
        documentation.
    
    New methods
    -----------
    from_EFTmethod
        Alternate constructor to instantiate `EFTPrediction` from an existing 
        EFTMethod (or subclass) instance, e.g., an `EFTInternalParameter`.
    get_json_repr
        Overloads `EFTMethod` method to return a special json representation 
        that accounts for the need to associate an observable name to the 
        prediction.
    from_json
        Overloads `EFTMethod` method to return a special json representation 
        that accounts for the need to associate an observable name to the 
        prediction.
    
    """
    def __init__(self, **kwargs):
        super(EFTPrediction, self).__init__(**kwargs)

    @classmethod
    def from_EFTMethod(cls, method, observable):
        """Initialise EFTPrediction from an EFTMethod instance.
        
        Parameters
        ----------
        method : EFTMethod
            Explicit list of theory parameters on which the internal parameter 
            depends.
        observable : str
            Name of observable to associate to this `EFTPrediction`.
        
        """
        fields = method.get_fields()
        fields['observable'] = observable
        return cls(**fields)

    def get_json_repr(self, indent=2):
        """Convert instance to json string, convert inner arrays to one line.
        
        Overloads `EFTMethod.get_json_repr()` to return new json format that 
        accounts for the additionally required `observable` field bu adding a 
        new top level field called "observable_name", whose value is a usual 
        json representation for `EFTMethod`. 
        
        e.g.

        "observable_name":{
          "params":[...],
          "linear":[...],
          ...
        }
        
        Parameters
        ----------
        indent : int, default=2
            Indentation parameter for `json.dump()`.
        
        """
        fields = self.to_dict()

        obs = fields.pop('observable')
        
        keys = ('name', 'doc', 'divide')
        defaults = (None, '', 1.)
        
        for key, default in zip(keys, defaults):
            try:
                if fields[key] == default: 
                    fields.pop(key)
            except KeyError:
                pass
        
        newfields = {
            obs:fields
        }
        return json.dumps(newfields, indent=indent)
      
    @classmethod
    def from_json(cls, json_file, linear_only=False, rename={}):
        """
        Initialise EFTPrediction from a json file.
        
        File must have a single field specifying the observable name, whose
        value matches the structure expected from EFTMethod.from_json.

        e.g.

        "observable_name":{
          "params":[...],
          "linear":[...],
          ...
        }
        
        See documentation for `EFTMethod.from_json()` for an example of 
        possible structure of the inner json data.
        
        Parameters
        ----------
        json_file : str
            Path to json file form which to load `EFTMethod` data.
        linear_only : bool, default=False
            Only include data for predictions up to linear order in the 
            quadratic polynomial (EFT expansion).
        rename : dict, default={}
            Dictionary specifying as keys any parameters in 'param' field to 
            rename, and as values, the corresponding name to replace it with. 
            E.g., rename={'MyParam':'MyNewParam'} will replace 'MyParam' in 
            the 'params' data field with 'MyNewParam'.
        
        Returns
        -------
        EFTPrediction
            New instance reflecting data fields contained in `json_file`.
        
        Raises
        ------
        EFTPredictionInitError
            If the `json_file` cannot be found or raises 
            `json.decoder.JSONDecodeError` or if the lowest level field has 
            more than one key.
        
        """
        try:
            with open(json_file, 'r') as jfile:
                fields = json.load(jfile)
        except (IOError, json.decoder.JSONDecodeError) as e:
            msg =(
            "Unable to initialise EFTPrediction from json file: {}"
            ).format(e)
            raise EFTPredictionInitError(json_file, msg)

        if len(fields.keys()) > 1:
            msg = "Bad format for json EFT prediction. "
            raise EFTPredictionInitError(json_file, msg)

        obs = str(list(fields.keys())[0])

        data = fields[obs]

        if linear_only:
            data.pop('quadratic', None)

        if rename:
            data['params'] = [ rename.get(p,p) for p in data['params'] ]

        args = {'observable':obs,'name':obs}
        args.update(**data)
        
        return cls(**args)

################################################################################
# EFTPrediction exceptions
class EFTPredictionError(TheoryPredictionError):
    """Base Exception class for EFTPrediction."""
    pass

class EFTPredictionInitError(TheoryPredictionError):
    """Exception class raised during initialisation of EFTPrediction."""
    pass

################################################################################
# Base EFT class
class EFTBase(TheoryBase):
    """
    Effective Field Theory base class inheriting from TheoryBase.
    
    Has the cutoff parameter, Lambda, as an additional data member.
    Implements additional methods for adding (multiple) EFTPredictions from 
    json file.
    
    All attributes and methods inherited from `EFTMethod`.
    
    Attributes
    ----------
    Lambda : float, default=1000. 
        Cutoff value for EFT in units of GeV.
    
    New methods
    -----------
    add_prediction_from_json
        Add an `EFTPrediction` instance as a prediction method from a json file.
    add_predictions_from_json
        Add multiple `EFTPrediction` instances as a prediction methods from a 
        single json file.
    """
    def __init__(self, *args, **kwargs):
        """
        Parameters
        ----------
        args : tuple or list
            Positional arguments passed on to `TheoryBase` parent class.
        kwargs : dict
            Keyword arguments passed on to `TheoryBase` parent class. The 
            special key 'Lambda' if it is found here, is extracted from kwargs, 
            setting the value of the `Lambda` attribute.
        
        """
        # add special cutoff parameter, Lambda, not in external_parameters
        self.Lambda = kwargs.pop('Lambda', 1000.)
        super(EFTBase, self).__init__(*args, **kwargs)

    def __repr__(self):
        return ('<EFT class {} instance, Lambda={} GeV>').format(self.classname, self.Lambda)

    @classmethod
    def add_prediction_from_json(cls, json_file, **kwargs):
        """Add a new prediction from json file.
        
        Structure of the json file should match what is expected by 
        `EFTPrediction.from_json()`. See its documentation.
        
        Parameters
        ----------
        json_file : str
            Path to json file form which to load `EFTMethod` data.
        kwargs : dict
            Keyword arguments passed to `EFTPrediction.from_json()`. See its 
            documentation.
        
        """
        pred = EFTPrediction.from_json(json_file, **kwargs)
        cls._add_prediction( pred._observable, pred )

    @classmethod
    def add_predictions_from_json(cls, json_file, **kwargs):
        """Add multiple new predictions from a single json file.
        
        The json file should have a special structure as follows. Lowest level 
        fields will correspond to the "observable_name" field expected when 
        instantiating a single EFTPrediction from a json file (see 
        documentation for `EFTPrediction.from_json()`). The file can contain 
        many of these, each of which will be read in and added as predictions 
        of the `EFTBase` instance.
        
        Parameters
        ----------
        json_file : str
            Path to json file from which to load data for multiple 
            `EFTPrediction` instances.
        kwargs : dict
            Keyword arguments passed to `EFTPrediction.from_json()`. See its 
            documentation.
                
        """
        with open(json_file, 'r') as jfile:
            fields = json.load(jfile)

        linear_only = kwargs.get('linear_only', False)

        for obsname, kwargs2 in iter(fields.items()):
            obs = str(obsname)
           
            kwargs2.update({'observable':obs, 'name':obs})

            if linear_only:
                kwargs2['quadratic']=None
                
            pred = EFTPrediction(**kwargs2)
                
            cls._add_prediction(obs,  pred)

################################################################################
# tester/utility functions
def EFT_obs_coeff(model, obs, coeff, Lambda=1., linear=False):
    """
    Returns the predicted value of the theory `model`, for the observable, 
    `obs`, when setting only the theory parameter `coeff` to 1.0.
    
    If linear is set to True, the constant piece (when all coeffs are zero) is 
    subtracted.
    
    Parameters
    ----------
    model : TheoryBase or subclass
        Model class to use.
    obs : str
        Name of observable to return prediction for.
    coeff : str
        Name of parameter to set to 1.0. All other parameters are set to 0.
    
    Returns
    -------
    float or numpy.array
        Prediction for `obs` when `coeff`=1.0.
    
    """
    coeffs = model.external_parameters
    vals = [1. if x==coeff else 0. for x in coeffs]
    guyEFT = model(*vals, Lambda=Lambda)
    pred = guyEFT.predict(obs)
    if not linear:
        return pred
    else:
        guyEFT.update(*[0. for _ in coeffs] )
        return pred - guyEFT.predict(obs)

def EFT_par_coeff(model, param, coeff, Lambda=1., linear=False):
    """
    Returns the value of the parameter `param` of the theory `model`, when 
    setting only the theory parameter `coeff` to 1.0.
        
    If linear is set to True, the constant piece (when all coeffs are zero) is 
    subtracted.
    
    Parameters
    ----------
    model : TheoryBase or subclass
        Model class to use.
    param : str
        Name of parameter to return value of.
    coeff : str
        Name of parameter to set to 1.0. All other parameters are set to 0.
    
    Returns
    -------
    float or numpy.array
        Value of parameter `param` when `coeff`=1.0.
    
    """
    coeffs = model.external_parameters
    vals = [1. if x==coeff else 0. for x in coeffs]
    guyEFT = model(*vals, Lambda=Lambda)
    pval = guyEFT[param]
    if not linear:
        return pval
    else:
        guyEFT.update(*[0. for _ in coeffs] )
        return pval - guyEFT[param]

################################################################################

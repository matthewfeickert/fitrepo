from ....fitlib.theory import internal_parameter, prediction

from ...SM.aEW_MZ_GF_5F.higgs import (
  Production as SM_HiggsProduction, 
  Decay_old as SM_HiggsDecay
)

from ..higgs.decay import HiggsDecay as SMEFT_HiggsDecay_new # new ones
from ..higgs.decay import SMEFT_HiggsDecay_old as SMEFT_HiggsDecay # old ones
from ..higgs.production import HiggsProduction as SMEFT_HiggsProduction
from ..higgs.STXS import STXS_1p0 as SMEFT_HiggsProduction_STXS_1p0

class SMEFT_HiggsSignalStrengths_Run2_CMS_H_tata_new(SM_HiggsProduction, SMEFT_HiggsProduction_STXS_1p0, SMEFT_HiggsProduction, SMEFT_HiggsDecay_new):

    @prediction('mu_ggF0j_H_tata_13')
    def mu_ggF0j_H_tata_13(self):
        rprod = self['ratio_ggF0j']
        rdecay = self['rGam_H_tata']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggF1j_ptH_0T60_H_tata_13')
    def mu_ggF1j_ptH_0T60_H_tata_13(self):
        rprod = self['ratio_ggF1j_ptH_0T60']
        rdecay = self['rGam_H_tata']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggF1j_ptH_60T120_H_tata_13')
    def mu_ggF1j_ptH_60T120_H_tata_13(self):
        rprod = self['ratio_ggF1j_ptH_60T120']
        rdecay = self['rGam_H_tata']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggF1j_ptH_120Tinf_H_tata_13')
    def mu_ggF1j_ptH_120Tinf_H_tata_13(self):
        rprod = self['ratio_ggF1j_ptH_120Tinf']
        rdecay = self['rGam_H_tata']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggF2j_H_tata_13')
    def mu_ggF2j_H_tata_13(self):
        rprod = self['ratio_ggF2j']
        rdecay = self['rGam_H_tata']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggF_plus_qqH_VBF_H_tata_13')
    def mu_ggF_plus_qqH_VBF_H_tata_13(self):
        rprod = self['ratio_ggF_plus_qqH_VBF']
        rdecay = self['rGam_H_tata']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_qqH_mjj_60T120_H_tata_13')
    def mu_qqH_mjj_60T120_H_tata_13(self):
        rprod = self['ratio_qqH_mjj_60T120']
        rdecay = self['rGam_H_tata']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_qqH_ptj1_200Tinf_H_tata_13')
    def mu_qqH_ptj1_200Tinf_H_tata_13(self):
        rprod = self['ratio_qqH_ptj1_200Tinf']
        rdecay = self['rGam_H_tata']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_qqH_rest_H_tata_13')
    def mu_qqH_rest_H_tata_13(self):
        rprod = self['ratio_qqH_rest']
        rdecay = self['rGam_H_tata']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

class SMEFT_HiggsSignalStrengths_Run2_CMS_H_tata(SM_HiggsProduction, SMEFT_HiggsProduction_STXS_1p0, SMEFT_HiggsProduction, SMEFT_HiggsDecay, SM_HiggsDecay):

    @prediction('mu_ggF0j_H_tata_13')
    def mu_ggF0j_H_tata_13(self):
        rprod = self['ratio_ggF0j']
        rdecay = self['dGam_H_tata']/self['Gam_H_tata_SM']
        rwidth = self['dGam_H']/self['Gam_H_SM']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggF1j_ptH_0T60_H_tata_13')
    def mu_ggF1j_ptH_0T60_H_tata_13(self):
        rprod = self['ratio_ggF1j_ptH_0T60']
        rdecay = self['dGam_H_tata']/self['Gam_H_tata_SM']
        rwidth = self['dGam_H']/self['Gam_H_SM']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggF1j_ptH_60T120_H_tata_13')
    def mu_ggF1j_ptH_60T120_H_tata_13(self):
        rprod = self['ratio_ggF1j_ptH_60T120']
        rdecay = self['dGam_H_tata']/self['Gam_H_tata_SM']
        rwidth = self['dGam_H']/self['Gam_H_SM']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggF1j_ptH_120Tinf_H_tata_13')
    def mu_ggF1j_ptH_120Tinf_H_tata_13(self):
        rprod = self['ratio_ggF1j_ptH_120Tinf']
        rdecay = self['dGam_H_tata']/self['Gam_H_tata_SM']
        rwidth = self['dGam_H']/self['Gam_H_SM']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggF2j_H_tata_13')
    def mu_ggF2j_H_tata_13(self):
        rprod = self['ratio_ggF2j']
        rdecay = self['dGam_H_tata']/self['Gam_H_tata_SM']
        rwidth = self['dGam_H']/self['Gam_H_SM']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggF_plus_qqH_VBF_H_tata_13')
    def mu_ggF_plus_qqH_VBF_H_tata_13(self):
        rprod = self['ratio_ggF_plus_qqH_VBF']
        rdecay = self['dGam_H_tata']/self['Gam_H_tata_SM']
        rwidth = self['dGam_H']/self['Gam_H_SM']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_qqH_mjj_60T120_H_tata_13')
    def mu_qqH_mjj_60T120_H_tata_13(self):
        rprod = self['ratio_qqH_mjj_60T120']
        rdecay = self['dGam_H_tata']/self['Gam_H_tata_SM']
        rwidth = self['dGam_H']/self['Gam_H_SM']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_qqH_ptj1_200Tinf_H_tata_13')
    def mu_qqH_ptj1_200Tinf_H_tata_13(self):
        rprod = self['ratio_qqH_ptj1_200Tinf']
        rdecay = self['dGam_H_tata']/self['Gam_H_tata_SM']
        rwidth = self['dGam_H']/self['Gam_H_SM']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_qqH_rest_H_tata_13')
    def mu_qqH_rest_H_tata_13(self):
        rprod = self['ratio_qqH_rest']
        rdecay = self['dGam_H_tata']/self['Gam_H_tata_SM']
        rwidth = self['dGam_H']/self['Gam_H_SM']
        return 1. + rprod + rdecay - rwidth

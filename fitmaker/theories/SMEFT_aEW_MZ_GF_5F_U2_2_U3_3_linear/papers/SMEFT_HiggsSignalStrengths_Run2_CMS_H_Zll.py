from ....fitlib.theory import internal_parameter, prediction

from ...SM.aEW_MZ_GF_5F.higgs import (
  Production as SM_HiggsProduction, 
  Decay_old as SM_HiggsDecay
)

from ..higgs.decay import HiggsDecay as SMEFT_HiggsDecay_new # new ones
from ..higgs.decay import SMEFT_HiggsDecay_old as SMEFT_HiggsDecay # old ones
from ..higgs.production import HiggsProduction as SMEFT_HiggsProduction
from ..higgs.STXS import STXS_1p1 as SMEFT_HiggsProduction_STXS_1p1

class SMEFT_HiggsSignalStrengths_Run2_CMS_H_Zll_new(SM_HiggsProduction, SMEFT_HiggsProduction_STXS_1p1, SMEFT_HiggsProduction, SMEFT_HiggsDecay_new):
    '''
    SMEFT predictions for Stage 1.1 STXS merged bins used in CMS H -> ZZ* -> 4l paper http://cds.cern.ch/record/2668684
    '''
    @prediction('mu_ggF0j_ptH_0T10_H_Zll_13')
    def mu_ggF0j_ptH_0T10_H_Zll_13(self):
        rprod = self['ratio_ggF0j_ptH_0T10']
        rdecay = self['rGam_H_4l']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggF0j_ptH_10T200_H_Zll_13')
    def mu_ggF0j_ptH_10T200_H_Zll_13(self):
        rprod = self['ratio_ggF0j_ptH_10T200']
        rdecay = self['rGam_H_4l']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggF1j_ptH_0T60_H_Zll_13')
    def mu_ggF1j_ptH_0T60_H_Zll_13(self):
        rprod = self['ratio_ggF1j_ptH_0T60']
        rdecay = self['rGam_H_4l']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggF1j_ptH_60T120_H_Zll_13')
    def mu_ggF1j_ptH_60T120_H_Zll_13(self):
        rprod = self['ratio_ggF1j_ptH_60T120']
        rdecay = self['rGam_H_4l']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggF1j_ptH_120T200_H_Zll_13')
    def mu_ggF1j_ptH_120T200_H_Zll_13(self):
        rprod = self['ratio_ggF1j_ptH_120T200']
        rdecay = self['rGam_H_4l']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggF_ptH_200Tinf_H_Zll_13')
    def mu_ggF_ptH_200Tinf_H_Zll_13(self):
        rprod = self['ratio_ggF_ptH_200Tinf']
        rdecay = self['rGam_H_4l']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggF2j_mjj_0T350_ptH_0T60_H_Zll_13')
    def mu_ggF2j_mjj_0T350_ptH_0T60_H_Zll_13(self):
        rprod = self['ratio_ggF2j_mjj_0T350_ptH_0T60']
        rdecay = self['rGam_H_4l']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggF2j_mjj_0T350_ptH_60T120_H_Zll_13')
    def mu_ggF2j_mjj_0T350_ptH_60T120_H_Zll_13(self):
        rprod = self['ratio_ggF2j_mjj_0T350_ptH_60T120']
        rdecay = self['rGam_H_4l']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggF2j_mjj_0T350_ptH_120T200_H_Zll_13')
    def mu_ggF2j_mjj_0T350_ptH_120T200_H_Zll_13(self):
        rprod = self['ratio_ggF2j_mjj_0T350_ptH_120T200']
        rdecay = self['rGam_H_4l']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggF2j_mjj_350Tinf_H_Zll_13')
    def mu_ggF2j_mjj_350Tinf_H_Zll_13(self):
        rprod = self['ratio_ggF2j_mjj_350Tinf']
        rdecay = self['rGam_H_4l']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_qqH_mjj_60T120_H_Zll_13')
    def mu_qqH_mjj_60T120_H_Zll_13(self):
        rprod = self['ratio_qqH_mjj_60T120']
        rdecay = self['rGam_H_4l']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_qqH_mjj_350T700_ptH_0T200_ptHjj_0T25_H_Zll_13')
    def mu_qqH_mjj_350T700_ptH_0T200_ptHjj_0T25_H_Zll_13(self):
        rprod = self['ratio_qqH_mjj_350T700_ptH_0T200_ptHjj_0T25']
        rdecay = self['rGam_H_4l']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_qqH_mjj_700Tinf_ptH_0T200_ptHjj_0T25_H_Zll_13')
    def mu_qqH_mjj_700Tinf_ptH_0T200_ptHjj_0T25_H_Zll_13(self):
        rprod = self['ratio_qqH_mjj_700Tinf_ptH_0T200_ptHjj_0T25']
        rdecay = self['rGam_H_4l']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_qqH_mjj_350Tinf_ptH_0T200_ptHjj_25Tinf_H_Zll_13')
    def mu_qqH_mjj_350Tinf_ptH_0T200_ptHjj_25Tinf_H_Zll_13(self):
        rprod = self['ratio_qqH_mjj_350Tinf_ptH_0T200_ptHjj_25Tinf']
        rdecay = self['rGam_H_4l']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_qqH_mjj_350Tinf_ptH_200Tinf_H_Zll_13')
    def mu_qqH_mjj_350Tinf_ptH_200Tinf_H_Zll_13(self):
        rprod = self['ratio_qqH_mjj_350Tinf_ptH_200Tinf']
        rdecay = self['rGam_H_4l']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_qqH_rest_H_Zll_13')
    def mu_qqH_rest_H_Zll_13(self):
        rprod = self['ratio_qqH_rest']
        rdecay = self['rGam_H_4l']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_VH_ptV_0T150_H_Zll_13')
    def mu_VH_ptV_0T150_H_Zll_13(self):
        rprod = self['ratio_VH_ptV_0T150']
        rdecay = self['rGam_H_4l']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_VH_ptV_150Tinf_H_Zll_13')
    def mu_VH_ptV_150Tinf_H_Zll_13(self):
        rprod = self['ratio_VH_ptV_150Tinf']
        rdecay = self['rGam_H_4l']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ttH_H_Zll_13')
    def mu_ttH_H_Zll_13(self):
        rprod = self['dsigma_ttH_13']/self['sigma_ttH_13_SM']
        rdecay = self['rGam_H_4l']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

class SMEFT_HiggsSignalStrengths_Run2_CMS_H_Zll(SM_HiggsProduction, SMEFT_HiggsProduction_STXS_1p1, SMEFT_HiggsProduction, SMEFT_HiggsDecay, SM_HiggsDecay):
    '''
    SMEFT predictions for Stage 1.1 STXS merged bins used in CMS H -> ZZ* -> 4l paper http://cds.cern.ch/record/2668684
    '''
    @prediction('mu_ggF0j_ptH_0T10_H_Zll_13')
    def mu_ggF0j_ptH_0T10_H_Zll_13(self):
        rprod = self['ratio_ggF0j_ptH_0T10']
        rdecay = self['dGam_H_Zll']/self['Gam_H_Zll_SM']
        rwidth = self['dGam_H']/self['Gam_H_SM']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggF0j_ptH_10T200_H_Zll_13')
    def mu_ggF0j_ptH_10T200_H_Zll_13(self):
        rprod = self['ratio_ggF0j_ptH_10T200']
        rdecay = self['dGam_H_Zll']/self['Gam_H_Zll_SM']
        rwidth = self['dGam_H']/self['Gam_H_SM']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggF1j_ptH_0T60_H_Zll_13')
    def mu_ggF1j_ptH_0T60_H_Zll_13(self):
        rprod = self['ratio_ggF1j_ptH_0T60']
        rdecay = self['dGam_H_Zll']/self['Gam_H_Zll_SM']
        rwidth = self['dGam_H']/self['Gam_H_SM']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggF1j_ptH_60T120_H_Zll_13')
    def mu_ggF1j_ptH_60T120_H_Zll_13(self):
        rprod = self['ratio_ggF1j_ptH_60T120']
        rdecay = self['dGam_H_Zll']/self['Gam_H_Zll_SM']
        rwidth = self['dGam_H']/self['Gam_H_SM']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggF1j_ptH_120T200_H_Zll_13')
    def mu_ggF1j_ptH_120T200_H_Zll_13(self):
        rprod = self['ratio_ggF1j_ptH_120T200']
        rdecay = self['dGam_H_Zll']/self['Gam_H_Zll_SM']
        rwidth = self['dGam_H']/self['Gam_H_SM']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggF_ptH_200Tinf_H_Zll_13')
    def mu_ggF_ptH_200Tinf_H_Zll_13(self):
        rprod = self['ratio_ggF_ptH_200Tinf']
        rdecay = self['dGam_H_Zll']/self['Gam_H_Zll_SM']
        rwidth = self['dGam_H']/self['Gam_H_SM']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggF2j_mjj_0T350_ptH_0T60_H_Zll_13')
    def mu_ggF2j_mjj_0T350_ptH_0T60_H_Zll_13(self):
        rprod = self['ratio_ggF2j_mjj_0T350_ptH_0T60']
        rdecay = self['dGam_H_Zll']/self['Gam_H_Zll_SM']
        rwidth = self['dGam_H']/self['Gam_H_SM']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggF2j_mjj_0T350_ptH_60T120_H_Zll_13')
    def mu_ggF2j_mjj_0T350_ptH_60T120_H_Zll_13(self):
        rprod = self['ratio_ggF2j_mjj_0T350_ptH_60T120']
        rdecay = self['dGam_H_Zll']/self['Gam_H_Zll_SM']
        rwidth = self['dGam_H']/self['Gam_H_SM']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggF2j_mjj_0T350_ptH_120T200_H_Zll_13')
    def mu_ggF2j_mjj_0T350_ptH_120T200_H_Zll_13(self):
        rprod = self['ratio_ggF2j_mjj_0T350_ptH_120T200']
        rdecay = self['dGam_H_Zll']/self['Gam_H_Zll_SM']
        rwidth = self['dGam_H']/self['Gam_H_SM']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggF2j_mjj_350Tinf_H_Zll_13')
    def mu_ggF2j_mjj_350Tinf_H_Zll_13(self):
        rprod = self['ratio_ggF2j_mjj_350Tinf']
        rdecay = self['dGam_H_Zll']/self['Gam_H_Zll_SM']
        rwidth = self['dGam_H']/self['Gam_H_SM']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_qqH_mjj_60T120_H_Zll_13')
    def mu_qqH_mjj_60T120_H_Zll_13(self):
        rprod = self['ratio_qqH_mjj_60T120']
        rdecay = self['dGam_H_Zll']/self['Gam_H_Zll_SM']
        rwidth = self['dGam_H']/self['Gam_H_SM']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_qqH_mjj_350T700_ptH_0T200_ptHjj_0T25_H_Zll_13')
    def mu_qqH_mjj_350T700_ptH_0T200_ptHjj_0T25_H_Zll_13(self):
        rprod = self['ratio_qqH_mjj_350T700_ptH_0T200_ptHjj_0T25']
        rdecay = self['dGam_H_Zll']/self['Gam_H_Zll_SM']
        rwidth = self['dGam_H']/self['Gam_H_SM']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_qqH_mjj_700Tinf_ptH_0T200_ptHjj_0T25_H_Zll_13')
    def mu_qqH_mjj_700Tinf_ptH_0T200_ptHjj_0T25_H_Zll_13(self):
        rprod = self['ratio_qqH_mjj_700Tinf_ptH_0T200_ptHjj_0T25']
        rdecay = self['dGam_H_Zll']/self['Gam_H_Zll_SM']
        rwidth = self['dGam_H']/self['Gam_H_SM']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_qqH_mjj_350Tinf_ptH_0T200_ptHjj_25Tinf_H_Zll_13')
    def mu_qqH_mjj_350Tinf_ptH_0T200_ptHjj_25Tinf_H_Zll_13(self):
        rprod = self['ratio_qqH_mjj_350Tinf_ptH_0T200_ptHjj_25Tinf']
        rdecay = self['dGam_H_Zll']/self['Gam_H_Zll_SM']
        rwidth = self['dGam_H']/self['Gam_H_SM']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_qqH_mjj_350Tinf_ptH_200Tinf_H_Zll_13')
    def mu_qqH_mjj_350Tinf_ptH_200Tinf_H_Zll_13(self):
        rprod = self['ratio_qqH_mjj_350Tinf_ptH_200Tinf']
        rdecay = self['dGam_H_Zll']/self['Gam_H_Zll_SM']
        rwidth = self['dGam_H']/self['Gam_H_SM']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_qqH_rest_H_Zll_13')
    def mu_qqH_rest_H_Zll_13(self):
        rprod = self['ratio_qqH_rest']
        rdecay = self['dGam_H_Zll']/self['Gam_H_Zll_SM']
        rwidth = self['dGam_H']/self['Gam_H_SM']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_VH_ptV_0T150_H_Zll_13')
    def mu_VH_ptV_0T150_H_Zll_13(self):
        rprod = self['ratio_VH_ptV_0T150']
        rdecay = self['dGam_H_Zll']/self['Gam_H_Zll_SM']
        rwidth = self['dGam_H']/self['Gam_H_SM']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_VH_ptV_150Tinf_H_Zll_13')
    def mu_VH_ptV_150Tinf_H_Zll_13(self):
        rprod = self['ratio_VH_ptV_150Tinf']
        rdecay = self['dGam_H_Zll']/self['Gam_H_Zll_SM']
        rwidth = self['dGam_H']/self['Gam_H_SM']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ttH_H_Zll_13')
    def mu_ttH_H_Zll_13(self):
        rprod = self['dsigma_ttH_13']/self['sigma_ttH_13_SM']
        rdecay = self['dGam_H_Zll']/self['Gam_H_Zll_SM']
        rwidth = self['dGam_H']/self['Gam_H_SM']
        return 1. + rprod + rdecay - rwidth

class SMEFT_HiggsSignalStrengths_Run2(SM_HiggsProduction, SMEFT_HiggsProduction_STXS_1p1, SMEFT_HiggsProduction, SMEFT_HiggsDecay, SM_HiggsDecay):
    '''
    Testing the production cross sections input above - will turn these into signal strength predictions 
    '''

    @prediction('ratio_WH0j_ptV_0T75')
    def ratio_WH0j_ptV_0T75(self):
        return self['ratio_WH0j_ptV_0T75']*self['dGam_H_bb']

    @prediction('ratio_WH0j_ptV_75T150')
    def ratio_WH0j_ptV_75T150(self):
        return self['ratio_WH0j_ptV_75T150']

    @prediction('ratio_WH0j_ptV_150T250')
    def ratio_WH0j_ptV_150T250(self):
        return self['ratio_WH0j_ptV_150T250']

    @prediction('ratio_WH1j_ptV_150T250')
    def ratio_WH1j_ptV_150T250(self):
        return self['ratio_WH1j_ptV_150T250']

    @prediction('ratio_WH0j_ptV_250Tinf')
    def ratio_WH0j_ptV_250Tinf(self):
        return self['ratio_WH0j_ptV_250Tinf']
        
    @prediction('ratio_ZH0j_ptV_0T75')
    def ratio_ZH0j_ptV_0T75(self):
        return self['ratio_ZH0j_ptV_0T75']*self['dGam_H_bb']

    @prediction('ratio_ZH0j_ptV_75T150')
    def ratio_ZH0j_ptV_75T150(self):
        return self['ratio_ZH0j_ptV_75T150']

    @prediction('ratio_ZH1j_ptV_150T250')
    def ratio_ZH1j_ptV_150T250(self):
        return self['ratio_ZH1j_ptV_150T250']

    @prediction('ratio_ZH0j_ptV_150T250')
    def ratio_ZH0j_ptV_150T250(self):
        return self['ratio_ZH0j_ptV_150T250']

    @prediction('ratio_ZH0j_ptV_250Tinf')
    def ratio_ZH0j_ptV_250Tinf(self):
        return self['ratio_ZH0j_ptV_250Tinf']

    @prediction('ratio_ZH1j_ptV_150T250')
    def ratio_ZH1j_ptV_150T250(self):
        return self['ratio_ZH1j_ptV_150T250']

    @prediction('ratio_qqH_mjj_60T120')
    def ratio_qqH_mjj_60T120(self):
        return self['ratio_qqH_mjj_60T120']


    @prediction('ratio_qqH_mjj_350T700_ptH_0T200_ptHjj_0T25')
    def ratio_qqH_mjj_350T700_ptH_0T200_ptHjj_0T25(self):
        return self['ratio_qqH_mjj_350T700_ptH_0T200_ptHjj_0T25']


    @prediction('ratio_qqH_mjj_700Tinf_ptH_0T200_ptHjj_0T25')
    def ratio_qqH_mjj_700Tinf_ptH_0T200_ptHjj_0T25(self):
        return self['ratio_qqH_mjj_700Tinf_ptH_0T200_ptHjj_0T25']


    @prediction('ratio_qqH_mjj_350Tinf_ptH_0T200_ptHjj_25Tinf')
    def ratio_qqH_mjj_350Tinf_ptH_0T200_ptHjj_25Tinf(self):
        return self['ratio_qqH_mjj_350Tinf_ptH_0T200_ptHjj_25Tinf']


    @prediction('ratio_qqH_mjj_350Tinf_ptH_200Tinf')
    def ratio_qqH_mjj_350Tinf_ptH_200Tinf(self):
        return self['ratio_qqH_mjj_350Tinf_ptH_200Tinf']


from ....fitlib.theory import internal_parameter, prediction

from ...SM.aEW_MZ_GF_5F.higgs import (
  Production as SM_HiggsProduction, 
  Decay_old as SM_HiggsDecay
)

from ..higgs.decay import HiggsDecay as SMEFT_HiggsDecay_new # new ones
from ..higgs.decay import SMEFT_HiggsDecay_old as SMEFT_HiggsDecay # old ones
from ..higgs.production import HiggsProduction as SMEFT_HiggsProduction
from ..higgs.STXS import STXS_1p0 as SMEFT_HiggsProduction_STXS_1p0


class SMEFT_HiggsSignalStrengths_Run2_CMS_H_aa_new(SM_HiggsProduction, SMEFT_HiggsProduction_STXS_1p0, SMEFT_HiggsProduction, SMEFT_HiggsDecay_new):

    @prediction('mu_ggF0j_H_aa_13')
    def mu_ggF0j_H_aa_13(self):
        rprod = self['ratio_ggF0j']
        rdecay = self['rGam_H_aa']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggF1j_ptH_0T60_H_aa_13')
    def mu_ggF1j_ptH_0T60_H_aa_13(self):
        rprod = self['ratio_ggF1j_ptH_0T60']
        rdecay = self['rGam_H_aa']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggF1j_ptH_60T120_H_aa_13')
    def mu_ggF1j_ptH_60T120_H_aa_13(self):
        rprod = self['ratio_ggF1j_ptH_60T120']
        rdecay = self['rGam_H_aa']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggF1j_ptH_120T200_H_aa_13')
    def mu_ggF1j_ptH_120T200_H_aa_13(self):
        rprod = self['ratio_ggF1j_ptH_120T200']
        rdecay = self['rGam_H_aa']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggF1j_ptH_200Tinf_H_aa_13')
    def mu_ggF1j_ptH_200Tinf_H_aa_13(self):
        rprod = self['ratio_ggF1j_ptH_200Tinf']
        rdecay = self['rGam_H_aa']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggF2j_ptH_0T60_H_aa_13')
    def mu_ggF2j_ptH_0T60_H_aa_13(self):
        rprod = self['ratio_ggF2j_ptH_0T60']
        rdecay = self['rGam_H_aa']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggF2j_ptH_60T120_H_aa_13')
    def mu_ggF2j_ptH_60T120_H_aa_13(self):
        rprod = self['ratio_ggF2j_ptH_60T120']
        rdecay = self['rGam_H_aa']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggF2j_ptH_120T200_H_aa_13')
    def mu_ggF2j_ptH_120T200_H_aa_13(self):
        rprod = self['ratio_ggF2j_ptH_120T200']
        rdecay = self['rGam_H_aa']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggF2j_ptH_200Tinf_H_aa_13')
    def mu_ggF2j_ptH_200Tinf_H_aa_13(self):
        rprod = self['ratio_ggF2j_ptH_200Tinf']
        rdecay = self['rGam_H_aa']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggF2j_mjj_400Tinf_ptH_0T200_H_aa_13')
    def mu_ggF2j_mjj_400Tinf_ptH_0T200_H_aa_13(self):
        rprod = self['ratio_ggF2j_mjj_400Tinf_ptH_0T200']
        rdecay = self['rGam_H_aa']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_qqH_other_H_aa_13')
    def mu_qqH_other_H_aa_13(self):
        rprod = self['ratio_qqH_other']
        rdecay = self['rGam_H_aa']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_qqH_mjj_400Tinf_ptj1_0T200_ptHjj_0T25_H_aa_13')
    def mu_qqH_mjj_400Tinf_ptj1_0T200_ptHjj_0T25_H_aa_13(self):
        rprod = self['ratio_qqH_VBFtopo']
        rdecay = self['rGam_H_aa']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_qqH_mjj_400Tinf_ptj1_0T200_ptHjj_25Tinf_H_aa_13')
    def mu_qqH_mjj_400Tinf_ptj1_0T200_ptHjj_25Tinf_H_aa_13(self):
        rprod = self['ratio_qqH_VBFtopo']
        rdecay = self['rGam_H_aa']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth
        
class SMEFT_HiggsSignalStrengths_Run2_CMS_H_aa(SM_HiggsProduction, SMEFT_HiggsProduction_STXS_1p0, SMEFT_HiggsProduction, SMEFT_HiggsDecay, SM_HiggsDecay):

    @prediction('mu_ggF0j_H_aa_13')
    def mu_ggF0j_H_aa_13(self):
        rprod = self['ratio_ggF0j']
        rdecay = self['dGam_H_aa']/self['Gam_H_aa_SM']
        rwidth = self['dGam_H']/self['Gam_H_SM']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggF1j_ptH_0T60_H_aa_13')
    def mu_ggF1j_ptH_0T60_H_aa_13(self):
        rprod = self['ratio_ggF1j_ptH_0T60']
        rdecay = self['dGam_H_aa']/self['Gam_H_aa_SM']
        rwidth = self['dGam_H']/self['Gam_H_SM']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggF1j_ptH_60T120_H_aa_13')
    def mu_ggF1j_ptH_60T120_H_aa_13(self):
        rprod = self['ratio_ggF1j_ptH_60T120']
        rdecay = self['dGam_H_aa']/self['Gam_H_aa_SM']
        rwidth = self['dGam_H']/self['Gam_H_SM']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggF1j_ptH_120T200_H_aa_13')
    def mu_ggF1j_ptH_120T200_H_aa_13(self):
        rprod = self['ratio_ggF1j_ptH_120T200']
        rdecay = self['dGam_H_aa']/self['Gam_H_aa_SM']
        rwidth = self['dGam_H']/self['Gam_H_SM']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggF1j_ptH_200Tinf_H_aa_13')
    def mu_ggF1j_ptH_200Tinf_H_aa_13(self):
        rprod = self['ratio_ggF1j_ptH_200Tinf']
        rdecay = self['dGam_H_aa']/self['Gam_H_aa_SM']
        rwidth = self['dGam_H']/self['Gam_H_SM']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggF2j_ptH_0T60_H_aa_13')
    def mu_ggF2j_ptH_0T60_H_aa_13(self):
        rprod = self['ratio_ggF2j_ptH_0T60']
        rdecay = self['dGam_H_aa']/self['Gam_H_aa_SM']
        rwidth = self['dGam_H']/self['Gam_H_SM']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggF2j_ptH_60T120_H_aa_13')
    def mu_ggF2j_ptH_60T120_H_aa_13(self):
        rprod = self['ratio_ggF2j_ptH_60T120']
        rdecay = self['dGam_H_aa']/self['Gam_H_aa_SM']
        rwidth = self['dGam_H']/self['Gam_H_SM']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggF2j_ptH_120T200_H_aa_13')
    def mu_ggF2j_ptH_120T200_H_aa_13(self):
        rprod = self['ratio_ggF2j_ptH_120T200']
        rdecay = self['dGam_H_aa']/self['Gam_H_aa_SM']
        rwidth = self['dGam_H']/self['Gam_H_SM']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggF2j_ptH_200Tinf_H_aa_13')
    def mu_ggF2j_ptH_200Tinf_H_aa_13(self):
        rprod = self['ratio_ggF2j_ptH_200Tinf']
        rdecay = self['dGam_H_aa']/self['Gam_H_aa_SM']
        rwidth = self['dGam_H']/self['Gam_H_SM']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggF2j_mjj_400Tinf_ptH_0T200_H_aa_13')
    def mu_ggF2j_mjj_400Tinf_ptH_0T200_H_aa_13(self):
        rprod = self['ratio_ggF2j_mjj_400Tinf_ptH_0T200']
        rdecay = self['dGam_H_aa']/self['Gam_H_aa_SM']
        rwidth = self['dGam_H']/self['Gam_H_SM']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_qqH_other_H_aa_13')
    def mu_qqH_other_H_aa_13(self):
        rprod = self['ratio_qqH_other']
        rdecay = self['dGam_H_aa']/self['Gam_H_aa_SM']
        rwidth = self['dGam_H']/self['Gam_H_SM']
        return 1. + rprod + rdecay - rwidth


    @prediction('mu_qqH_mjj_400Tinf_ptj1_0T200_ptHjj_0T25_H_aa_13')
    def mu_qqH_mjj_400Tinf_ptj1_0T200_ptHjj_0T25_H_aa_13(self):
        rprod = self['ratio_qqH_VBFtopo']
        rdecay = self['dGam_H_aa']/self['Gam_H_aa_SM']
        rwidth = self['dGam_H']/self['Gam_H_SM']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_qqH_mjj_400Tinf_ptj1_0T200_ptHjj_25Tinf_H_aa_13')
    def mu_qqH_mjj_400Tinf_ptj1_0T200_ptHjj_25Tinf_H_aa_13(self):
        rprod = self['ratio_qqH_VBFtopo']
        rdecay = self['dGam_H_aa']/self['Gam_H_aa_SM']
        rwidth = self['dGam_H']/self['Gam_H_SM']
        return 1. + rprod + rdecay - rwidth
from ...fitlib.constants import pi, GeV2pb 
from ...fitlib.theory import internal_parameter, prediction
from ..EFT import EFTBase, EFTPrediction, EFTInternalParameter
from .inputs import aEW_MZ_GF_5F_U2_2_U3_3
from . import data_path

json_path = data_path+'Diboson/'

class LHC(EFTBase, aEW_MZ_GF_5F_U2_2_U3_3):
    '''
    SMEFT Warsaw basis for Diboson production, based on "SMEFT_global_fit_warsaw.nb". 
    '''

    mu_WW_enumunu_ptl1G120_13 = EFTPrediction.from_json(
      json_path+'mu_WW_enumunu_ptl1G120_ATLAS_13.json', linear_only=True
    )
    
    fidmu_WW_enumunu_pt300_Run2_ATLAS13 = EFTPrediction.from_json(
      json_path+'fidmu_WW_enumunu_pt300_Run2_ATLAS13.json', linear_only=True
    )
    
    # ATLAS WW pTl1 distribution
    fidmu_WW_enumunu_ptl_ATLAS13 = EFTPrediction.from_json(
      json_path+'fidmu_WW_enumunu_ptl_ATLAS13.json', linear_only=True
    )
    # CMS Zjj dphi_jj distribution
    r_dsig_dphijj_Zjj_ATLAS_13TeV = EFTInternalParameter.from_json(json_path+'r_dsig_dphijj_Zjj_ATLAS_13TeV.json', linear_only=True)

    # ATLAS WZ pTZ distribution
    mu_dsig_dpTZ_WZ_ATLAS_13TeV = EFTPrediction.from_json(
      json_path+'mu_dsig_dpTZ_WZ_ATLAS_13TeV.json', linear_only=True
    )

    # CMS WZ pTZ distribution (normalised)
    mu_dsig_dpTZ_norm_WZ_CMS_13TeV = EFTPrediction.from_json(
      json_path+'mu_dsig_dpTZ_norm_WZ_CMS_13TeV.json', linear_only=True
    )
    
    @prediction('mu_dsig_dphijj_Zjj_ATLAS_13TeV')
    def mu_dsig_dphijj_Zjj_ATLAS_13TeV(self):
        rprod = self['r_dsig_dphijj_Zjj_ATLAS_13TeV']
        rZdecay = self['dGam_Z_ll']/self['Gam_Z_ll_SM']
        rZwidth = self['dGam_Z']/self['Gam_Z_SM']
        return 1. + rprod + rZdecay - rZwidth 
        


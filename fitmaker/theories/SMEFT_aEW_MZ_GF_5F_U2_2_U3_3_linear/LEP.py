import numpy as np
from numpy import sqrt
from ...fitlib.constants import pi, GeV2pb
from ...fitlib.theory import internal_parameter, prediction

from ..SM.aEW_MZ_GF_5F.EWPO import Tree_Fixed as SM_aEW_MZ_GF_5F_Fixed, LEP_EWWG as SM_aEW_MZ_GF_5F_EWPO
from ..SM.aEW_MZ_GF_5F.diboson import LEP as SM_aEW_MZ_GF_5F_Diboson_LEP

from ..EFT import EFTBase
from .inputs import aEW_MZ_GF_5F_U2_2_U3_3

# Generic 'core shifts'
class Deltas(SM_aEW_MZ_GF_5F_Fixed):
    '''
    Model of SM plus EW parameter and coupling shifts, based on
    SMEFT_global_fit_warsaw.nb.
    Modified for b-quark specific couplings in U(2)^2 x U(3)^5 flavor scenario.
    '''
    external_parameters = [
      # EW parameter
      'dGF', 'dMZ2', 'dMW2', 'dv', 'dgp', 'dgw', 'dsw2',
      # Z couplings
      'dgZ', 'dgVl', 'dgAl', 'dgVv', 'dgAv',
      'dgVu', 'dgAu', 'dgVd', 'dgAd',
      'dgVb', 'dgAb', # b-specific
      # W couplings
      'dgVWl', 'dgVWq',
      # TGCs
      'dg1A', 'dg1Z', 'dkA', 'dkZ', 'dlamA', 'dlamZ'
    ]

    @internal_parameter('dgVWl')
    def dgAWl(self):
        return self['dgVWl']

    @internal_parameter('dgVWq')
    def dgAWq(self):
        return self['dgVWq']

    @internal_parameter(depends_on=('Gam_Z_fac', 'sw', 'dgAl', 'dgVl'))
    def dGam_Z_ll(self):
        cgA, cgV = -1., ( -1. + 4.*self['sw']**2 )
        return self['Gam_Z_fac']/6.*(cgA*self['dgAl'] +  cgV*self['dgVl'])

    @internal_parameter(depends_on=('Gam_Z_fac', 'dgAv', 'dgVv'))
    def dGam_Z_vv(self):
        return self['Gam_Z_fac']/6.*(self['dgAv'] +  self['dgVv'])

    @internal_parameter(depends_on=('Gam_Z_fac', 'sw', 'dgAu', 'dgVu'))
    def dGam_Z_uu(self):
        cgA, cgV = 1., ( 1. - (8./3.)*self['sw']**2 )
        return self['Gam_Z_fac']/2.*(cgA*self['dgAu'] +  cgV*self['dgVu'])

    @internal_parameter(depends_on=('Gam_Z_fac', 'sw', 'dgAd', 'dgVd'))
    def dGam_Z_dd(self):
        cgA, cgV = -1., ( -1. + (4./3.)*self['sw']**2 )
        return self['Gam_Z_fac']/2.*(cgA*self['dgAd'] +  cgV*self['dgVd'])

    @internal_parameter(depends_on=('Gam_Z_fac', 'sw', 'dgAb', 'dgVb'))
    def dGam_Z_bb(self):
        cgA, cgV = -1., ( -1. + (4./3.)*self['sw']**2 )
        return self['Gam_Z_fac']/2.*(cgA*self['dgAb'] +  cgV*self['dgVb'])

    @internal_parameter(depends_on=('dGam_Z_uu', 'dGam_Z_dd', 'dGam_Z_bb'))
    def dGam_Z_had(self):
        return 2.*(self['dGam_Z_uu'] + self['dGam_Z_dd']) + self['dGam_Z_bb']

    @internal_parameter(depends_on=('dGam_Z_had', 'dGam_Z_ll', 'dGam_Z_vv'))
    def dGam_Z(self):
        return 3.*self['dGam_Z_ll'] + 3.*self['dGam_Z_vv'] + self['dGam_Z_had']

    @internal_parameter(depends_on=('MW', 'dMW2', 'Gam_W_qq_SM', 'dgVWq'))
    def dGam_W_qq(self):
        return self['Gam_W_qq_SM']*(4.*self['dgVWq'] + self['dMW2']/self['MW']**2/2.)

    @internal_parameter(depends_on=('MW', 'dMW2', 'Gam_W_lv_SM', 'dgVWl'))
    def dGam_W_lv(self):
        return self['Gam_W_lv_SM']*(4.*self['dgVWl'] + self['dMW2']/self['MW']**2/2.)

    @internal_parameter(depends_on=('MW','dMW2','Gam_W_SM', 'dgVWl', 'dgVWq'))
    def dGam_W(self):
        return self['Gam_W_SM']*(
          (4./3.)*self['dgVWl'] + (8./3.)*self['dgVWq'] +
          self['dMW2']/self['MW']**2/2.
        )

# EWPO in terms of 'core shifts'
class Deltas_EWPO(Deltas, SM_aEW_MZ_GF_5F_EWPO):
    '''
    Model of SM plus EW parameter and coupling shifts, based on
    SMEFT_global_fit_warsaw.nb. Predictions for LEP Z-pole observables in terms
    of the shifts at linear order.
    '''
    @prediction('W_mass')
    def W_mass(self):
        MW_SM = self.predict_baseclass(SM_aEW_MZ_GF_5F_EWPO, 'W_mass')
        return MW_SM+self['dMW2']/self['MW']/2.

    @prediction('Gam_Z')
    def Gam_Z(self):
        '''Z width.'''
        Gam_Z_SM = self.predict_baseclass(SM_aEW_MZ_GF_5F_EWPO, 'Gam_Z')
        return  Gam_Z_SM + self['dGam_Z']

    @prediction('sigma_had_0')
    def sigma_had_0(self):
        '''Hadronic cross section [nb].'''
        sigma_SM = self.predict_baseclass(SM_aEW_MZ_GF_5F_EWPO, 'sigma_had_0')

        fac = GeV2pb*12.*pi/self['MZ']**2/self['Gam_Z_SM']**3/1000.

        dsigma = fac*(
          self['Gam_Z_had_SM']*self['Gam_Z_SM']*self['dGam_Z_ll'] +
          self['Gam_Z_ll_SM']*self['Gam_Z_SM']*self['dGam_Z_had'] -
          2.*self['Gam_Z_had_SM']*self['Gam_Z_ll_SM']*self['dGam_Z']
        )

        return sigma_SM + dsigma

    @prediction('Rl_0')
    def Rl_0(self):
        '''Hadronic to leptonic cross section ratio.'''
        Rl_SM = self.predict_baseclass(SM_aEW_MZ_GF_5F_EWPO, 'Rl_0')

        dRl = (
          self['Gam_Z_ll_SM']*self['dGam_Z_had'] -
          self['Gam_Z_had_SM']*self['dGam_Z_ll']
        )/self['Gam_Z_ll_SM']**2

        return Rl_SM + dRl

    @prediction('Rb_0')
    def Rb_0(self):
        '''b-quark to hadronic cross section ratio.'''
        Rb_SM = self.predict_baseclass(SM_aEW_MZ_GF_5F_EWPO, 'Rb_0')

        dRb = (
          self['Gam_Z_had_SM']*self['dGam_Z_bb'] -
          self['Gam_Z_dd_SM']*self['dGam_Z_had']
        )/self['Gam_Z_had_SM']**2

        return Rb_SM + dRb

    @prediction('Rc_0')
    def Rc_0(self):
        '''c-quark to hadronic cross section ratio.'''
        Rc_SM = self.predict_baseclass(SM_aEW_MZ_GF_5F_EWPO, 'Rc_0')

        dRc = (
          self['Gam_Z_had_SM']*self['dGam_Z_uu'] -
          self['Gam_Z_uu_SM']*self['dGam_Z_had']
        )/self['Gam_Z_had_SM']**2

        return Rc_SM + dRc

    @internal_parameter(depends_on=('gVlSM','gAlSM','dgAl','dgVl'))
    def dAl(self):
        gV, gA = self['gVlSM'],  self['gAlSM']
        fac = 2.*(gV**2 - gA**2)/(gV**2 + gA**2)**2
        return fac*( gV*self['dgAl'] - gA*self['dgVl'] )

    @prediction('Al')
    def Al(self):
        '''lepton chiral coupling parameter.'''
        A = self.predict_baseclass(SM_aEW_MZ_GF_5F_EWPO, 'Al')
        return A + self['dAl']

    @internal_parameter(depends_on=('gVdSM','gAdSM','dgAb','dgVb'))
    def dAb(self):
        gV, gA = self['gVdSM'],  self['gAdSM']
        fac = 2.*(gV**2 - gA**2)/(gV**2 + gA**2)**2
        return fac*( gV*self['dgAb'] - gA*self['dgVb'] )

    @prediction('Ab')
    def Ab(self):
        '''b-quark chiral coupling parameter.'''
        A = self.predict_baseclass(SM_aEW_MZ_GF_5F_EWPO, 'Ab')
        return A + self['dAb']

    @internal_parameter(depends_on=('gVuSM','gAuSM','dgAu','dgVu'))
    def dAc(self):
        gV, gA = self['gVuSM'],  self['gAuSM']
        fac = 2.*(gV**2 - gA**2)/(gV**2 + gA**2)**2
        return fac*( gV*self['dgAu'] - gA*self['dgVu'] )

    @prediction('Ac')
    def Ac(self):
        '''c-quark chiral coupling parameter.'''
        A = self.predict_baseclass(SM_aEW_MZ_GF_5F_EWPO, 'Ac')
        return A + self['dAc']

    @prediction('AFB_l')
    def AFB_l(self):
        '''Leptonic forward-backward asymmetry.'''
        AFB = self.predict_baseclass(SM_aEW_MZ_GF_5F_EWPO, 'AFB_l')
        return AFB + (3./2.)*self['Al_SM']*self['dAl']

    @prediction('AFB_b')
    def AFB_b(self):
        '''b-quark forward-backward asymmetry.'''
        AFB = self.predict_baseclass(SM_aEW_MZ_GF_5F_EWPO, 'AFB_b')
        return AFB + (3./4.)*(self['Al_SM']*self['dAb']+self['Ab_SM']*self['dAl'])

    @prediction('AFB_c')
    def AFB_c(self):
        '''c-quark forward-backward asymmetry.'''
        AFB = self.predict_baseclass(SM_aEW_MZ_GF_5F_EWPO, 'AFB_c')
        return AFB + (3./4.)*(self['Al_SM']*self['dAc']+self['Ac_SM']*self['dAl'])

# LEP Diboson in terms of 'core shifts'
class Deltas_Diboson_LEP(Deltas, SM_aEW_MZ_GF_5F_Diboson_LEP):
    '''
    Model of SM plus EW parameter and coupling shifts, based on
    SMEFT_global_fit_warsaw.nb. Predictions for LEP Diboson observables in terms
    of the shifts at linear order.
    '''
    # W+W- CC03 cross section predictions from paramter shifts (see 1606.06693)
    def dCC03_vec(self, i, f):
        '''
        Vector of parameter shifts needed to obtain predictions for LEP II
        W+W- (CC03) cross section measurements from 1606.06693, Table 2.
        Energies: 188.6, 191.6, 195.5, 199.5, 201.6, 204.8, 206.5, 208.0 GeV
        '''
        return np.array([
          -self['dMW2']/self['MW']**2, # dMW2 has a minus sign w.r.t 1706.08945
          self['dGam_W']/self['Gam_W_SM'],
          self['dgVWl'],
          (self['dgVW'+i]+self['dgVW'+f])/2.,
          self['dgVl'], self['dgAl'],
          self['dg1Z'], self['dkA'], self['dkZ'], self['dlamA'], self['dlamZ']
        ])

    def dCC03_coeffs(self):
        '''
        Matrix of coefficients needed to obtain predictions for LEP II
        W+W- cross section measurements from 1606.06693, Table 2.
        Energies: 188.6, 191.6, 195.5, 199.5, 201.6, 204.8, 206.5, 208.0 GeV
        '''
        return np.array([
          [  2.6 , -17., 72., 34., 5.3, 0.3, -0.08, -0.50, -0.19, -0.29,  0.026 ],
          [  1.6 , -17., 73., 34., 5.8, 0.4, -0.10, -0.56, -0.22, -0.32,  0.018 ],
          [  0.26, -17., 74., 34., 6.5, 0.6, -0.12, -0.64, -0.27, -0.36,  0.005 ],
          [ -0.54, -17., 75., 34., 7.1, 0.8, -0.15, -0.71, -0.31, -0.40, -0.009 ],
          [ -0.97, -17., 75., 34., 7.4, 0.9, -0.16, -0.75, -0.33, -0.42, -0.017 ],
          [ -1.4 , -17., 75., 34., 7.8, 1.0, -0.18, -0.80, -0.37, -0.44, -0.029 ],
          [ -1.8 , -17., 76., 34., 8.0, 1.1, -0.19, -0.83, -0.39, -0.46, -0.036 ],
          [ -2.0 , -17., 76., 34., 8.2, 1.2, -0.20, -0.85, -0.40, -0.47, -0.042 ]
        ])

    @internal_parameter(depends_on=(
      'MW','dMW2','dGam_W','Gam_W_SM','dgVWl','dgVWq','dgVl','dgAl','dg1Z','dkA',
      'dkZ','dlamA','dlamZ'
    ))
    def dCC03_eelvqq(self):
        return self.dCC03_coeffs().dot(self.dCC03_vec('l','q'))

    @internal_parameter(depends_on=(
      'MW','dMW2','dGam_W','Gam_W_SM','dgVWl','dgVWq','dgVl','dgAl','dg1Z','dkA',
      'dkZ','dlamA','dlamZ'
    ))
    def dCC03_eeqqqq(self):
        return self.dCC03_coeffs().dot(self.dCC03_vec('q','q'))*1.01

    @internal_parameter(depends_on=(
      'MW','dMW2','dGam_W','Gam_W_SM','dgVWl','dgVl','dgAl','dg1Z','dkA',
      'dkZ','dlamA','dlamZ'
    ))
    def dCC03_eelvlv(self):
        return np.dot(self.dCC03_coeffs(), self.dCC03_vec('l','l'))/4.04

    @prediction('WW_xs_lvlv_ALEPH')
    def WW_xs_lvlv_ALEPH(self):
        SM = self.predict_baseclass(SM_aEW_MZ_GF_5F_Diboson_LEP, 'WW_xs_lvlv_ALEPH')
        return SM + self['dCC03_eelvlv'][:-1]

    @prediction('WW_xs_lvqq_ALEPH')
    def WW_xs_lvqq_ALEPH(self):
        SM = self.predict_baseclass(SM_aEW_MZ_GF_5F_Diboson_LEP, 'WW_xs_lvqq_ALEPH')
        return SM + self['dCC03_eelvqq'][:-1]

    @prediction('WW_xs_qqqq_ALEPH')
    def WW_xs_qqqq_ALEPH(self):
        SM = self.predict_baseclass(SM_aEW_MZ_GF_5F_Diboson_LEP, 'WW_xs_qqqq_ALEPH')
        return SM + self['dCC03_eeqqqq'][:-1]

    @prediction('WW_xs_lvlv_OPAL')
    def WW_xs_lvlv_OPAL(self):
        SM = self.predict_baseclass(SM_aEW_MZ_GF_5F_Diboson_LEP, 'WW_xs_lvlv_OPAL')
        return SM + self['dCC03_eelvlv'][:-1]

    @prediction('WW_xs_lvqq_OPAL')
    def WW_xs_lvqq_OPAL(self):
        SM = self.predict_baseclass(SM_aEW_MZ_GF_5F_Diboson_LEP, 'WW_xs_lvqq_OPAL')
        return SM + self['dCC03_eelvqq'][:-1]

    @prediction('WW_xs_qqqq_OPAL')
    def WW_xs_qqqq_OPAL(self):
        SM = self.predict_baseclass(SM_aEW_MZ_GF_5F_Diboson_LEP, 'WW_xs_qqqq_OPAL')
        return SM + self['dCC03_eeqqqq'][:-1]

    @prediction('WW_xs_lvlv_L3')
    def WW_xs_lvlv_L3(self):
        SM = self.predict_baseclass(SM_aEW_MZ_GF_5F_Diboson_LEP, 'WW_xs_lvlv_L3')
        return SM + self['dCC03_eelvlv']

    @prediction('WW_xs_lvqq_L3')
    def WW_xs_lvqq_L3(self):
        SM = self.predict_baseclass(SM_aEW_MZ_GF_5F_Diboson_LEP, 'WW_xs_lvqq_L3')
        return SM + self['dCC03_eelvqq']

    @prediction('WW_xs_qqqq_L3')
    def WW_xs_qqqq_L3(self):
        SM = self.predict_baseclass(SM_aEW_MZ_GF_5F_Diboson_LEP, 'WW_xs_qqqq_L3')
        return SM + self['dCC03_eeqqqq']

    # W+W- polar angle differential cross section measurements from LEP
    def dWW_costheta_LEPII_coeffs(self):
        '''
        Matrix of coefficients needed to obtain predictions for LEP II
        W+W- cos(theta) differential cross section measurements.
        Bins 1,4,7 & 10 at 182.66 & 205.92 GeV, from 1606.06693, Table 3.
        '''
        return np.array([
          [  -1.6 , -1.5 ,  12.  ,   2.9 ,  4.1 ,   3.0 ,  -0.44,  -0.34,  -0.47,  -0.32,  -0.45  ],
          [  -1.5 , -2.8 ,  16.  ,   5.5 ,  3.5 ,   2.2 ,  -0.30,  -0.32,  -0.39,  -0.26,  -0.34  ],
          [   0.16, -5.3 ,  22.  ,  10.  ,  1.5 ,   0.2 ,  -0.04,  -0.14,  -0.06,  -0.06,   0.026 ],
          [  18.  , -14. ,  39.  ,  27.  , -7.7 ,  -8.8 ,   1.2 ,   0.62,   1.3 ,   0.63,   1.3   ],
          [  -1.1 , -0.9 ,  11.  ,   1.8 ,  4.9 ,   3.0 ,  -0.44,  -0.44,  -0.50,  -0.40,  -0.46  ],
          [  -1.7 , -2.1 ,  15.  ,   4.1 ,  5.0 ,   2.8 ,  -0.34,  -0.53,  -0.55,  -0.37,  -0.41  ],
          [  -2.3 , -4.6 ,  22.  ,   9.0 ,  3.5 ,   1.2 ,  -0.19,  -0.35,  -0.25,  -0.19,  -0.086 ],
          [  10.  , -20. ,  59.  ,  39.  , -9.6 , -11.0 ,   1.5 ,  -0.86,   1.7 ,   0.90,   1.7   ]
        ])

    @internal_parameter(depends_on=(
      'MW','dMW2','dGam_W','Gam_W_SM','dgVWl','dgVl','dgAl','dg1Z','dkA',
      'dkZ','dlamA','dlamZ'
    ))
    def dWW_costheta_LEPII(self):
        return np.dot(self.dWW_costheta_LEPII_coeffs(), self.dCC03_vec('l','q'))

    @prediction('WW_costheta_LEPII')
    def WW_costheta_LEPII(self):
        SM = self.predict_baseclass(SM_aEW_MZ_GF_5F_Diboson_LEP, 'WW_costheta_LEPII')
        return SM + self['dWW_costheta_LEPII']

# Map of Warsaw basis coefficients to core shifts
class SMEFT_Deltas(EFTBase, Deltas, aEW_MZ_GF_5F_U2_2_U3_3):
    '''
    SMEFT Warsaw basis for EWPO, based on "SMEFT_global_fit_warsaw.nb". Maps
    Wilson coefficients at linear order to the parameter and coupling shifts
    of SM_deltas.
    '''
    # 'CHWB', 'CHD', 'Cll', 'CHbox', 'CHG', 'CHW', 'CHB', 'CW', 'CG', # bosonic
    # 'CHl3', 'CHl1', 'CHe', # flavor universal lepton currents
    # 'CHq3', 'CHq1', 'CHu', 'CHd', # Currents for 1st 2 gens.
    # 'CHQ3', 'CHQ1', 'CHt', # Currents for 3rd gen.
    # 'CtaH', 'CtH', 'CbH' # 3rd gen. Yukawas
    # 'CtG', 'CtW', 'CtB', # top-specific dipoles
    #  # interfering two-heavy-two-light 4-quark operators
    # 'CQq38', 'CQq18', 'CQu8', 'CQd8', 'Ctq8', 'Ctu8', 'Ctd8'
    #  # interfering four-heavy 4-quark operators
    # 'CQQ8', 'CQt8', 'CQb8', 'CQQ1', 'CQt1', 'CQb1', 'Ctb8', 'Ctt'

    # EW param shifts
    @internal_parameter(depends_on=('GF', 'CHl3', 'Cll'))
    def dGF(self):
        return (self['CHl3']-self['Cll']/2.)/self['GF']/self.Lambda**2

    @internal_parameter(depends_on=('MZ','GF','aEW','CHD','CHWB'))
    def dMZ2(self):
        aHD = self['MZ']**2/self['GF']/(2.*sqrt(2.))
        aHWB = 2.**(1./4)*sqrt(pi*self['aEW'])*self['MZ']/self['GF']**(3./2.)
        return (aHD*self['CHD'] +  aHWB*self['CHWB'])/self.Lambda**2

    @internal_parameter(depends_on=('MW','s2w','c2w','sw','cw','vev','CHD','CHWB','Cll','CHl3'))
    def dMW2(self):
        fac = -self['MW']**2*self['s2w']/self['c2w']*self['vev']**2/4.
        aHD, aCl = self['cw']/self['sw'], self['sw']/self['cw']
        return (
          aHD*self['CHD'] + aCl*(4.*self['CHl3']-2.*self['Cll'])
          + 4.*self['CHWB']
        )*fac/self.Lambda**2

    @internal_parameter(depends_on=('GF','dGF'))
    def dv(self):
        return self['dGF']/self['GF']/2.

    @internal_parameter(depends_on=('vev','c2w','s2w','sw','gp','CHWB','CHD','Cll','CHl3'))
    def dgp(self):
        fac = self['gp']*self['vev']**2/self['c2w']/4.
        Cbar = self['CHD'] + 4.*self['CHl3'] - 2.*self['Cll']
        return fac*( self['sw']**2*Cbar + 2.*self['s2w']*self['CHWB'] )/self.Lambda**2

    @internal_parameter(depends_on=('vev','c2w','s2w','cw','gw','CHWB','CHD','Cll','CHl3'))
    def dgw(self):
        fac = -self['gw']*self['vev']**2/self['c2w']/4.
        Cbar = self['CHD'] + 4.*self['CHl3'] - 2.*self['Cll']
        return fac*( self['cw']**2*Cbar + 2.*self['s2w']*self['CHWB'] )/self.Lambda**2

    @internal_parameter(depends_on=('GF','c2w','s2w','CHWB','CHD','Cll','CHl3'))
    def dsw2(self):
        fac = self['s2w']/self['c2w']/(8.*sqrt(2.)*self['GF'])
        Cbar = self['CHD'] + 4.*self['CHl3'] - 2.*self['Cll']
        return fac*( self['s2w']*Cbar + 4.*self['CHWB'] )/self.Lambda**2

    # Z coupling shifts
    @internal_parameter(depends_on=('MZ','GF','s2w','dMZ2','dGF','CHWB'))
    def dgZ(self):
    # 2020/06/05: sign error fixed in second term, now matches 1709.08945
    # Validated against SMEFTsim & SMEFTatNLO
        return (
          - self['dGF']/sqrt(2.) - self['dMZ2']/(2.*self['MZ']**2)
          + self['s2w']/(2.*sqrt(2.)*self['GF'])*self['CHWB']/self.Lambda**2
        )

    @internal_parameter(depends_on=('GF','gVlSM','dgZ','dsw2','CHe','CHl1','CHl3'))
    def dgVl(self):
        return (
          self['dgZ']*self['gVlSM']
          - (self['CHe']+self['CHl1']+self['CHl3'])/(4.*sqrt(2.)*self['GF'])/self.Lambda**2
          + self['dsw2']
        )

    @internal_parameter(depends_on=('GF','gAlSM','dgZ','CHe','CHl1','CHl3'))
    def dgAl(self):
        return (
          self['dgZ']*self['gAlSM']
          + (self['CHe']-self['CHl1']-self['CHl3'])/(4.*sqrt(2.)*self['GF'])/self.Lambda**2
        )

    @internal_parameter(depends_on=('GF','gVvSM','dgZ','CHl1','CHl3'))
    def dgVv(self):
        return (
          self['dgZ']*self['gVvSM']
          - (self['CHl1']-self['CHl3'])/(4.*sqrt(2.)*self['GF'])/self.Lambda**2
        )

    @internal_parameter(depends_on=('dgVv',))
    def dgAv(self):
        return self['dgVv']

    @internal_parameter(depends_on=('GF','gVuSM','dgZ','dsw2','CHu','CHq1','CHq3'))
    def dgVu(self):
        return (
          self['dgZ']*self['gVuSM']
          + (-self['CHq1']+self['CHq3']-self['CHu'])/(4.*sqrt(2.)*self['GF'])/self.Lambda**2
          - (2./3.)*self['dsw2']
        )

    @internal_parameter(depends_on=('GF','gAuSM','dgZ','CHu','CHq1','CHq3'))
    def dgAu(self):
        return (
          self['dgZ']*self['gAuSM']
          - (self['CHq1']-self['CHq3']-self['CHu'])/(4.*sqrt(2.)*self['GF'])/self.Lambda**2
        )

    @internal_parameter(depends_on=('GF','gVdSM','dgZ','dsw2','CHd','CHq1','CHq3'))
    def dgVd(self):
        return (
          self['dgZ']*self['gVdSM']
          - (self['CHq1']+self['CHq3']+self['CHd'])/(4.*sqrt(2.)*self['GF'])/self.Lambda**2
          + 1./3.*self['dsw2']
        )

    @internal_parameter(depends_on=('GF','gAdSM','dgZ','CHd','CHq1','CHq3'))
    def dgAd(self):
        return (
          self['dgZ']*self['gAdSM']
          + (-self['CHq1']-self['CHq3']+self['CHd'])/(4.*sqrt(2.)*self['GF'])/self.Lambda**2
        )

    @internal_parameter(depends_on=('GF','gVdSM','dgZ','dsw2','CHd','CHQ1','CHQ3'))
    def dgVb(self):
        return (
          self['dgZ']*self['gVdSM']
          - (self['CHQ1']+self['CHQ3']+self['CHd'])/(4.*sqrt(2.)*self['GF'])/self.Lambda**2
          + 1./3.*self['dsw2']
        )

    @internal_parameter(depends_on=('GF','gAdSM','dgZ','CHd','CHQ1','CHQ3'))
    def dgAb(self):
        return (
          self['dgZ']*self['gAdSM']
          + (-self['CHQ1']-self['CHQ3']+self['CHd'])/(4.*sqrt(2.)*self['GF'])/self.Lambda**2
        )

    # W coupling shifts
    @internal_parameter(depends_on=('GF','cw','sw','dsw2','CHl3','CHWB'))
    def dgVWl(self):
        Cbar = self['CHl3'] + self['cw']/self['sw']*self['CHWB']/2.
        return Cbar/(2.*sqrt(2.)*self['GF'])/self.Lambda**2 - self['dsw2']/self['sw']**2/4.

    @internal_parameter(depends_on=('GF','cw','sw','dsw2','CHq3','CHWB'))
    def dgVWq(self):
        Cbar = self['CHq3'] + self['cw']/self['sw']*self['CHWB']/2.
        return Cbar/(2.*sqrt(2.)*self['GF'])/self.Lambda**2 - self['dsw2']/self['sw']**2/4.

    # TGC shifts
    @internal_parameter()
    def dg1A(self):
        return 0.

    @internal_parameter(depends_on=('GF','s2w','CHWB','dsw2'))
    def dg1Z(self):
        return (
          self['CHWB']/(self['s2w']*sqrt(2.)*self['GF'])/self.Lambda**2
          -2.*self['dsw2']/self['s2w']**2
        )

    @internal_parameter(depends_on=('GF','cw','sw','CHWB'))
    def dkA(self):
        return self['CHWB']*self['cw']/self['sw']/(sqrt(2.)*self['GF'])/self.Lambda**2

    @internal_parameter(depends_on=('GF','s2w','c2w','CHWB','dsw2'))
    def dkZ(self):
        return (
          self['CHWB']*self['c2w']/self['s2w']/(sqrt(2.)*self['GF'])/self.Lambda**2
          -2.*self['dsw2']/self['s2w']**2
        )

    @internal_parameter(depends_on=('MW','sw','gAWWSM','CW'))
    def dlamA(self):
        return 6.*self['sw']*self['MW']**2/self['gAWWSM']*self['CW']/self.Lambda**2

    @internal_parameter(depends_on=('MW','cw','gZWWSM','CW'))
    def dlamZ(self):
        return 6.*self['cw']*self['MW']**2/self['gZWWSM']*self['CW']/self.Lambda**2

# EWPO in terms of Warsaw basis
class SMEFT_EWPO(SMEFT_Deltas, Deltas_EWPO):
    '''
    SMEFT Warsaw basis for EWPO, based on "SMEFT_global_fit_warsaw.nb".
    Core shifts + map from Warsaw basis.
    '''
    pass

# LEP Diboson in terms of Warsaw basis
class SMEFT_Diboson_LEP(SMEFT_Deltas, Deltas_Diboson_LEP):
    '''
    SMEFT Warsaw basis for LEP Diboson, based on "SMEFT_global_fit_warsaw.nb".
    Core shifts + map from Warsaw basis.
    '''
    pass

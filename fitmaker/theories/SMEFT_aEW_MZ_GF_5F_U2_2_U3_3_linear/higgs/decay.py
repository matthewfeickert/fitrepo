from ....fitlib.constants import pi, GeV2pb 
from ....fitlib.theory import internal_parameter, prediction

from ..LEP import SMEFT_Deltas as EWPO
from ...SM.aEW_MZ_GF_5F.higgs import Decay as SM_aEW_MZ_GF_5F_HiggsDecay
from ...SM.aEW_MZ_GF_5F.higgs import Decay_old as SM_aEW_MZ_GF_5F_HiggsDecay_old
from ...EFT import EFTBase, EFTPrediction, EFTInternalParameter

from .. import data_path
from ..inputs import aEW_MZ_GF_5F_U2_2_U3_3
from numpy import sqrt
json_path = data_path+'HiggsDecay/'

rename = {
  "CuH":"CtH", 
  "CuG":"CtG"
}

mpole = {
  3:  0.096,# yms
  # 4:  1.28, # ymc
  4:  0.907, # ymc
  # 5:  4.18, # ymb
  5:  3.237, # ymb
  13: 0.10566, # ymm 
  15: 1.777 # ymtau 
}
vev = aEW_MZ_GF_5F_U2_2_U3_3()['vev']
ypole = { i: sqrt(2.)*m/vev for i,m in mpole.items()}

class HiggsDecay(EFTBase, SM_aEW_MZ_GF_5F_HiggsDecay, aEW_MZ_GF_5F_U2_2_U3_3):
    '''
    SMEFT Warsaw basis for Higgs decay, based on "SMEFT_global_fit_warsaw.nb" 
    (SMEFTsim_A_U35_alphaScheme_UFO_v2_1 with input parameters fixed to match 
    aEW_MZ_GF_5F_Fixed) & 1906.06949. Relative impact on H->gg decay calculated 
    with SMEFTatNLO.
    '''
    # H -> g g
    # Same coefficients as STXS/mu_ggF0j.json without the constant "1"
    rGam_H_gg = EFTInternalParameter.from_json(
      json_path+'rGam_H_gg.json', 
      linear_only=True, 
      rename=rename
    )
    
    @internal_parameter(depends_on=('rGam_H_gg', 'Gam_H_gg_SM'))
    def dGam_H_gg(self):
        '''SMEFT Partial Higgs width to gluon-gluon [GeV].''',
        return self['Gam_H_gg_SM']*self['rGam_H_gg']
    
    # H -> gamma gamma
    @internal_parameter(depends_on=('CHW','CHB','CHWB'))
    def dGam_H_aa(self):
        '''SMEFT Partial Higgs width to di-photon [GeV].''',
        return (-1.457e-4*self['CHW'] -4.782e-4*self['CHB'] 
                + 2.64e-4*self['CHWB'])*1e6/self.Lambda**2
    
    @internal_parameter(depends_on=('dGam_H_aa', 'Gam_H_aa_SM'))
    def rGam_H_aa(self):
        '''SMEFT Partial Higgs width to di-photon [GeV].''',
        return self['dGam_H_aa']/ self['Gam_H_aa_SM']
    
    # H -> Z gamma
    @internal_parameter(depends_on=('CHW','CHB','CHWB'))
    def dGam_H_Za(self):
        '''SMEFT Partial Higgs width to Z-photon [GeV].''',
        return (-9.345e-5*self['CHW'] + 9.345e-5*self['CHB'] 
                + 5.885e-5 *self['CHWB'])*1e6/self.Lambda**2
    
    @internal_parameter(depends_on=('dGam_H_Za', 'Gam_H_Za_SM'))
    def rGam_H_Za(self):
        '''SMEFT Partial Higgs width to gluon-gluon[GeV].''',
        return self['dGam_H_Za']/ self['Gam_H_Za_SM']
    
    # LO Higgs decay partial widths to 2 fermions
    
    # H -> b b~
    dGam_H_bb = EFTInternalParameter(
      params = ['CHbox', 'CHD', 'CbH', 'CHl3', 'Cll'],
      linear = [3.108e-4, -7.77e-5, -3.108e-4/ypole[5], -3.108e-4, 2.*7.7e-5],
      doc = '''SMEFT Partial Higgs width to b-bbar [GeV].''',
      lambda_gen=1000.
    )
    
    @internal_parameter(depends_on=('dGam_H_bb', 'Gam_H_bb_SM'))
    def rGam_H_bb(self):
        '''SMEFT Partial Higgs width to gluon-gluon[GeV].''',
        return self['dGam_H_bb']/ self['Gam_H_bb_SM']
        
    # H -> tau+ tau-
    dGam_H_tata = EFTInternalParameter(
      params = ['CHbox', 'CHD', 'CtaH', 'CHl3', 'Cll'],
      linear = [3.14e-5, -7.849e-6, -3.14e-5/ypole[15], -3.14e-5, 2.*7.849e-6],
      doc = '''SMEFT Partial Higgs width to tau+ tau- [GeV].''',
      lambda_gen=1000.
    )
    
    @internal_parameter(depends_on=('dGam_H_tata', 'Gam_H_tata_SM'))
    def rGam_H_tata(self):
        '''SMEFT Partial Higgs width to gluon-gluon[GeV].''',
        return self['dGam_H_tata']/ self['Gam_H_tata_SM']
    
    
    # H -> c c~
    dGam_H_cc = EFTInternalParameter(
      params = ['CHbox', 'CHD', 'CcH', 'CHl3', 'Cll'],
      linear = [2.455e-5, -6.138e-6, -2.455e-5/ypole[4], -2.455e-5, 2.* 6.138e-6],
      doc = '''SMEFT Partial Higgs width to c-cbar [GeV].''',
      lambda_gen=1000.    
    )
    
    @internal_parameter(depends_on=('dGam_H_cc', 'Gam_H_cc_SM'))
    def rGam_H_cc(self):
        '''SMEFT Partial Higgs width to gluon-gluon[GeV].''',
        return self['dGam_H_cc']/ self['Gam_H_cc_SM']

    # H -> mu+ mu-
    dGam_H_mumu = EFTInternalParameter(
      params = ['CHbox', 'CHD', 'CmuH', 'CHl3', 'Cll'],
      linear = [1.111e-7, -2.778e-8, -1.111e-7/ypole[13], -1.111e-7, 2.*2.778e-8],
      doc = '''SMEFT Partial Higgs width to c-cbar [GeV].''',
      lambda_gen=1000.
    )
    
    @internal_parameter(depends_on=('dGam_H_mumu', 'Gam_H_mumu_SM'))
    def rGam_H_mumu(self):
        '''SMEFT Partial Higgs width to gluon-gluon[GeV].''',
        return self['dGam_H_mumu']/ self['Gam_H_mumu_SM']
    
    # LO Higgs decay partial widths to 4 fermions
    
    # H -> 4f

    # SMEFT relative corrections to partial Higgs width to four fermions
    rGam_H_4f = EFTInternalParameter.from_json(json_path+'rGam_H_4f.json')  
    
    @internal_parameter(depends_on=("rGam_H_4f",) )
    def dGam_H_4f(self):
        return self['Gam_H_4f_SM']*self['rGam_H_4f']
        
    # SMEFT relative corrections to partial Higgs width to two different flavor pairs of OSSF leptons.
    rGam_H_lplplrlr = EFTInternalParameter.from_json(json_path+'rGam_H_lplplrlr.json')
    
    # SMEFT relative corrections to partial Higgs width to two different flavor pairs of SF neutrinos.
    rGam_H_vpvpvrvr = EFTInternalParameter.from_json(json_path+'rGam_H_vpvpvrvr.json')
    
    # SMEFT relative corrections to partial Higgs width to different flavor pairs of OSSF lepton and SF neutrino.
    rGam_H_lplpvrvr = EFTInternalParameter.from_json(json_path+'rGam_H_lplpvrvr.json')
    
    # SMEFT relative corrections to partial Higgs width to two different flavor pairs of SF up-type quarks.
    rGam_H_upupurur = EFTInternalParameter.from_json(json_path+'rGam_H_upupurur.json')
    
    # SMEFT relative corrections to partial Higgs width to two different flavor pairs of SF down-type quarks.
    rGam_H_dpdpdrdr = EFTInternalParameter.from_json(json_path+'rGam_H_dpdpdrdr.json')
    
    # SMEFT relative corrections to partial Higgs width to (ss/dd) bb.
    rGam_H_dpdpbb = EFTInternalParameter.from_json(json_path+'rGam_H_dpdpbb.json')
    
    # SMEFT relative corrections to partial Higgs width to different flavor pairs of SF up-type and SF down-type quarks.
    rGam_H_upupdrdr = EFTInternalParameter.from_json(json_path+'rGam_H_upupdrdr.json')
    
    # SMEFT relative corrections to partial Higgs width to (uu/cc) bb.
    rGam_H_upupbb = EFTInternalParameter.from_json(json_path+'rGam_H_upupbb.json')
    
    # SMEFT relative corrections to partial Higgs width to a lepton and an up-type quark pair.
    rGam_H_lluu = EFTInternalParameter.from_json(json_path+'rGam_H_lluu.json')
    
    # SMEFT relative corrections to partial Higgs width to a lepton and an s/d quark pair.
    rGam_H_lldd = EFTInternalParameter.from_json(json_path+'rGam_H_lldd.json')
    
    # SMEFT relative corrections to partial Higgs width to a lepton and b quark pair.
    rGam_H_lplpbb = EFTInternalParameter.from_json(json_path+'rGam_H_lplpbb.json')
    
    # SMEFT relative corrections to partial Higgs width to a neutrino and an up-type quark pair.
    rGam_H_vvuu = EFTInternalParameter.from_json(json_path+'rGam_H_vvuu.json')
    
    # SMEFT relative corrections to partial Higgs width to a neutrino and an s/d quark pair.
    rGam_H_vvdd = EFTInternalParameter.from_json(json_path+'rGam_H_vvdd.json')
    
    # SMEFT relative corrections to partial Higgs width to a neutrino and an b quark pair.
    rGam_H_vpvpbb = EFTInternalParameter.from_json(json_path+'rGam_H_vpvpbb.json')
    
    # SMEFT relative corrections to partial Higgs width to two same flavor pairs of charged leptons.
    rGam_H_lplplplp = EFTInternalParameter.from_json(json_path+'rGam_H_lplplplp.json')
    
    # SMEFT relative corrections to partial Higgs width to two same flavor pairs of neutrinos.
    rGam_H_vpvpvpvp = EFTInternalParameter.from_json(json_path+'rGam_H_vpvpvpvp.json')
    
    # SMEFT relative corrections to partial Higgs width to two same flavor pairs of up-type quarks.
    rGam_H_upupupup = EFTInternalParameter.from_json(json_path+'rGam_H_upupupup.json')
    
    # SMEFT relative corrections to partial Higgs width to ssss/dddd.
    rGam_H_dpdpdpdp = EFTInternalParameter.from_json(json_path+'rGam_H_dpdpdpdp.json')
    
    # SMEFT relative corrections to partial Higgs width to bbbb.
    rGam_H_bbbb = EFTInternalParameter.from_json(json_path+'rGam_H_bbbb.json')
    
    # SMEFT relative corrections to partial Higgs width to two different flavor pairs of SF lepton and neutrino.
    rGam_H_lpvplrvr = EFTInternalParameter.from_json(json_path+'rGam_H_lpvplrvr.json')
    
    # SMEFT relative corrections to partial Higgs width to two different flavor pairs of SF up- and down-type quark.
    rGam_H_updpurdr = EFTInternalParameter.from_json(json_path+'rGam_H_updpurdr.json')
    
    # SMEFT relative corrections to partial Higgs width to a lepton-neutrino and an up-down quark pair.
    rGam_H_lvud = EFTInternalParameter.from_json(json_path+'rGam_H_lvud.json')
    
    # SMEFT relative corrections to partial Higgs width to same flavor pairs of SF up-type and SF down-type quarks.
    rGam_H_upupdpdp = EFTInternalParameter.from_json(json_path+'rGam_H_upupdpdp.json')
    
    # SMEFT relative corrections to partial Higgs width to same flavor pairs of OSSF lepton and SF neutrino.
    rGam_H_lplpvpvp = EFTInternalParameter.from_json(json_path+'rGam_H_lplpvpvp.json')
    
    @internal_parameter(depends_on=('rGam_H_lplplplp','rGam_H_lplplrlr') )
    def dGam_H_4l(self):
        '''SMEFT Partial Higgs width to 4 light leptons (not taus) [GeV].'''
        return (
          2.*self['Gam_H_lplplplp_SM']*self['rGam_H_lplplplp'] +
             self['Gam_H_lplplrlr_SM']*self['rGam_H_lplplrlr'] 
        )
    
    @internal_parameter(depends_on=('dGam_H_4l','Gam_H_4l_SM') )
    def rGam_H_4l(self):
        '''SMEFT Partial Higgs width to 4 light leptons (not taus) divided by SM.'''
        return self['dGam_H_4l']/self['Gam_H_4l_SM']                                
    
    @internal_parameter(depends_on=('rGam_H_lplpvrvr','rGam_H_lpvplrvr','rGam_H_lplpvpvp'))
    def dGam_H_2l2v(self):
        '''SMEFT Partial Higgs width to 2 light leptons (not taus) & 2 neutrinos [GeV].'''
        return ( 4.*self['Gam_H_lplpvrvr_SM']*self['rGam_H_lplpvrvr'] +
                    self['Gam_H_lpvplrvr_SM']*self['rGam_H_lpvplrvr'] +
                 2.*self['Gam_H_lplpvpvp_SM']*self['rGam_H_lplpvpvp'] )
    
    @internal_parameter(depends_on=('rGam_H_lpvplrvr', 'rGam_H_lplpvpvp'))
    def dGam_H_WW_2l2v(self):
        '''
        SMEFT Partial Higgs width via charged or charged/neutral currents to 
        2 light leptons (not taus) & 2 neutrinos [GeV].
        '''
        return (    self['Gam_H_lpvplrvr_SM']*self['rGam_H_lpvplrvr'] +
                 2.*self['Gam_H_lplpvpvp_SM']*self['rGam_H_lplpvpvp'] )
    
    @internal_parameter(depends_on=('dGam_H_WW_2l2v',) )
    def rGam_H_WW_2l2v(self):
        '''SMEFT Partial Higgs width to 2 leptons & 2 neutrinos (not taus) divided by SM.'''
        return self['dGam_H_WW_2l2v']/self['Gam_H_WW_2l2v_SM']
    
    # total
    @internal_parameter(depends_on=(
      'dGam_H_bb','dGam_H_tata','dGam_H_cc','dGam_H_gg',
      'dGam_H_aa','dGam_H_Za','dGam_H_mumu','dGam_H_4f'
    ))
    def dGam_H(self):
        '''SMEFT total Higgs width [GeV].'''
        return (
          self['dGam_H_bb'] + self['dGam_H_tata'] + self['dGam_H_cc']
        + self['dGam_H_gg'] + self['dGam_H_aa'] + self['dGam_H_Za']
        + self['dGam_H_mumu'] + self['dGam_H_4f'] 
    )
    
    @internal_parameter(depends_on=('dGam_H',))
    def rGam_H(self):
        '''SMEFT total Higgs width [GeV].'''
        return self['dGam_H']/self['Gam_H_SM']

    # backwards compatibility 
    # @internal_parameter(depends_on=('dGam_H_4l',) )
    # def dGam_H_Zll(self):
    #     '''For backwards compatibility with old implementation'''
    #     return self['dGam_H_4l']
    #
    # @internal_parameter(depends_on=('rGam_H_4l',) )
    # def rGam_H_Zll(self):
    #     '''For backwards compatibility with old implementation'''
    #     return self['rGam_H_4l']
    #
    # @internal_parameter(depends_on=('dGam_H_WW_2l2v',) )
    # def dGam_H_Wlvl(self):
    #     '''For backwards compatibility with old implementation'''
    #     return self['dGam_H_WW_2l2v']
    #
    # @internal_parameter(depends_on=('rGam_H_WW_2l2v',) )
    # def rGam_H_Wlvl(self):
    #     return self['rGam_H_WW_2l2v']

class SMEFT_HiggsDecay_old(SM_aEW_MZ_GF_5F_HiggsDecay_old, EWPO, aEW_MZ_GF_5F_U2_2_U3_3):
    '''
    SMEFT Warsaw basis for Higgs decay, based on "SMEFT_global_fit_warsaw.nb". 
    '''
    # LO Higgs decay partial widths
    dGam_H_bb = EFTInternalParameter(
      params = ['CHbox', 'CHD', 'CbH', 'CHl3', 'Cll'],
      linear = [3.108e-4, -7.77e-5, -3.108e-4/ypole[5],-3.108e-4,2.*7.7e-5],
      doc = '''SMEFT Partial Higgs width to b-bbar [GeV]. mb=3.237 GeV used to approximate QCD running to mh''',
      lambda_gen=1000.
    )

    dGam_H_tata = EFTInternalParameter(
      params = ['CHbox', 'CHD', 'CtaH', 'CHl3', 'Cll'],
      linear = [3.14e-5, -7.849e-6, -3.14e-5/ypole[15], -3.14e-5, 2.*7.849e-6],
      doc = '''SMEFT Partial Higgs width to tau+ tau- [GeV].''',
      lambda_gen=1000.
    )
    
    dGam_H_cc = EFTInternalParameter(
      params = ['CHbox', 'CHD', 'CcH', 'CHl3', 'Cll'],
      linear = [2.455e-5, -6.138e-6, -2.455e-5/ypole[4], -2.455e-5, 2.* 6.138e-6],
      doc = '''SMEFT Partial Higgs width to c-cbar [GeV].''',
      lambda_gen=1000.    
    )

    dGam_H_mumu = EFTInternalParameter(
      params = ['CHbox', 'CHD', 'CmuH', 'CHl3', 'Cll'],
      linear = [1.111e-7, -2.778e-8, -1.111e-7/ypole[13], -1.111e-7, 2.*2.778e-8],
      doc = '''SMEFT Partial Higgs width to c-cbar [GeV].''',
      lambda_gen=1000.
    )
    
    dGam_H_Zdd = EFTInternalParameter(
      params = [
        'CHbox', 'CHD', 'CHW', 'CHB', 'CHWB', 'CHl3', 'CHq1', 'CHq3',  
        'CHd', 'Cll'
      ],
      # linear = [
      #   1.545e-6, -1.589e-7, 4.365e-7, -1.188e-6, -1.715e-6, -6.681e-15,
      #   -3.728e-6, 1.157e-7, 1.156e-7, -2.245e-8, 2.*9.328e-7
      # ],
      # updated numbers rerunning SMEFTsim.
      linear = [
        1.556e-06, -1.67e-07, 7.88e-07, -1.542e-06, -1.944e-06,
        -3.786e-06, 1.157e-7, 1.156e-7, -2.245e-8, 1.885e-06
      ],
      doc = '''SMEFT Partial Higgs width to Z-(Z*->d-dbar)[GeV].''',
      lambda_gen=1000.
    )
    
    dGam_H_Zbb = EFTInternalParameter(
      params = [
        'CHbox', 'CHD', 'CHW', 'CHB', 'CHWB', 'CHl3', 'CHQ1', 'CHQ3',  
        'CHd', 'Cll'
      ],
      # linear = [
      #   1.545e-6, -1.589e-7, 4.365e-7, -1.188e-6, -1.715e-6, -6.681e-15,
      #   -3.728e-6, 1.157e-7, 1.156e-7, -2.245e-8, 2.*9.328e-7
      # ],
      # updated numbers rerunning SMEFTsim.
      linear = [
        1.556e-06, -1.67e-07, 7.88e-07, -1.542e-06, -1.944e-06,
        -3.786e-06, 1.157e-7, 1.156e-7, -2.245e-8, 1.885e-06
      ],
      doc = '''SMEFT Partial Higgs width to Z-(Z*->b-bbar)[GeV].''',
      lambda_gen=1000.
    )
    
    
    dGam_H_Zuu = EFTInternalParameter(
      params = [
        'CHbox', 'CHD', 'CHW', 'CHB', 'CHWB', 'CHl3', 'CHq1', 'CHq3',  
        'CHu', 'Cll'
      ],
      # linear = [
      #   1.193e-6, -1.632e-7, -6.636e-7, 6.7e-8, -8.891e-7, 7.252e-16, -3.037e-6,
      #   -9.31e-8, 9.322e-8, 4.487e-8, 2.*7.603e-7
      # ],
      # updated numbers rerunning SMEFTsim.
      linear = [
        1.205e-06, -1.82e-07, 1.043e-06, -1.633e-06, -2.024e-06, -3.155e-06, 
        -9.31e-8, 9.322e-8, 4.487e-8, 1.566e-06
      ],
      doc = '''SMEFT Partial Higgs width to Z-(Z*->u-ubar)[GeV].''',
      lambda_gen=1000.
    )
    # @internal_parameter(depends_on = ('CHbox', 'CHD', 'Cll'))
    # def dGam_H_Znunu(self):
    #     '''SMEFT Partial Higgs width to Z-(Z*->nu nu)[GeV].''',
    #     return (2.112e-6*self['CHbox'] + 6.958e-22*self['CHD']
    #              + 2.*1.056e-6 *self['Cll'])*1e6/self.Lambda**2

    dGam_H_Znunu = EFTInternalParameter(
      params = [
        'CHbox','CHD', 'CHW', 'CHB', 'CHWB', 'CHl3', 'Cll'
      ],
      # updated numbers rerunning SMEFTsim. 
      linear = [
        2.107e-06, 0., -7.87e-07, -2.38e-07, -4.42e-07 , -4.091e-06, 2.107e-06,
      ],
      doc = '''SMEFT Partial Higgs width to Z-(Z*->nu nu)[GeV].''',
      lambda_gen=1000.
    )
    
    dGam_H_Zll = EFTInternalParameter(
      params = [
        'CHbox', 'CHD', 'CHW', 'CHB', 'CHWB', 'CHl1', 'CHl3', 'CHe', 'Cll'
      ],
      # linear = [
      # 7.048e-7, 3.268e-7, 2.639e-6, 5.062e-6, 4.714e-6, 7.908e-13, 4.731e-8,
      # 9.43e-7, 4.764e-8, 2.*4.317e-7
      # ],
      # updated numbers rerunning SMEFTsim. CHW precision poor.
      linear = [ 
        7.065e-07, -3.07e-08, -6e-09, -3.41e-07, -4.67e-07 , 4.7e-08,
       -1.502e-06, -4.1e-08, 7.72e-07
      ],
      doc = '''SMEFT Partial Higgs width to Z-(Z*->l+ l-)[GeV].''',
      lambda_gen=1000.
    )
    
    #
    # @internal_parameter(depends_on=('CHG',))
    # def dGam_H_gg(self):
    #     '''SMEFT Partial Higgs width to gluon-gluon[GeV].''',
    #     return 7.692e-3*self['CHG']*1e6/self.Lambda**2
    
    # H -> g g
    # Same coefficients as STXS/mu_ggF0j.json without the constant "1"
    rGam_H_gg = EFTInternalParameter.from_json(
      json_path+'rGam_H_gg.json', 
      linear_only=True, 
      rename=rename
    )
    
    @internal_parameter(depends_on=('rGam_H_gg',))
    def dGam_H_gg(self):
        '''SMEFT Partial Higgs width to gluon-gluon [GeV].''',
        return self['Gam_H_gg_SM']*self['rGam_H_gg']
    
    @internal_parameter(depends_on=('CHW','CHB','CHWB'))
    def dGam_H_aa(self):
        '''SMEFT Partial Higgs width to di-photon [GeV].''',
        return (-1.457e-4*self['CHW'] -4.782e-4*self['CHB'] 
                + 2.64e-4*self['CHWB'])*1e6/self.Lambda**2
        
    @internal_parameter(depends_on=('CHW','CHB','CHWB'))
    def dGam_H_Za(self):
        '''SMEFT Partial Higgs width to Z-photon [GeV].''',
        return (-9.345e-5*self['CHW'] + 9.345e-5*self['CHB'] 
                + 5.885e-5 *self['CHWB'])*1e6/self.Lambda**2

    dGam_H_Wqqp = EFTInternalParameter(
      # params = [
      #   'CHbox', 'CHD', 'CHW', 'CHWB', 'CuH', 'CdH', 'CHl3', 'CHq3', 'Cll'
      # ],
      # linear = [
      #   1.602e-5, -2.965e-5, -1.219e-5, -7.976e-5, -1.326e-14, -6.057e-14,
      #   -7.95e-5, 2.62e-6, 2.*1.205e-5
      # ],
      # updated numbers rerunning SMEFTsim & fitting W mass shift coefficients
      # fits not good for CHl3 & Cll (h > w+/w- j j)
      params = [
        'CHbox', 'CHD', 'CHW', 'CHWB', 'CHl3', 'CHq3', 'Cll'
      ],
      linear = [
        6.758e-05, 9.37e-05, -5.124e-05, 2.48e-04, -4e-06, 1.08e-05 , 9.6e-07
      ],
      doc = '''SMEFT Partial Higgs width to W-(W*->q-qbar)[GeV].''',
      lambda_gen=1000.
    )
    
    dGam_H_Wlvl = EFTInternalParameter(
      # params = ['CHbox', 'CHW', 'CeH', 'CHl3', 'Cll'],
      # linear=[1.127e-5, 5.636e-12, 5.191e-12, 1.09e-12, 2.*8.436e-6],
      # updated numbers rerunning SMEFTsim & fitting W mass shift coefficients
      # fits not good for CHl3 & Cll
      params = ['CHbox', 'CHD', 'CHWB', 'CHW', 'CHl3', 'Cll'],
      linear=[2.244e-5, 3.06e-05, 8.11e-05, -1.71e-05, 3.1e-06, 9.23e-09],
      doc = '''SMEFT Partial Higgs width to W-(W*->l nu)[GeV].''',
      lambda_gen=1000.
    )

    @internal_parameter(depends_on=(
      'dGam_H_bb','dGam_H_tata','dGam_H_cc','dGam_H_gg','dGam_H_Wlvl',
      'dGam_H_Wqqp', 'dGam_H_Zll','dGam_H_Zuu','dGam_H_Zdd','dGam_H_Znunu',
      'dGam_H_aa','dGam_H_Za','dGam_H_mumu'
    ))
    def dGam_H(self):
        '''SMEFT total Higgs width [GeV].'''
        return (
          self['dGam_H_bb'] + self['dGam_H_tata'] + self['dGam_H_cc'] 
        + self['dGam_H_gg'] + 3./2.*self['dGam_H_Wlvl'] + self['dGam_H_Wqqp']
        # + self['dGam_H_gg'] + 3./2.*self['dGam_H_Wlvl'] + 2.*self['dGam_H_Wqqp']
        + 3./2.*self['dGam_H_Zll'] + 2.*self['dGam_H_Zuu'] + 2.*self['dGam_H_Zdd'] 
        + self['dGam_H_Zbb'] + self['dGam_H_Znunu'] 
        + self['dGam_H_aa'] + self['dGam_H_Za'] 
        + self['dGam_H_mumu']
        )
        
    @internal_parameter(depends_on=('dGam_H', 'Gam_H_SM'))
    def rGam_H(self):
        '''SMEFT total Higgs width divided by SM.'''
        return self['dGam_H']/self['Gam_H_SM']

    @internal_parameter(depends_on=(
      'dGam_H_Zll','dGam_Z_ll','dGam_Z','Gam_H_Zll_SM','Gam_Z_ll_SM','Gam_Z_SM'
    ))
    def rGam_H_4l(self):
        '''ratio of SMEFT Partial Higgs width to 4 leptons to SM value.'''
        return (
          self['dGam_H_Zll']/self['Gam_H_Zll_SM']
        + self['dGam_Z_ll']/self['Gam_Z_ll_SM']  
        - self['dGam_Z']/self['Gam_Z_SM']
        )

    @internal_parameter(depends_on=(
      'dGam_H_Wlvl','dGam_W_lv','dGam_W','Gam_H_Wlvl_SM','Gam_W_lv_SM','Gam_W_SM'
    ))
    def rGam_H_WW_2l2v(self):
        '''ratio of SMEFT Partial Higgs width to 2 leptons 2 neutrinos to SM value.'''
        return (
          self['dGam_H_Wlvl']/self['Gam_H_Wlvl_SM'] 
        + self['dGam_W_lv']/self['Gam_W_lv_SM'] 
        - self['dGam_W']/self['Gam_W_SM']
        )

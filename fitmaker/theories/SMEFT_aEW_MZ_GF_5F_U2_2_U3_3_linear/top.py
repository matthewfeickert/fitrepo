import sys
import numpy as np
from numpy import sqrt

from ...fitlib.constants import pi, GeV2pb 
from ...fitlib.theory import internal_parameter, prediction

from ..EFT import EFTBase, EFTPrediction, EFTInternalParameter

from .inputs import aEW_MZ_GF_5F_U2_2_U3_3
from .LEP import SMEFT_Deltas as EWPO, SMEFT_EWPO

from . import data_path

json_path = data_path+'top/'

class Tevatron(aEW_MZ_GF_5F_U2_2_U3_3):
    # Charge asymmetry combination
    mu_AFB_mtt_ttbar_Tevatron_comb = EFTPrediction.from_json(
      json_path+'mu_AFB_mtt_ttbar_Tevatron_comb.json', linear_only=True
    )
    

class Run1(EWPO, aEW_MZ_GF_5F_U2_2_U3_3):
    '''
    SMEFT Warsaw basis for top quark observables, partly based on SMEFiT results. 
    '''
    # ttbar 
    ## W helicity fractions
    w_helicity_F0FLFR = EFTPrediction.from_json(
      json_path+'w_helicity_F0FLFR.json', linear_only=True
    )
    
    ## CMS dilepton analysis
    mu_dsig_dmtt_ptt_ttbar_dilep_8TeV_CMS = EFTPrediction.from_json(
      json_path+'mu_dsig_dmtt_ptt_ttbar_dilep_8TeV_CMS.json', linear_only=True
    )
    
    mu_dsig_dmtt_yt_ttbar_dilep_8TeV_CMS = EFTPrediction.from_json(
      json_path+'mu_dsig_dmtt_yt_ttbar_dilep_8TeV_CMS.json', linear_only=True
    )
    
    mu_dsig_dmtt_ytt_ttbar_dilep_8TeV_CMS = EFTPrediction.from_json(
      json_path+'mu_dsig_dmtt_ytt_ttbar_dilep_8TeV_CMS.json', linear_only=True
    )
    
    mu_dsig_dyt_pt_ttbar_dilep_8TeV_CMS = EFTPrediction.from_json(
      json_path+'mu_dsig_dyt_pt_ttbar_dilep_8TeV_CMS.json', linear_only=True
    )
    
    ## ATLAS dilepton analysis
    mu_dsig_dmtt_ttbar_dilep_8TeV_ATLAS = EFTPrediction.from_json(
      json_path+'mu_dsig_dmtt_ttbar_dilep_8TeV_ATLAS.json', linear_only=True
    )
    
    ## CMS & ATLAS l+jets analyses (common binning)
    mu_dsig_dmtt_ttbar_8TeV = EFTPrediction.from_json(
      json_path+'mu_dsig_dmtt_ttbar_8TeV.json', linear_only=True
    )
    mu_dsig_dpt_ttbar_8TeV = EFTPrediction.from_json(
      json_path+'mu_dsig_dpt_ttbar_8TeV.json', linear_only=True
    )
    mu_dsig_dyt_ttbar_8TeV = EFTPrediction.from_json(
      json_path+'mu_dsig_dyt_ttbar_8TeV.json', linear_only=True
    )
    mu_dsig_dytt_ttbar_8TeV = EFTPrediction.from_json(
      json_path+'mu_dsig_dytt_ttbar_8TeV.json', linear_only=True
    )
    mu_dsig_dabsyt_ttbar_8TeV = EFTPrediction.from_json(
      json_path+'mu_dsig_dabsyt_ttbar_8TeV.json', linear_only=True
    )
    mu_dsig_dabsytt_ttbar_8TeV = EFTPrediction.from_json(
      json_path+'mu_dsig_dabsytt_ttbar_8TeV.json', linear_only=True
    )

    # Charge asymmetry 
    # CMS dilepton diff.
    mu_AC_mtt_ttbar_dilep_8TeV_CMS = EFTPrediction.from_json(
      json_path+'mu_AC_mtt_ttbar_dilep_8TeV_CMS.json', linear_only=True
    )
    # ATLAS dilepton incl.
    mu_AC_mtt_ttbar_dilep_8TeV_ATLAS = EFTPrediction.from_json(
      json_path+'mu_AC_ttbar_dilep_8TeV_ATLAS.json', linear_only=True
    )
    # l+jets combination diff.
    mu_AC_mtt_ttbar_8TeV_comb = EFTPrediction.from_json(
      json_path+'mu_AC_mtt_ttbar_ljets_8TeV_comb.json', linear_only=True
    )
    
    # single-top 
    ## t-channel signal strengths
    ### t
    mu_tchannel_t_8TeV = EFTPrediction.from_json(
      json_path+'mu_tchannel_t_8TeV.json', linear_only=True
    )
    ### t+tbar
    mu_tchannel_t_tbar_8TeV = EFTPrediction.from_json(
      json_path+'mu_tchannel_t_tbar_8TeV.json', linear_only=True
    )
    ### R(t/tbar) 
    mu_R_tchannel_8TeV = EFTPrediction.from_json(
      json_path+'mu_R_tchannel_8TeV.json', linear_only=True
    )
    
    ## CMS t-channel distributions
    ### pT(t+tbar)
    mu_dsig_dptt_tbar_tchannel_8TeV_CMS = EFTPrediction.from_json(
      json_path+'mu_dsig_dptt_tbar_tchannel_8TeV_CMS.json', linear_only=True
    )
    
    ### y(t+tbar)
    mu_dsig_dyt_tbar_tchannel_8TeV_CMS = EFTPrediction.from_json(
      json_path+'mu_dsig_dyt_tbar_tchannel_8TeV_CMS.json', linear_only=True
    )
    
    ## ATLAS t-channel distributions
    ### pT(t)
    mu_dsig_dptt_tchannel_8TeV_ATLAS = EFTPrediction.from_json(
      json_path+'mu_dsig_dptt_tchannel_8TeV_ATLAS.json', linear_only=True
    )
    ### pT(tbar)
    mu_dsig_dpttbar_tchannel_8TeV_ATLAS = EFTPrediction.from_json(
      json_path+'mu_dsig_dpttbar_tchannel_8TeV_ATLAS.json', linear_only=True
    )
    ### y(t)
    mu_dsig_dyt_tchannel_8TeV_ATLAS = EFTPrediction.from_json(
      json_path+'mu_dsig_dyt_tchannel_8TeV_ATLAS.json', linear_only=True
    )
    ### y(tbar)
    mu_dsig_dytbar_tchannel_8TeV_ATLAS = EFTPrediction.from_json(
      json_path+'mu_dsig_dytbar_tchannel_8TeV_ATLAS.json', linear_only=True
    )
    
    ## s-channel signal strength
    mu_schannel_t_tbar_8TeV = EFTPrediction.from_json(
      json_path+'mu_schannel_t_tbar_8TeV.json', linear_only=True
    )
    
    # ttW signal strength
    r_ttW_8TeV = EFTInternalParameter.from_json(
      json_path+'r_ttW_8TeV.json', linear_only=True
    )
    @prediction('mu_ttW_8TeV')
    def mu_ttW_13TeV(self):
        '''ttW production corrected by leptonic W BR'''
        r_ttW = self['r_ttW_8TeV']
        r_pwlv = self['dGam_W_lv']/self['Gam_W_lv_SM']
        r_wwid = self['dGam_W']/self['Gam_W_SM']
        return 1. + r_ttW + r_pwlv - r_wwid
    
    # ttZ signal strength
    r_ttZ_8TeV = EFTInternalParameter.from_json(
      json_path+'r_ttZ_8TeV.json', linear_only=True
    )
    @prediction('mu_ttZ_8TeV')
    def mu_ttZ_13TeV(self):
        '''ttZ production corrected by leptonic Z BR'''
        r_ttZ = self['r_ttZ_8TeV']
        r_pwll = self['dGam_Z_ll']/self['Gam_Z_ll_SM']
        r_zwid = self['dGam_Z']/self['Gam_Z_SM']
        return 1. + r_ttZ + r_pwll - r_zwid

    # ttgamma signal strength
    mu_ttgamma_8TeV = EFTPrediction.from_json(
      json_path+'mu_ttgamma_8TeV.json', linear_only=True
    )

    # tW signal strength
    r_tW_8TeV = EFTInternalParameter.from_json(
      json_path+'r_tW_8TeV.json', linear_only=True
    )
    @prediction('mu_tW_8TeV')
    def mu_tW_8TeV(self):
        '''tW production corrected by leptonic W BR'''
        r_tW = self['r_tW_8TeV']
        r_pwlv = self['dGam_W_lv']/self['Gam_W_lv_SM']
        r_wwid = self['dGam_W']/self['Gam_W_SM']
        return 1. + r_tW + r_pwlv - r_wwid


class Run2(EWPO, aEW_MZ_GF_5F_U2_2_U3_3):
    # ttbar
    ## CMS dilepton mtt
    mu_dsig_dmtt_ttbar_dilep_13TeV_CMS = EFTPrediction.from_json(
      json_path+'mu_dsig_dmtt_ttbar_dilep_13TeV_CMS.json', linear_only=True
    )

    ## CMS l+jets mtt    
    mu_dsig_dmtt_ttbar_ljets_13TeV_CMS = EFTPrediction.from_json(
      json_path+'mu_dsig_dmtt_ttbar_ljets_13TeV_CMS.json', linear_only=True
    )
    
    # Charge asymmetry ATLAS
    mu_AC_mtt_ttbar_13TeV_ATLAS = EFTPrediction.from_json(
      json_path+'mu_AC_mtt_ttbar_ljets_13TeV_ATLAS.json', linear_only=True
    )
    
    # single-top 
    ## t-channel signal strengths (2 independent)
    ### t+tbar
    mu_tchannel_t_tbar_13TeV = EFTPrediction.from_json(
      json_path+'mu_tchannel_t_tbar_13TeV.json', linear_only=True
    )
    ### t
    mu_tchannel_t_13TeV = EFTPrediction.from_json(
      json_path+'mu_tchannel_t_13TeV.json', linear_only=True
    )
    ### tbar
    mu_tchannel_tbar_13TeV = EFTPrediction.from_json(
      json_path+'mu_tchannel_tbar_13TeV.json', linear_only=True
    )
    ### R(t/tbar)
    mu_R_tchannel_13TeV = EFTPrediction.from_json(
      json_path+'mu_R_tchannel_13TeV.json', linear_only=True
    )
    
    ## CMS t-channel distributions
    ### CMS-PAS-TOP-16-004 old 
    #### pT(t+tbar)    
    mu_dsig_dptt_tbar_tchannel_13TeV_CMS = EFTPrediction.from_json(
      json_path+'mu_dsig_dptt_tbar_tchannel_13TeV_CMS.json', linear_only=True
    )
    #### y(t+tbar)
    mu_dsig_dyt_tbar_tchannel_13TeV_CMS = EFTPrediction.from_json(
      json_path+'mu_dsig_dyt_tbar_tchannel_13TeV_CMS.json', linear_only=True
    )
    ### CMS-TOP-17-023 new 
    mu_dsig_dptt_tbar_tchannel_13TeV_CMS_new = EFTPrediction.from_json(
      json_path+'mu_dsig_dptt_tbar_tchannel_13TeV_CMS_new.json', linear_only=True
    )
    #### ratio of t to t+tbar 
    R_dptt_tbar_tchannel_13TeV_CMS = EFTPrediction.from_json(
      json_path+'R_dptt_tbar_tchannel_13TeV_CMS.json', linear_only=True
    )
    
    # ttW signal strength
    r_ttW_13TeV = EFTInternalParameter.from_json(
      json_path+'r_ttW_13TeV.json', linear_only=True
    )
    @prediction('mu_ttW_13TeV')
    def mu_ttW_13TeV(self):
        '''ttW production corrected by leptonic W BR'''
        r_ttW = self['r_ttW_13TeV']
        r_pwlv = self['dGam_W_lv']/self['Gam_W_lv_SM']
        r_wwid = self['dGam_W']/self['Gam_W_SM']
        return 1. + r_ttW + r_pwlv - r_wwid
    
    # ttZ 
    ## signal strength
    r_ttZ_13TeV = EFTInternalParameter.from_json(
      json_path+'r_ttZ_13TeV.json', linear_only=True
    )
    @prediction('mu_ttZ_13TeV')
    def mu_ttZ_13TeV(self):
        '''ttZ production corrected by leptonic Z BR'''
        r_ttZ = self['r_ttZ_13TeV']
        r_pwll = self['dGam_Z_ll']/self['Gam_Z_ll_SM']
        r_zwid = self['dGam_Z']/self['Gam_Z_SM']
        return 1. + r_ttZ + r_pwll - r_zwid
        
    ## CMS pT(Z) with 35.9 fb^-1 (1907.11270)
    r_dsig_dptZ_ttZll_13TeV_CMS = EFTInternalParameter.from_json(
      json_path+'r_dsig_dptZ_ttZll_13TeV_CMS.json', linear_only=True
    )
    @prediction('mu_dsig_dptZ_ttZ_13TeV_CMS')
    def mu_dsig_dptZ_ttZ_13TeV_CMS(self):
        '''
        ttll production (70 < mll < 110 GeV) corrected by total Z width shift 
        assuming NWA (partial width corrections included in generation).
        '''
        r_ttll = self['r_dsig_dptZ_ttZll_13TeV_CMS']
        r_zwid = self['dGam_Z']/self['Gam_Z_SM']
        return 1. + r_ttll - r_zwid
    
    r_dsig_dcosthetastar_ttZll_13TeV_CMS = EFTInternalParameter.from_json(
      json_path+'r_dsig_dcosthetastar_ttZll_13TeV_CMS.json', linear_only=True
    )
    @prediction('mu_dsig_dcosthetastar_ttZ_13TeV_CMS')
    def mu_dsig_dcosthetastar_ttZ_13TeV_CMS(self):
        '''
        ttll production (70 < mll < 110 GeV) corrected by total Z width shift 
        assuming NWA (partial width corrections included in generation).
        '''
        r_ttll = self['r_dsig_dcosthetastar_ttZll_13TeV_CMS']
        r_zwid = self['dGam_Z']/self['Gam_Z_SM']
        return 1. + r_ttll - r_zwid
    
    # ttgamma
    # ATLAS pT(gamma) with 139 fb^-1 (JHEP 09 (2020) 049)
    mu_dsig_dptgamma_ttgamma_13TeV_ATLAS = EFTPrediction.from_json(
      json_path+'mu_dsig_dptgamma_ttgamma_13TeV_ATLAS.json', linear_only=True
    )
    
    # tW signal strength
    # mu_tW_13TeV = EFTPrediction.from_json(
    #   json_path+'mu_tW_13TeV.json', linear_only=True
    # )
    r_tW_13TeV = EFTInternalParameter.from_json(
      json_path+'r_tW_13TeV.json', linear_only=True
    )
    @prediction('mu_tW_13TeV')
    def mu_tW_13TeV(self):
        '''tW production corrected by leptonic W BR'''
        r_tW = self['r_tW_13TeV']
        r_pwlv = self['dGam_W_lv']/self['Gam_W_lv_SM']
        r_wwid = self['dGam_W']/self['Gam_W_SM']
        return 1. + r_tW + r_pwlv - r_wwid
    
    # tZ 
    ## signal strength
    r_tZ_13TeV = EFTInternalParameter.from_json(
      json_path+'r_tZ_13TeV.json', linear_only=True
    )
    @prediction('mu_tZ_13TeV')
    def mu_tZ_13TeV(self):
        '''tZ production corrected by Z->ll branching ratio'''
        r_tZ = self['r_tZ_13TeV']
        r_pwll = self['dGam_Z_ll']/self['Gam_Z_ll_SM']  
        r_zwid = self['dGam_Z']/self['Gam_Z_SM']
        return 1. + r_tZ + r_pwll - r_zwid

class top_LHC_Tevatron(Run1, Run2, Tevatron):
    pass

class SMEFiT(Run1, Run2, Tevatron):
    @internal_parameter()
    def CHWB(self):
        return 0.

    @internal_parameter()
    def CHD(self):
        return 0.

    @internal_parameter()
    def Cll(self):
        return 0.

    @internal_parameter()
    def CHbox(self):
        return 0.

    @internal_parameter()
    def CHG(self):
        return 0.

    @internal_parameter()
    def CHW(self):
        return 0.

    @internal_parameter()
    def CHB(self):
        return 0.

    @internal_parameter()
    def CW(self):
        return 0.

    @internal_parameter()
    def CG(self):
        return 0.

    @internal_parameter()
    def CHl3(self):
        return 0.

    @internal_parameter()
    def CHl1(self):
        return 0.

    @internal_parameter()
    def CHe(self):
        return 0.

    @internal_parameter()
    def CHd(self):
        return 0.

    @internal_parameter()
    def CHq3(self):
        return 0.

    @internal_parameter()
    def CHq1(self):
        return 0.

    @internal_parameter()
    def CHu(self):
        return 0.

    @internal_parameter()
    def CtaH(self):
        return 0.

    @internal_parameter()
    def CbH(self):
        return 0.
    
    # these should be there but can uncomment for testing
    # @internal_parameter()
    # def CHQ3(self):
    #     return 0.
    #
    #
    # @internal_parameter()
    # def CHQ1(self):
    #     return 0.
    #
    #
    # @internal_parameter()
    # def CHt(self):
    #     return 0.
    #
    #
    # @internal_parameter()
    # def CtH(self):
    #     return 0.
    #
    #
    # @internal_parameter()
    # def CtG(self):
    #     return 0.
    #
    #
    # @internal_parameter()
    # def CtW(self):
    #     return 0.
    #
    #
    # @internal_parameter()
    # def CtB(self):
    #     return 0.
    #
    #
    # @internal_parameter()
    # def CQq38(self):
    #     return 0.
    #
    #
    # @internal_parameter()
    # def CQq18(self):
    #     return 0.
    #
    #
    # @internal_parameter()
    # def CQu8(self):
    #     return 0.
    #
    #
    # @internal_parameter()
    # def CQd8(self):
    #     return 0.
    #
    #
    # @internal_parameter()
    # def Ctq8(self):
    #     return 0.
    #
    #
    # @internal_parameter()
    # def Ctu8(self):
    #     return 0.
    #
    #
    # @internal_parameter()
    # def Ctd8(self):
    #     return 0.


    # @internal_parameter()
    # def CQq31(self):
    #     return 0.
    #
    # ttH inclusive SS for SMEFiT xcheck
    mu_ttH_13 = EFTPrediction.from_json(
      json_path+"mu_ttH_13TeV.json", linear_only=True
    )

class SMEFiT_no_ttthres(SMEFiT):
    mu_dsig_dmtt_ttbar_dilep_13TeV_CMS = EFTPrediction.from_json(
      json_path+'mu_dsig_dmtt_ttbar_dilep_13TeV_CMS_no_thres.json', linear_only=True
    )

    ## CMS l+jets mtt    
    mu_dsig_dmtt_ttbar_ljets_13TeV_CMS = EFTPrediction.from_json(
      json_path+'mu_dsig_dmtt_ttbar_ljets_13TeV_CMS_no_thres.json', linear_only=True
    )
        
class SMEFiT_EWPO(SMEFiT, SMEFT_EWPO):
    pass

class SMEFiT_EWPO_no_ttthres(SMEFiT_no_ttthres, SMEFT_EWPO):
    pass 
    
    

"""
``fitlib.observable``
=====================

The observable module defines the base classes that hold the information about 
measurements of observables that go into fitmaker analyses. 

`Uncertainty`
    A container for the multiple possible uncertainties associated to a given 
    measurement of an observable, possibly with asymmetric intervals.
`Obs`
    For single measurements of a particular single- or list-valued onservable.
`ObsGroup`
    A (possibly nested) collection of `Obs` or `ObsGroup` instances, 
    representing a dataset to be used in a fitmaker analyis.

"""
#Name: observable.py
#Description: Observable and Observable Group classes
# 2024/02/05
# TODO:
# Errors when trying to access or remove Obs from ObsGroup
# 24/02/2020
# !!mixed types (list & ndarray) for correlation matrix before/ after adding observables
#IDEA: should we cast everything to np array internally?

from collections import namedtuple
import warnings
import json
import os,re,sys
import numpy as np
# from util import chunks, is_non_string_iterable
from scipy.linalg import block_diag
from collections import OrderedDict
if ((sys.version_info[0] >= 3) and (sys.version_info[1] >= 10)):
    from collections.abc import MutableMapping
else:
    from collections import MutableMapping

from . import formatting
from .util import is_non_string_iterable

#-------------------------------------------------------------------------------
# error struct
class Uncertainty(MutableMapping):
    """
    Dict-like class for handling uncertainty field of observable.
    Behaves like a dict with extra methods.
    Values can be single-valued or list-valued, but lengths != 1 should be
    specified in the constructor. This allows for pair-valued error entries to
    encode asymmetric uncertainties, e.g.

    single-valued: single or [upper, lower]

    and

    list-valued: [s1, s2, ..., sn] or [[u1, l1], [u2, l2], ..., [un, ln]].
    All values should be positive floats.
    The get_error() and get_total() methods have the 'symmetrise' option.
    If True, a single valued number or list is returned, averaging over
    asymmetric errors.
    If False, a pair or list of pairs is returned, doubling non-asymmetric
    entries.

    The value associated to the special key 'tot', will be returned by
    get_total().
    
    Attributes
    ----------
    
    _dict : dict
        Container for different types of uncertainties and their values.
    _len : int, default=1
        Length of observable. 1 for single observable, >1 for list-valued 
        observable.
    _asym : list
        Uncertainty types (keys in `_dict`) that have asymmetric uncertainties.
        
    """

    def __init__(self, arg_dict, length=1):
        """
        Parameters
        ----------
        arg_dict : dict {str:float or list}
            Uncertainty data. Keys are descriptor labels for the type of 
            uncertainty (e.g. 'stat', 'syst' and the special key, 'tot') and 
            values are the uncertainty associated to that key. Allowed types 
            for value are a single float, a list of floats, or a list of 
            2-lists or 2-tuples of floats characterising asymmetric uncertainty 
            intervals.
        length : int, default=1
            Size of the data, 1 for a single float or len(values) for 
            list-valued observables.
        
        Raises
        ------
        UncertaintyDimensionError
            If unexpected shape is detected for uncertainty value. Allowed 
            shapes are: () or (2,) for single-valued observables and 
            (self._len,) or (self._len,2) for list-valued observables. 
            !Currently no support beyond 1D list-valued observables!
        
        """
        self._dict = arg_dict
        self._len = length
        self._asym = []

        # check shapes
        for key, val in self.items():
            v_shape = np.array(val).shape
            if self._len == 1:
                allowed_shapes = ((),(2,))
            else:
                allowed_shapes = ((self._len,),(self._len,2))
            # entry can be a single number or a list of two numbers
            if v_shape not in allowed_shapes:
                msg = (
                  'Wrong shape ({}) in length-{} "{}" uncertainty sub-field, '
                  'should be {} or {}.'
                ).format(v_shape, self._len, key, allowed_shapes[0], allowed_shapes[1])
                raise UncertaintyDimensionError(msg)

            elif v_shape == allowed_shapes[1]:
                self._asym.append(key)

    def __getitem__(self, key):
        """Abstract item getter method for MutableMapping.
        
        Parameters
        ----------
        key : str
            Name of parameter to get value of.
        
        Returns
        -------
        float or list
            Value of the uncertainty associated to `key`.
        
        """
        return self._dict[key]

    def __setitem__(self, key, value):
        """Abstract item setter method for MutableMapping.
        
        Parameters
        ----------
        key : str
            Name of parameter to set value of.
        
        """
        self._dict[key] = value

    def __delitem__(self, key):
        """Abstract item deleter method for MutableMapping.
        
        Parameters
        ----------
        key : str
            Name of parameter to delete.
        
        """
        del self._dict[key]

    def __iter__(self):
        """Abstract Iterator method for MutableMapping.
        
        Returns
        ----------
        Iterator
            Iterator for `self._dict`.
        
        """
        return iter(self._dict)

    def __len__(self):
        """Abstract __len__ method for MutableMapping.
        
        Returns
        ----------
        int
            Length of `self._dict`.
        
        """
        return len(self._dict)

    def __repr__(self):
        """String representation method.
        
        Returns
        -------
        str
            uses repr() method of `self._dict`.
        
        """
        return repr(self._dict)

    def is_asymmetric(self, key):
        """Query if a certain key has an asymmetric uncertainty.
        
        Returns
        -------
        bool
            Whether or not uncertainty type named `key` has an asymmetric 
            uncertainty.
        
        """
        return key in self._asym

    def get_error(self, key, symmetrise=True, symmetrise_method='average'):
        """Return uncertainty component associated to label `key`.
        
        Parameters
        ----------
        key : str
            Label of uncertainty to retrieve the value of.
        symmetrise : bool, default=True
            Whether or not to symmetrise an asymmetric uncertainty.
        symmetrise_method : str, default='average'
            Method to use when symmetrising the uncertainty.
        
        Returns
        -------
        list of floats or list of 2-tuples or 2-lists of floats
            Value of uncertainty associated to label `key`.
            
        """
        error = np.array(self[key])
        if symmetrise:
            if self.is_asymmetric(key):
                up, lo = error.T
                sym_method = getattr(self, symmetrise_method)
                result = sym_method(up, lo)
            else:
                result = error
        else:
            if self.is_asymmetric(key):
                result = error
            else:
                result = np.array([error,error]).T

        return np.abs(result).tolist()

    def get_total(self, symmetrise=True, symmetrise_method='average', include=[], exclude=[]):
        """
        Return total uncertainty, symmetrised or not.
        If 'tot' key exists, returns that value, otherwise adds all values in
        quadrature.
        
        Parameters
        ----------
        symmetrise : bool, default=True
            Whether or not to symmetrise asymmetric uncertainties.
        symmetrise_method : str {'average', 'geometric', 'average', 
        'quadrature', 'maximum'}, default='average'
            Method to use when symmetrising uncertainties. Assumes there is a 
            correspondingly named static method class member implemented.
        include : list, default=[]
            Specify labels to include in the total uncertainty calculation.
        exclude : list, default=[]
            Specify labels to exclude in the total uncertainty calculation.
        
        Returns
        -------
        list of floats or list of 2-tuples or 2-lists of floats
            Total value of uncertainty associated to all labels specified by 
            include, exclude. If neither are specified, all labels will be 
            summed over. If the 'tot' key exists, its value is simply returned, 
            ignoring all others.
        
        """
        if 'tot' in self:
            return self.get_error('tot', symmetrise=symmetrise)
        else:
            if not include: # all keys if unspecified
                include = list(self.keys())
            
            if self._len==1:
                # special treatment for single-valued observables
                totalsq = np.zeros(2,)
            else:
                # list-valued observables
                totalsq = np.zeros((self._len,2))

            for key in [k for k in include if k not in exclude]:
                err = np.array(self.get_error(key, symmetrise=False))
                totalsq += err**2

            total = np.sqrt(totalsq)

            if symmetrise:
                up, lo = total.T
                sym_method = getattr(self, symmetrise_method)
                return sym_method(up, lo).tolist()
            else:
                return total.tolist()

    # symmetrisation methods
    @staticmethod
    def average(upper, lower):
        """Return the arithmetic mean of upper and lower errors.
        
        Parameters
        ----------
        upper : float
            Upper uncertainty interval.
        lower : float
            Lower uncertainty interval.
        
        Returns
        -------
        float
            Arithmetic mean of `upper` and `lower`.
        
        """
        return (upper+lower)/2.

    @staticmethod
    def geometric(upper, lower):
        """Return the geometric mean of upper and lower errors.
        
        Parameters
        ----------
        upper : float
            Upper uncertainty interval.
        lower : float
            Lower uncertainty interval.
        
        Returns
        -------
        float
            Geometric mean of `upper` and `lower`.
        
        """
        return np.sqrt(upper*lower)

    @staticmethod
    def quadrature(upper, lower):
        """Return the normalised sum in quadrature upper and lower errors.
        
        Parameters
        ----------
        upper : float
            Upper uncertainty interval.
        lower : float
            Lower uncertainty interval.
        
        Returns
        -------
        float
            Normalised sum in quadrature of `upper` and `lower`.
        
        """

        return np.sqrt((upper**2 + lower**2)/2.)

    @staticmethod
    def maximum(upper, lower):
        """Return the maximum between the upper and lower errors.
        
        Parameters
        ----------
        upper : float
            Upper uncertainty interval.
        lower : float
            Lower uncertainty interval.
        
        Returns
        -------
        float
            Maximum of `upper` and `lower`.
        
        """
        
        return np.maximum(upper,lower)

class UncertaintyError(Exception):
    """Base class for Uncertainty."""
    pass

class UncertaintyDimensionError(UncertaintyError):
    """Error when shape of uncertainty arguments is bad."""
    pass

#-------------------------------------------------------------------------------

class Obs(object):
    """Dict-like class containing the information for a single observable, 
    which can either be a single measurement or a list of values (e.g. in the 
    case of a histogram distribution) with its correlation matrix. Set of 
    possible (meta)data (keys) are fixed by the contents of __slots__. 
    Metadata fields are optional, while the `value` and `uncertainty` fields 
    are required (although this is not currently enforced in the code it and 
    probably should be).
    
    Metadata Attributes
    -------------------
    observable_name : str
        Theoretical observable identifier connected to predictions in 
        TheoryBase subclasses.
    measurement_name : str
        Unique identifier connected to a specific experimental measurement. For 
        example, two experiments may report measurements of the same physical 
        quantity. In this case, `observable_name` would be the same for both, 
        while `measurement_name` would be different, since it must be a unique 
        identifier. 
    datafile : str or None
        Path to json file on local disk from which the instance was created.
    arxiv : str or None
        arXiv preprint number.
    CDS : str or None
        Link to CDS entry.
    reportnumber : str or None
        Experimental report number.
    DOI : str or None
        DOI of associated publication.
    hepdata : str or None
        Link to HEPData record.
    date : str or None
        Date published.
    experiment : str or None
        Name of experimental collaboration that reported the measurement.
    description : str or None
        Text field containing information about the nature of the measurement,
        and any other detailed deemed relevant for the user.
    superseded_by : str or None, default=None
        measurement_name of another observable that supersedes this measurement.
    supersedes : str, default=None
        measurement_name of another observable that is superseded by this 
        measurement.
    
    Data Attributes
    ---------------
    value : float or list
        Central value of the measurement. Can be a single or multiple values, 
        organised as a list or nested list depending on the type of measurement
        (binned, multi-dimensional,...).
    uncertainty : dict
        Value of uncertainties that will be input as an argument to constructor 
        of Uncertainty instance. e.g. {'stat':[...],'syst':[...],...}
    uncertainty_sigma : int, default=1
        Number of standard deviations to which the uncertainty values/intervals 
        in `uncertainty` correspond.
    th_flat : bool, default=True
        Assume a flat prior for theory uncertainties. Not generally used.
    correlation_matrix : 2D list, default=None
        Correlation matrix in the case of list-valued observable. Assumed to be 
        the identity matrix (uncorrelated) if not specified. 
    
    Internal Attributes
    -------------------
    __slots__ : list
        List of allowed (meta)data attributes.
    _default_vals_dict : dict
        Dictionary specifying default values for attributes in `__slots__`.
    _obs_default_dict : dict
        Dictionary containing default values for attributes in `__slots__`, 
        filled with None for those not specified in `default_vals_dict`.
    
    """
    # pre-allocated instance attributes
    __slots__ = [
        # metadata
        'observable_name', 'measurement_name', 'datafile',
        'arxiv', 'CDS', 'reportnumber', 'DOI', 'hepdata', 'date', 'experiment',
        'description', 'superseded_by', 'supersedes',
        # data
        'value', 'uncertainty', 'uncertainty_sigma', 'th_flat',
        # correlation matrix
        'correlation_matrix'
    ]
    
    # static class attributes: default values for data members of Obs class. 
    # if unspecified, set value to None.
    _default_vals_dict = {'th_flat':True, 'uncertainty_sigma':1}

    _obs_default_dict = {}
    for var in __slots__:
        _obs_default_dict[var] = _default_vals_dict.get(var, None)
    #set data member values upon instantiation
    def __init__(self, arg_dict=_obs_default_dict.copy()):
        """Constructor that initialises values according to those specified in 
        `arg_dict`, deferring to default values in `Obs._obs_default_dict` when 
        unspecified. All (meta)data keys in `arg_dict` are assigned as data 
        members upon initialisation.
        
        Parameters
        ----------
        arg_dict : dict, default=`Obs._obs_default_dict.copy()`
            Values of metadata/data attributes.
        
        Raises
        ------
        ObsShapeError
            If the shape of the `correlation_matrix` attribute is inconsistent 
            with that of the `value` attribute. Shape check for `uncertainty` 
            attribute is done by feeding the len(self) to the `length` keyword 
            of the Uncertainty constructor.
        
        """
        # if data member is not in arg_dict, use default value.
        #(note that if arg_dict contains extra variables not defined in
        # __slots__ then it is simply ignored and not included)
        datamembers_dict = dict([
          (var, arg_dict.get(var, self._obs_default_dict[var])) 
          for var in self.__slots__
        ])
        #set correlation matrix to identity if list of values given without one
        if isinstance(datamembers_dict["value"], list):
            if datamembers_dict["correlation_matrix"] == None:
                datamembers_dict["correlation_matrix"] = np.identity(len(datamembers_dict["value"]))
            else:
                # cast correlation matrix to np.ndarray
                datamembers_dict["correlation_matrix"] = np.array(datamembers_dict["correlation_matrix"])

        for key, val in datamembers_dict.items():
            setattr(self, key, val)
        # Wrap uncertainty in Uncertainty class
        if self.uncertainty is not None:
            self.uncertainty = Uncertainty(self.uncertainty, length=len(self))

        # check correlation matrix shape
        if self._is_list() and self.correlation_matrix.shape != (len(self),len(self)):
            msg = 'Shape mismatch between values ({},) and correlation matrix {}'.format(len(self), self.correlation_matrix.shape)
            raise ObsShapeError(self.observable_name, msg)

    def __len__(self):
        """Length of observable, determined using the length of the `value` 
        attribute.
        
        Returns
        -------
        int
            Length of observable.
        """
        return len(self.value) if self._is_list() else 1

    def _is_list(self):
        """Whether the observable is list-valued determined by whether the 
        `value` attribute is of type list.
        
        Returns
        -------
        bool
            Whether or not the observable is list-valued.
        """
        return isinstance(self.value, list)

    @classmethod
    def init_from_json(cls, arg_json_file, suppress_datafile=False):
        """Alternate constructor to initialise the Obs instance from a json 
        file. json file should match the dict-like structure specifying the 
        (meta)data attributes, as it it passed to the `arg_dict` keyword of 
        `__init__`.
        
        Parameters
        ----------
        arg_json_file : str
            Path to json file containing the information on the observable.
        suppress_datafile : bool, default=False
            Whether to omit the path to the datafile in the metadata.
        
        Returns
        -------
        Obs
            Obs instance containing the information in the json file.
        
        Raises
        ------
        ObsJsonIOError
            If an IOError is raised when trying to open the json file.
        ObsFromJsonWarning
            If `observable_name` field is not specified in the json file.
            
        """
        try:
            with open(arg_json_file, 'r') as jfile:
                json_fields_dict = json.load(jfile)
        except IOError as e:
            msg = 'json file {} not found.'.format(arg_json_file)
            raise ObsJsonIOError(msg)

        #check this is a (single) obs json file
        json_obs_name = json_fields_dict.get("observable_name", None)
        if json_obs_name == None:
            warnings.warn(
              "Could not find observable in " + str(arg_json_file) + ", setting values to default.",
              ObsFromJsonWarning
            )

            return cls()

        if not suppress_datafile:
            # set path field to json datafile path
            json_fields_dict["datafile"] = arg_json_file

        return cls(json_fields_dict)

    def get_total_uncertainty(
      self, 
      symmetrise=True, 
      symmetrise_method='average', 
      include=[], 
      exclude=[]
    ):
        """Returns total uncertainty as obtained from get_total() method of
        Uncertainty class.

        Parameters
        ----------
        symmetrise : bool, default=True
            Whether or not to symmetrise asymmetric uncertainties.
        symmetrise_method : str {'average', 'geometric', 'average', 
        'quadrature', 'maximum'}, default='average'
            Method to use when symmetrising uncertainties. Assumes there is a 
            correspondingly named static method class member implemented.
        include : list, default=[]
            Specify labels to include in the total uncertainty calculation.
        exclude : list, default=[]
            Specify labels to exclude in the total uncertainty calculation.
        
        Returns
        -------
        list of floats or list of 2-tuples or 2-lists of floats
            Total value of uncertainty associated to all labels specified by 
            include, exclude. If neither are specified, all labels will be 
            summed over. If the 'tot' key exists, its value is simply returned, 
            ignoring all others.
        
        """
        return self.uncertainty.get_total(
          symmetrise=symmetrise,
          symmetrise_method=symmetrise_method,
          include=include,
          exclude=exclude
        )

    def __str__(self):
        """Return string representation of Obs instance using `Obs.to_json()`."""
        return self.to_json(indent=2)

    def to_dict(self):
        """Convert instance data to OrderedDict. Ensures ordering in python 2.
        
        Returns
        -------
        dict
            Dictionary containing (meta)data attributes.
        
        """
        # Use OrderedDict to preserve order, automatic in py3
        result = OrderedDict()
        for key in self.__slots__:
            value = getattr(self,key)
            if value is not None:
                # convert numpy arrays back to lists
                if isinstance(value, np.ndarray):
                    value = value.tolist()
                elif isinstance(value, Uncertainty):
                    value = value._dict
                result[key] = value
        return result

    def to_json(self, indent=2):
        """Convert instance to json string, convert inner arrays to one line.

        Parameters
        ----------
        indent : int
            Number of spaces to indent with in json string representation.
        
        Returns
        -------
        str
            json string representation of Obs instance.
        
        """
        jstr = json.dumps(self.to_dict(), indent=indent)
        pretty_jstr = re.sub(
          r'\s*\[[-\d.,\s]*?\],{0,1}', 
          formatting.reformat_numerical_array, 
          jstr, 
          flags=re.DOTALL
        )
        return re.sub(r'\[\s*?\[',r'[\n  [', pretty_jstr)

    def write_json_file(self, path, indent=2):
        """Write Obs instance data to json file at location specified by `path`.
        
        Parameters
        ----------
        path : str
            Target path to save json file.
        indent : int, default=2
            Number of spaces to indent with in json string representation.
        
        """
        with open(path, 'w') as outfile:
            outfile.write( self.to_json(indent=indent) )

#-------------------------------------------------------------------------------
# Exceptions
class ObsError(Exception):
    """Base exception class for Obs"""
    def __init__(self, obs, message):
        """
        Parameters
        ----------
        obs : str
            Name of observable to display in error message.
        message : str
            Basic error message to which `obs` name is appended.
        
        """
        message ='in observable {}: {}'.format(obs, message)
        super(ObsError, self).__init__(message)

class ObsShapeError(ObsError):
    """Exception to raise when inconsistent shapes are detected when 
    initialising Obs."""
    pass


class ObsJsonIOError(Exception):
    """Exception to raise when handling an IOError during the reading of a 
    json file in `Obs.init_from_json()`."""
    pass

class ObsWarning(Warning):
    """Base warning class for obs. Never raised."""
    pass

class ObsFromJsonWarning(ObsWarning):
    """Warning to raise when inconsistencies are detected in the Obs information
    read from a json file in `Obs.init_from_json()`."""
    pass

#-------------------------------------------------------------------------------
#Contains a group 
class ObsGroup(object):
    """Class containing of observables stored as a list of `Obs` and `ObsGroup` 
    objects together with their correlation matrix. Set of  possible attributes 
    are fixed by the contents of __slots__. 
    
    Attributes
    ----------
    observable_group_name : str
        Name of observable group
    datafile : str or None
        Path to json file on local disk from which the instance was created.
    description : str or None
        Text field containing information deemed relevant for the user.
    observables : list
        List of `Obs` or `ObsGroup` instances contained in the `ObsGroup`
    correlation_matrix : 2D list 
        Matrix capturing correlation information between the elements of 
        `observables`. Expected shape is 
        (len(`observables`), len(`observables`)).
    
    """
    #pre-allocated instance attributes
    __slots__ = [
        #metadata
        'observable_group_name', 'datafile', 'description',
        #data
        'observables', 'correlation_matrix'
    ]


    def __init__(self, arg_dict=None):
        """Set values for attributes in `ObsGroup.__slots__`, taking default 
        values from `default_dict`. `observables` field should be a list of dict 
        representations of Obs or ObsGroup instances.
        
        Parameters
        ----------
        arg_dict : dict, optional
            Values for attributes in `ObsGroup.__slots__`.
        
        Raises
        ------
        ObsGroupShapeError
            If unexpected shape of `correlation_matrix` is found.
        
        """
        
        default_dict = {
          "observable_group_name":'',
          "datafile":'',
          "description":'',
          "observables":[],
          "correlation_matrix":[]
        }

        # if arg_dict does not contain var, use default value in default_dict
        if arg_dict == None: arg_dict = default_dict
        obsgroupmembers_dict = dict([ 
            (var, arg_dict.get( var, default_dict[var] ))
            for var in self.__slots__ 
        ])

        # convert observables list of obs/obsgroup dicts to list of
        # Obs()/ObsGroup() objects
        if obsgroupmembers_dict['observables'] != []:

            converted_obslist = []
            for io, obsdict in enumerate(obsgroupmembers_dict['observables']):
                try:
                    converted_obslist.append(self.convert_dict_to_obs(obsdict))
                except ObsGroupConvertDictError as e:
                    msg = "Expected Obs or ObsGroup in entry {} of {}, none found.".format
                    warnings.warn(
                      msg(io, obsgroupmembers_dict['observable_group_name']),
                      ObsGroupConvertDictWarning
                    )
                    converted_obslist.append(ObsGroup())

            obsgroupmembers_dict['observables'] = converted_obslist

        # set correlation matrix to identity if given without one
        if not obsgroupmembers_dict["correlation_matrix"]: # not None == True
            obsgroupmembers_dict["correlation_matrix"] = np.identity(
              len(obsgroupmembers_dict["observables"])
            )
        else:
            # cast to np.ndarray
            obsgroupmembers_dict["correlation_matrix"] = np.array(
              obsgroupmembers_dict["correlation_matrix"]
            )

        # set instance attributes
        for key, val in obsgroupmembers_dict.items():
            setattr(self, key, val)
        
        # check shape of correlation matrix
        if self.correlation_matrix.shape != (len(self),len(self)):
            msg = 'Shape mismatch between values ({},) and correlation matrix {}'.format
            raise ObsGroupShapeError(
              self.observable_group_name,
              msg(len(self), self.correlation_matrix.shape)
            )

    @classmethod
    def init_from_json(cls, arg_json_file):
        """Alternate constructor to initialise the ObsGroup instance from a json 
        file. json file should match the dict-like structure specifying the 
        attributes, as it it passed to the `arg_dict` keyword of 
        `ObsGroup.__init__()`. `observables` field should be a list of dict 
        representations of individual Obs or ObsGroups.
        
        Parameters
        ----------
        arg_json_file : str
            Path to json file containing the information on the observable.
        
        Returns
        -------
        ObsGroup
            ObsGroup instance containing the information in the json file.
        
        Raises
        ------
        ObsGroupJsonIOError
            If an IOError is raised when trying to open the json file.
        ObsGroupFromJsonWarning
            If `observable_name` field is not specified in the json file.
            
        """
        try:
            with open(arg_json_file, 'r') as jfile:
                json_fields_dict = json.load(jfile)
        except IOError as e:
            msg = 'json file {} not found.'.format(arg_json_file)
            raise ObsGroupJsonIOError(msg)

        #check obs list is not empty
        if json_fields_dict.get("observables", None) is None:
            msg = 'Could not find "observables" list in {}, use default values.'.format
            warnings.warn(
              msg(str(arg_json_file)),
              ObsGroupFromJsonWarning
            )

        #set path field to json datafile path
        json_fields_dict["datafile"] = arg_json_file

        return cls(json_fields_dict)

    @staticmethod
    def convert_dict_to_obs(arg_dict):
        """Convert dict representation to Obs or Obsgroup instance.
        
        Parameters
        ----------
        arg_dict : dict
            Dictionary representation of Obs or ObsGroup instance.
        
        Returns
        -------
        Obs or Obsgroup
            According to the presence of the `observable_name` of 
            `observable_group_name` key, respectively.
        
        Raises
        ------
        ObsGroupConvertDictError
            If 'observable_name' not in arg_dict or 'observable_group_name' not 
            in arg_dict.
        
        """
        if arg_dict.get('observable_name', None) is not None:
            return Obs(arg_dict)
        elif arg_dict.get('observable_group_name', None) is not None:
            return ObsGroup(arg_dict)
        else:
            msg = ("Neither 'observable_name' or 'observable_group_name' "
            "keys were found in dict keys: {}".format(list(arg_dict.keys()))
            )
            raise ObsGroupConvertDictError(msg)


    def _update_correlation_matrix(self, arg_len):
        """
        Internal method to update correlation matrix when adding a new Obs or 
        ObsGroup to current ObsGroup instance.
        
        Parameters
        ----------
        arg_len : int
            Length of the Obs or ObsGroup instance being added to the current 
            ObsGroup instance.
        
        """
        # self.correlation will be set to [] by default if unspecified when 
        # initialising. Otherwise, it will be a 2D numpy.array. 
        # Maybe leaving it as None when unspecified would be safer?
        if not isinstance(self.correlation_matrix, np.ndarray):
            self.correlation_matrix = np.identity(arg_len)
        else:
            self.correlation_matrix = block_diag(
              self.correlation_matrix,
              np.identity(arg_len)
            )

    def add_obs(self, *arg_obss):
        """
        Method to add new `Obs` or `ObsGroup` instances to current `ObsGroup` 
        instance.
        
        Parameters
        ----------
        arg_obss : tuple or list
            Set of `Obs`/`ObsGroup` instances to be added.
        
        """
        self.observables += arg_obss
        self._update_correlation_matrix(len(arg_obss))

    def add_obs_from_json(self, *arg_json_files):
        """
        Method to add new `Obs` instances to current `ObsGroup` 
        instance using the `init_from_json()` constructor.
        
        Parameters
        ----------
        arg_json_files: tuple or list
            Set of paths to json files containing the information for the `Obs` 
            instances to be added.
        
        """
        for arg_json_file in arg_json_files:
            self.observables.append(Obs.init_from_json(arg_json_file))
        self._update_correlation_matrix(len(arg_json_files))

    def add_obsgroup_from_json(self, *arg_json_files):
        """
        Method to add new `ObsGroup` instances to current `ObsGroup` 
        instance using the `init_from_json()` constructor.
        
        Parameters
        ----------
        arg_json_files: tuple or list
            Set of paths to json files containing the information for the 
            `ObsGroup` instances to be added.
        
        """
        for arg_json_file in arg_json_files:
            self.observables.append(ObsGroup.init_from_json(arg_json_file))
        self._update_correlation_matrix(len(arg_json_files))

    def get_obs(self, obsname):
        """Get a particular `Obs` instance contained in the current `ObsGroup` 
        instance by its unique `measurement_name` attribute.
        
        Parameters
        ----------
        obsname: str
            `measurement_name` attribute of Obs instance to retrieve.
        
        Returns
        -------
        Obs
            Instance with matching `measurement_name` attribute.
        
        """
        flatself = self.flatten()
        iobs = [o.measurement_name for o in flatself.observables].index(obsname)
        return flatself.observables[iobs]

    def remove_obs(self, *arg_obs):
        """Remove a number of `Obs` instances contained in the current `ObsGroup` 
        instance by their unique `measurement_name` attributes. As a by product, 
        applies `self.flatten()`, returning a flatten instance, see 
        `ObsGroup.flatten()` documentation.
        
        Parameters
        ----------
        arg_obs: tuple or list of str
            `measurement_name` attribute(s) of Obs instance(s) to remove.
        
        Returns
        -------
        ObsGroup
            Flattened ObsGroup instance with the corresponding observable(s) 
            removed.
        
        """
        flatself = self.flatten()

        for obs in arg_obs:
            # get obs index
            iobs = [o.measurement_name for o in flatself.observables].index(obs)
            # remove obs
            flatself.observables.pop(iobs)
            # delete row & column of correlation matrix
            newcorr = np.delete(flatself.correlation_matrix, iobs, axis=0)
            newcorr = np.delete(newcorr, iobs, axis=1)
            flatself.correlation_matrix = newcorr

        return flatself

    def remove_bin(self, obsname, binnumber):
        """Remove a bin of a list-valued `Obs` instance contained in the 
        current `ObsGroup` instance by its unique `measurement_name` attribute 
        and a specific bin number. As a by product, applies `self.flatten()`, 
        returning a flatten instance, see `ObsGroup.flatten()` documentation.
        
        Parameters
        ----------
        obsname: str
            `measurement_name` attribute of Obs instance to retrieve.
        binnumber: int
            Element (bin) of the `Obs` instances data to delete.
        
        Returns
        -------
        ObsGroup
            Flattened ObsGroup instance with the corresponding bins of the 
            observable removed.
        
        """
        
        flatself = self.flatten()
        # Retrieve observable
        iobs = [o.measurement_name for o in flatself.observables].index(obsname)
        obs = flatself.observables[iobs]
        # Delte bin in value
        obs.value = np.delete(obs.value, binnumber, axis=0).tolist()
        # Delete bin in  uncertainties
        new_uncert={}
        for ename, err in obs.uncertainty.items():
            new_uncert[ename] = np.delete(err, binnumber, axis=0).tolist()
        obs.uncertainty = Uncertainty(new_uncert, length=len(obs.value))
        # Delete bin in covariance
        try:
            newcorr = np.delete(obs.correlation_matrix, binnumber, axis=0)
            newcorr = np.delete(newcorr, binnumber, axis=1)
            obs.correlation_matrix=newcorr.tolist()
        except AttributeError:
            # Ignore if no covariance matrix assigned.
            pass
        # replace in ObsGroup
        flatself.observables[iobs] = obs

        return flatself

    def flatten(self, expand_lists=False):
        """Returns an ObsGroup with a flattened structure, un-nesting inner 
        ObsGroups.
        
        Parameters
        ----------
        expand_lists: bool, default=False
            Expand list-valued observables into individual single-valued 
            observables.
        
        Returns
        -------
        ObsGroup
            Flattened ObsGroup instance.
        
        """
        # initialise new ObsGroup data copied from current one
        copydict = dict([ (key, getattr(self,key)) for key in self.__slots__ ])

        # flatten obs list
        obslist = self._flatten_observables_list(expand_lists=expand_lists)
        # flatten correlation matrix
        cmat = list(self._flatten_correlation_matrix(expand_lists=expand_lists))
        
        # replace input observables and correlation matrix
        copydict['observables'] = [ o.to_dict() for o in obslist ]
        copydict['correlation_matrix'] = cmat

        return ObsGroup(copydict)

    @staticmethod
    def _list_Obs_to_ObsGroup(list_obs):
        """Convert a list-valued observable into an ObsGroup of single-valued 
        observables. The `measurement_name` and `observable_name` attributes 
        of the i-th bin are suffixed with '[i]'. `description` attributes are 
        appended with the fact that it was been converted.
        
        Parameters
        ----------
        list_obs: Obs
            List-valued Obs instance.
        
        Returns
        -------
        ObsGroup
            ObsGroup instance containing each bin of the input Obs instance as 
            a list of single-valued Obs instances.
        
        """
        # dict representation
        ldict = list_obs.to_dict()
        olist = []
        # loop over bins
        for i, val in enumerate(list_obs.value):
            # index string to suffix names with
            tag = '['+str(i)+']' 
            # copy metadata
            odict = dict(**ldict)
            # Remove correlation matrix info here, will be added to ObsGroup
            try:
                del odict['correlation_matrix']
            except KeyError:
                pass
            # add suffix & extra description
            odict['observable_name']+=tag
            odict['measurement_name']+=tag
            odict['description']+= ' [converted to ObsGroup, bin {}]'.format(i)
            # single bin value & error
            odict['value'] = ldict['value'][i]
            odict['uncertainty'] = {
              k:v[i] for k,v in ldict['uncertainty'].items()
            }
            # add to ObsGroup input
            olist.append(odict)

        # new ObsGroup
        obsgroup = ObsGroup({
          'observable_group_name':list_obs.observable_name,
          'observables':olist,
          'correlation_matrix':list(list_obs.correlation_matrix),
          'datafile':list_obs.datafile,
          'description':list_obs.description + ' [converted to ObsGroup]'
        })

        return obsgroup

    def _flatten_observables_list(self, expand_lists=False):
        """Return single list of Obs instances contained in ObsGroup.
        Optionally expand list-valued Obs instances.
        Recursevly iterates over nested ObsGroup structures.
        Length will match dimensions of _flatten_correlation_matrix() called
        with same expand_lists option.
        
        Parameters
        ----------
        expand_lists: bool, default=False
            Expand list-valued observables into individual single-valued 
            observables.
        
        Returns
        -------
        list of Obs
        
        """
        obsList = []
        for obs in self.observables:
            # if obs is an ObsGroup then iterate
            if self._is_ObsGroup(obs):
                obsList += obs._flatten_observables_list(expand_lists = expand_lists)
            # not an ObsGroup: add Obs to list, optionally expanding lists
            elif self._is_single_valued(obs) or not expand_lists:
                obsList.append(obs)
            # expand list by converting to ObsGroup and iterating
            else:
                OG = self._list_Obs_to_ObsGroup(obs)
                obsList += OG._flatten_observables_list()

        return obsList

    def _flatten_correlation_matrix(self, expand_lists=False):
        """Return full correlation matrix of an ObsGroup instance.
        Handles general structure of single-valued and list-valued Obs and
        nested ObsGroups.
        Expansion of list-values observables is optional (expand_lists).
        
        Parameters
        ----------
        expand_lists: bool, default=False
            Expand list-valued observables into individual single-valued 
            observables.
        
        Returns
        -------
        square 2D numpy.array
            Expanded correlation matrix.
        
        """
        # 2024/02/05: There must be a better way to do this...
        correlation_matrix_list = []
        
        # controlled iteration needed
        iterobs = enumerate(iter(self.observables))
        do_next, stopiter = True, False

        while True:
            try:
                if do_next: # normal iteration
                    iobs, obs = next(iterobs)
                else: # special: just iterated over single-valued Obs
                    do_next=True
            except StopIteration:
                break

            # recursion on ObsGroup
            if self._is_ObsGroup(obs):
                submatrix = obs._flatten_correlation_matrix(
                  expand_lists=expand_lists
                )

            # optionally expand list-valued Obs
            elif obs._is_list() and expand_lists: # expand list-valued obs
                submatrix = obs.correlation_matrix

            else:
                # keep existing correlation matrix entries
                istart = iobs
                stopcondition = False
                while not stopcondition:
                    # iterate over successive single-valued Obs to get
                    # correlation submatrix indices
                    try: # read next obs
                        iobs, obs = next(iterobs)
                    except StopIteration: # catch end of iterable
                        stopiter = True
                        iobs += 1 # one for luck
                        break

                    # include list-valued Obs depending on expand_lists
                    if expand_lists:
                        stopcondition = not self._is_single_valued(obs)
                    else:
                        stopcondition = self._is_ObsGroup(obs)

                # end while not stopcondition

                if istart==iobs: # single Obs sequence special case
                    submatrix = 1.
                else: # get submatrix from ObsGroup.correlation_matrix
                    submatrix = self.correlation_matrix[istart:iobs,istart:iobs]

                # current value of obs is an unprocessed ObsGroup or list Obs
                # tell next iteration not to call next()
                do_next= False


            correlation_matrix_list.append(submatrix)
            
            # iterobs got fully consumed in inner while loop
            if stopiter: break 

        # end while True

        return block_diag(*correlation_matrix_list)

    @staticmethod
    def _is_ObsGroup(obs):
        """Helper function to determine if observable object is an `Obs` or 
        `ObsGroup` instance. Practically, it checks if it has an 
        `observable_group_name` attribute.
        
        Parameters
        ----------
        obs 
            Intended to be `Obs` or `ObsGroup` instance.
        
        Returns
        -------
        bool
        
        """
        return hasattr(obs, 'observable_group_name')

    @classmethod
    def _is_single_valued(cls, obs):
        """Helper function to determine if `Obs` instance is single-valued (not 
        list-valued). Also returns False if `obs` is an `ObsGroup` instance.
        
        Parameters
        ----------
        obs 
            Intended to be Obs or ObsGroup instance.
        
        Returns
        -------
        bool

        """
        if cls._is_ObsGroup(obs):
            return False
        else:
            return not obs._is_list()

    def _contains_nested_ObsGroup(self):
        """Helper function to determine if this `ObsGroup` is a nested 
        `ObsGroup`, i.e., contains other `ObsGroup` instances.
        
        Returns
        -------
        bool
        
        """
        for obs in self.observables:
            if self._is_ObsGroup(obs): return True
        return False

    def get_obs_vectors(
      self, 
      expand_observable_names=False, 
      symmetrise_errors=True, 
      symmetrise_method='average'
    ):
        """Return vectorised list of observable names, values, uncertainties, and
        combined block diagonal correlation matrix of flattened data in ObsGroup 
        instance.
    
        Parameters
        ----------        
        expand_observable_names : bool, default=False
            Expand the `measurement_name` of each list-valued observables into 
            individual names for each bin, appending the i-th bin with the 
            '[i]' suffix.
        symmetrise_errors : bool, default=True
            Symmetrise asymmetric errors. Option passed to 
            `Obs.get_total_uncertainty()`, see its documentation.
        symmetrise_method : str {'average', 'geometric', 'average', 
            'quadrature', 'maximum'}, default='average'
            Method to use when symmetrising uncertainties. Option passed to 
            `Obs.get_total_uncertainty()`, see its documentation.
        
        Returns
        -------
        `nameVec` : list of str
            `measurement_name` attributes of `Obs` instances in the `ObsGroup`. 
            Generally will not match shape of other return values when 
            list-valued obs are present, unless `expand_observable_names`=True.
        `valVec` : list of float
            Flattened list of `value` attributes of `Obs` instances in the 
            `ObsGroup`. 
        `errorVec` : list of float 
            Flattened list of total uncertainties of `Obs` instances in the 
            `ObsGroup`.
        `correlation_matrix`: 2D array
            Correlation matrix for flattened data.
        
        """
        # if not a flattened obsgroup then flatten to a list of Obs instances
        if self._contains_nested_ObsGroup():
            flattenedSelf = self.flatten(expand_lists=False)
        else:
            flattenedSelf = self

        nameVec, valVec, errorVec = [], [], []
        for obs in flattenedSelf.observables:
            if obs._is_list() and expand_observable_names:
                OG = self._list_Obs_to_ObsGroup(obs)
                nameVec += [o.observable_name for o in OG.observables]
            else:
                nameVec.append(obs.observable_name)

            try: # add list of values
                valVec += obs.value
                errorVec += list(obs.get_total_uncertainty(symmetrise=symmetrise_errors, symmetrise_method=symmetrise_method))

            except: # add single value

                valVec.append(obs.value)
                errorVec.append(obs.get_total_uncertainty(symmetrise=symmetrise_errors, symmetrise_method=symmetrise_method))

        correlation_matrix = self._flatten_correlation_matrix(expand_lists=True)

        return nameVec, valVec, errorVec, correlation_matrix

    def __str__(self, arg_tabstr=''):
        """String representation, using `ObsGroup.to_json()` method.
        
        Returns
        -------
        str
            json representation of `ObsGroup` data.
        
        """
        return self.to_json(indent=2)

    def __len__(self):
        """Length using length of `self.observables`.
        
        Returns
        -------
        int
            Length of `observables` attribute.
        
        """
        return len(self.observables)

    def to_dict(self):
        """Convert instance data to regular python dicts. Elements of 
        `observables` attribute are themselves converted to dict.
        
        Returns
        -------
        dict
            Dictionary representation of ObsGroup Data.
            
        """
        # Use OrderedDict to preserve order, automatic in py3
        result = OrderedDict()
        for key in self.__slots__:
            value = getattr(self, key)

            if key == 'observables':
                # Convert Obs instances
                value = [x.to_dict() for x in value]

            if value is not None:
                # convert numpy arrays back to lists
                if isinstance(value, np.ndarray):
                    value = value.tolist()

                result[key] = value

        return result

    def to_json(self, indent=2):
        """Convert instance to json string, convert inner arrays to one line.
        
        Parameters
        ----------
        indent : int
            Number of spaces to indent with in json string representation.
        
        Returns
        -------
        str
            json string representation of Obs instance.
        
        """
        
        jstr = json.dumps(self.to_dict(), indent=indent)
        
        return formatting.pretty_json_string(jstr)

    def write_json_file(self, path, indent=2):
        """Convert write data to json file specified by path
        
        Parameters
        ----------
        path : str
            Target path to save json file.
        indent : int, default=2
            Number of spaces to indent with in json string representation.
        
        """
        with open(path, 'w') as outfile:
            outfile.write( self.to_json(indent=indent) )

#-------------------------------------------------------------------------------
class ObsGroupError(Exception):
    """Base exception class for Obs."""
    def __init__(self, obs, message):
        """
        Parameters
        ----------
        obs : str
            Name of observable to display in error message.
        message : str
            Basic error message to which `obs` name is appended.
        
        """
        message ='in observable {}: {}'.format(obs, message)
        super(ObsGroupError, self).__init__(message)

class ObsGroupShapeError(ObsGroupError):
    pass

class ObsGroupJsonIOError(Exception):
    pass

class ObsGroupConvertDictError(Exception):
    """Exception class for static method convert_dict_to_obs."""
    pass

class ObsGroupWarning(Warning):
    """Base warning class for ObsGroup. Never raised."""
    pass

class ObsGroupFromJsonWarning(ObsGroupWarning):
    """Warning for init_from_json method."""
    pass

class ObsGroupConvertDictWarning(ObsGroupWarning):
    """Warning for init_from_json method."""
    pass


"""
``fitlib.fitter``
=====================

Brings together observables, theory, and likelihood to scan, minimise, 
marginalise etc.

Fitters
-------
FitterBase
    Base fitter class. Not intended to be instantiated, only subclassed.
FitterChiSquare
    Fitter based on chi^2 likelihood, 
    `fitmaker.fitlilb.likelihood.ChiSquareLikelihood`.
FitterAnalyticalChiSquare
    Fitter based on chi^2 likelihood, allowing the implementation of analytical 
    solutions to the best fit points and confidence intervals.
FitterMetropolisHastings
    Fitter for use with pymultinest.

"""
import os
import json
import warnings
warnings.filterwarnings('ignore')
import numpy as np
from scipy.optimize import minimize
from numpy.linalg import LinAlgError

# for multinest:
try:
    import pymultinest
    from ctypes.util import find_library
    if find_library('libmultinest') is None:
        raise ImportError('Could not find c library "libmultinest.so/libmultinest.dylib"')
    _has_pymultinest = True
    _no_pymultinest_error = None
except ImportError as e:
    _has_pymultinest = False
    _no_pymultinest_error = e

from .likelihood import ChiSquareLikelihood
from .observable import ObsGroup, Obs
from .theory import BoundPrediction
from .core import FMCallable

from ..theories.EFT import EFTBase

#-------------------------------------------------------------------------------

class FitterBase(object):
    """
    Fitter base class.
    
    Attributes
    ----------
    obsgroup : fitmaker.fitlib.observable.ObsGroup
        Set of measured observables to include in the fit.
    theory : fitmaker.fitlib.theory.TheoryBase subclass
        Theory class to get predictions from.
    LH : FMCallable
        Likelihood to feed to the fitter.
        
    """
    def __init__(self, arg_obsgroup, arg_theory, arg_LH=None):
        self.obsgroup = arg_obsgroup
        self.theory = arg_theory
        self.LH = arg_LH


class FitterError(Exception):
    """Base exception class for Fitter."""
    pass

class FitterAnalyticalChiSquareError(FitterError):
    """Base exception class for FitterAnalyticalChiSquare."""
    pass

class SingularFisherInformationError(FitterAnalyticalChiSquareError):
    """Exception for non-invertible Fisher Information matrix."""

class FitterWarning(Warning):
    """Base warning class for Fitter."""
    pass

class FitterAnalyticalChiSquareWarning(FitterWarning):
    """Base warning class for FitterAnalyticalChisquare."""
    pass

class SingularFisherInformationWarning(FitterAnalyticalChiSquareWarning):
    """Warning for non-invertible Fisher Information matrix."""
    pass

#-----------------------------------------------------------------------------------------------------

# TODO:
# Check condition number of FI matrix to know if U matrix entries are reliable

# This class is not very useful, particularly since  it tries to automatically 
# minimize the likelihood. This may not work well for high dimensional, 
# multi-modal likelihoods, i.e. when the theory predictions are non-linear in 
# the external parameters.
class FitterChiSquare(FitterBase):
    """
    Gaussian ChiSquare fitter. ChiSquare fit assuming Gaussian errors. 
    
    Attributes
    ----------
    chi2_min_val : float
        Value of chi^2 at minimum.
    chi2_min_pos : list
        Position of chi^2 minimum in the space of theory external parameters.
    LH : fitmaker.fitlib.likelihood.LikelihoodBase subclass
        Likelihood function.
    nobs : int
        Number of observables included in the dataset
    nparam : int 
        Number of external parameters in theory class, identified with number 
        of parameters to fit.
    ndof : int
        Number of degrees of freedom (`nobs`-`nparam`).
    
    """
    def __init__(
      self,
      arg_obsgroup=None,
      arg_theory=None,
      arg_theorykwargs=None,
      arg_chisquareLH=None,
      arg_symmetrise_method='average'
    ):
        """
        Parameters
        ----------
        args_obsgroup : fitmaker.fitlib.observables.ObsGroup, default=None
            `ObsGroup` instance containing fit data.
        args_theory: fitmaker.fitlib.theory.TheoryBase subclass, default=None
            Theory class to confront data with.
        arg_theorykwargs : dict , default=None
            Keyword arguments to pass to theory class constructor.
        arg_chisquareLH : fitmaker.fitlib.likelihood.ChiSquareLikelihood, default=None
            Chi^2 likelihood instance to feed directly, avoiding its construction 
            from input data, theory, errors etc.
        arg_symmetrise_method : str, default='average'
            Method to used to symmetrise errors. 
        
        """
        super(FitterChiSquare, self).__init__(arg_obsgroup, arg_theory, arg_chisquareLH)

        self.chi2_min_val = None
        self.chi2_min_pos = None

        #if likelihood not given then create likelihood from obsgroup and theory
        if arg_chisquareLH == None and arg_theory != None and arg_obsgroup != None:
            # Data inputs to the fit
            obs_name_vec, obs_val_vec, obs_err_vec, obs_corrMatrix = (
              self.obsgroup.get_obs_vectors(
                symmetrise_method=arg_symmetrise_method
              )
            )

            # Theory prediction            
            THprediction = BoundPrediction(
              arg_theory, 
              *obs_name_vec, 
              **arg_theorykwargs
            )

            # Construct chi^2 likelihood
            self.LH = ChiSquareLikelihood(
              THprediction, 
              np.array(obs_val_vec), 
              np.array(obs_err_vec), 
              np.array(obs_corrMatrix)
            )
            
            self.nobs = len(obs_val_vec)
            self.nparam = len(self.theory.external_parameters)
            self.ndof = self.nobs-self.nparam
    
    def chi2_min(self, start=None, **min_kwargs):
        """Minimize likelihood function.
        
        Runs `scipy.optimize.minimize` on likelihood function, assigning the 
        `chi2_min_val` and `chi2_min_pos` attributes. May not be 
        reliable for multi-dimensional, multi-modal likelihoods with several 
        local minima.
        
        Parameters
        ----------
        start : array-like, default=None
            Starting position for minimizer. Assumes the origin of an 
            `self.nparam`-dimensional space if not specified.
        min_kwargs : dict, optional
            Other keyword arguments to pass to `minimize`.
        
        Returns
        ----------
        chi2_min_val : float
            Value of likelihood at obtained minimum.
        chi2_min_pos : numpy.array
            Position of obtained minimum in theory parameter space.

        """
        chi2_min_func = lambda x: self.LH(*x)
        
        if start is None:
            start = np.zeros(self.nparam)
            
        chi2_min_sol = minimize(chi2_min_func, start)
        self.chi2_min_val = chi2_min_sol.fun
        self.chi2_min_pos = chi2_min_sol.x

        return (self.chi2_min_val, self.chi2_min_pos)

    def Delta_chi2(self, *THargs):
        """Delta chi^2 function.
        
        Returns the value of the chi^2 function at a point in parameter space 
        minus the value of the value at the minimum. In the Gaussian case, 
        isocontours can be interpreted as iso-probability contours containing 
        particular probabilities. Critical values are e.g. 3.84 for a 95% 
        confidence interval of a 1D parameter space or 5.99 for a 2D parameter 
        space.
        
        Parameters
        ----------
        THarg : array-like
            Value of theory external parameters.
        Returns
        ----------
        float
            Delta chi^2 value.

        """
        # minimize if not done already
        if self.chi2_min_val == None: 
            self.chi2_min()
        return self.LH(THargs) - self.chi2_min_val

#-------------------------------------------------------------------------------

class FitterAnalyticalChiSquare(FitterChiSquare):
    """
    Analytical Gaussian ChiSquare fitter using Hessian method.
    Assumes a linear parameter dependence in the theory predictions, as well as
    Gaussian errors.
    
    Attributes
    ----------
    LH : fitmaker.fitlib.likelihood.LikelihoodBase subclass
        Likelihood function.
    H : numpy.ndarray
        'H' matrix characterising the coefficients of the linear 
        dependence of each observable on the theory parameters.
    HT : numpy.ndarray
        Transpose of `H`.
    F : numpy.ndarray
        Fisher information matrix.
    U : numpy.ndarray
        Inverse of `F`, covariance matrix of theory parameters given the data.
    chi2_0 : float
        chi^2 value at origin in parameter space.
    chi2_min_val : float
        Value of chi^2 at minimum.
    chi2_min_pos : list
        Position of chi^2 minimum in the space of theory external parameters.
    W : np.array
        Part of the chi^2 likelihood linear in the theory parameters.
    bestfit : dict
        Global (marginalised/profiled) best-fit points for the theory 
        parameters.
    nobs : int
        Number of observables included in the dataset
    nparam : int 
        Number of external parameters in theory class, identified with number 
        of parameters to fit.
    ndof : int
        Number of degrees of freedom (`nobs`-`nparam`).
    chi2_ndof_0 : float
        Chi^2 per degrees of freedom at origin.
    chi2_ndof : float
        Chi^2 per degrees of freedom at best fit point.
    pull : float
        Pull of best-fit point with respect to the origin.

    """
    def __init__(
      self,
      arg_obsgroup=None,
      arg_theory=None,
      arg_theorykwargs=None,
      arg_chisquareLH=None,
      arg_symmetrise_method='average'
    ):
        """
        Parameters
        ----------
        args_obsgroup : fitmaker.fitlib.observables.ObsGroup, default=None
            `ObsGroup` instance containing fit data.
        args_theory: fitmaker.fitlib.theory.TheoryBase subclass, default=None
            Theory class to confront data with.
        arg_theorykwargs : dict , default=None
            Keyword arguments to pass to theory class constructor.
        arg_chisquareLH : fitmaker.fitlib.likelihood.ChiSquareLikelihood, default=None
            Chi^2 likelihood instance to feed directly, avoiding its construction 
            from input data, theory, errors etc.
        arg_symmetrise_method : str, default='average'
            Method to used to symmetrise errors. 
        
        Warns
        -----
        SingularFisherInformationWarning
            If the Fisher information, `F` is singular and cannot be inverted.
    
        """
        
        super(FitterAnalyticalChiSquare, self).__init__(
          arg_obsgroup, 
          arg_theory, 
          arg_theorykwargs, 
          arg_chisquareLH, 
          arg_symmetrise_method=arg_symmetrise_method
        )

        if arg_theory is not None and arg_obsgroup is not None:

            obsNameList, obsValList, obsErrList, obsCorrM = (
              self.obsgroup.get_obs_vectors(
                symmetrise_method=arg_symmetrise_method
              )
            )

            # theory prediction function for observables
            obsListPrediction = BoundPrediction(
              arg_theory, 
              *obsNameList, 
              **arg_theorykwargs
            )

            # compute H matrix of linear dependence of each coefficient for
            # each observable. For each theory coefficient of that observable, 
            # get the linear dependence (by setting all other coefficients to 
            # zero and the relevant one to 1)

            yexp = np.array(obsValList) # experimental data

            Vinv = self.LH.covariance_matrix_inv # inverse covariance matrix

            # parameter list of [0, 0, ..., 0]
            zeroparamlist = np.zeros(len(arg_theory.external_parameters))
            y0 = obsListPrediction(*zeroparamlist) # predictions at origin

            dy = yexp - y0

            # chi-squared at origin of parameter space
            self.chi2_0 = dy.dot(Vinv).dot(dy)
            # list of lists, with each list having one unit entry
            # note that to get linear dependence we also have to subtract the
            # SM value when all parameters are zero:
            unitparamlists = np.identity(len(self.theory.external_parameters))
            self.HT = np.array([
              obsListPrediction(*unitparamlist) - y0
              for unitparamlist in unitparamlists
            ])
            
            # ungodly hack for obsgroup with single observable:
            if self.HT.ndim == 1:
                self.HT= np.asarray([self.HT]).T
            self.H = self.HT.T

            HTVinv = self.HT.dot(Vinv)
            # Hessian matrix F = H^T*V^-1*H where V is the
            # covariance matrix of the experimental observable values
            # (otherwise known as Fisher information matrix)

            self.F = HTVinv.dot(self.H)

            if np.ndim(self.F)==0:
                # special 1D case
                self.U = 1./self.F
                self.chi2_min_pos = np.array([self.U*HTVinv*dy])
                self.chi2_min_val = (
                  self.chi2_0 - 
                  self.chi2_min_pos.dot(
                    np.array([[self.F]])
                  ).dot(
                      self.chi2_min_pos
                  )
                )
                self.W = np.array([HTVinv.dot(dy)])

            else:
                # covariance matrix for least squares estimators c_hat,
                # i.e. compute the U matrix (= inverse of Hessian matrix)
                try:
                    self.U = np.linalg.inv(self.F)
                    self.singular_fisher_information = False
                except LinAlgError as e:
                    warnings.warn(
                      'Fisher information matrix is singular. '
                      'No best-fit point or marginalised standard deviation. '
                      '"{}"'.format(e),
                      SingularFisherInformationWarning
                    )
                    self.singular_fisher_information = True
                    self.U = np.full(self.F.shape, np.nan)

                # best fit values c_hat of coefficients as a function of U,
                # H, covariance matrix of observed values V, vector of observed
                # values y, vector of SM predictions mu_SM
                self.chi2_min_pos = (self.U).dot(HTVinv).dot(dy)

                # chi-square minimum in terms of Fisher information and best fit
                self.chi2_min_val = (
                  self.chi2_0 - 
                  self.chi2_min_pos.dot(self.F).dot(self.chi2_min_pos)
                )

                # gradient vector about the minimum of chi-squared
                self.W = HTVinv.dot(dy)

            # dictionary of global best fit points
            self.bestfit = {
              k:v for k,v in 
              zip(self.theory.external_parameters, self.chi2_min_pos)
            }

            self.chi2_ndof_0 = self.chi2_0/self.nobs
            self.chi2_ndof = self.chi2_min_val/self.ndof
            self.pull = np.sqrt(np.abs(self.chi2_0 - self.chi2_min_val))

    def chisquare(self, *params, **keyparams):
        """
        Return value of chi-squared function from analytical expression in terms
        of chi2_0, best fit values, chi2_min_pos, & Fisher information, F.
        
        Parameters
        ----------
        params : tuple or list
            External parameter values of the theory class, in the order that 
            they are defined in its `external_parameters` attribute.
        keyparams : dict
            External parameter values of the theory class specified by keyword 
            argument.
        
        Returns
        -------
        float
            Value of chi^2 function.
        
        Raises
        ------
        RuntimeError
            If neither or both of params and keyparams are specified.
        ValueError
            If the length of `params` does not match the number of external 
            parameters defined in the theory class.
        
        """
        # check arguments
        if params and keyparams:
            msg = 'chisquared() function accepts either positional or keyword arguments, not both.'
            raise RuntimeError(msg)
        elif params:
            c = np.array(params)
        elif keyparams:
            c = np.array([ keyparams.get(k, 0.) for k in self.theory.external_parameters ])
        else:
            msg = 'chisquared() function called with no arguments'
            raise RuntimeError(msg)

        # check shape
        if len(c) != len(self.theory.external_parameters):
            msg = 'Number of arguments does not match number of external parameters in {}.'.format
            raise ValueError(msg(self.theory))

        c_hat = self.chi2_min_pos

        dc = c - c_hat

        return ( self.chi2_min_val + dc.dot( self.F ).dot( dc ) ).item()

    def get_index(self, *params):
        """Return indices of params in external_parameters of self.theory.
        
        Parameters
        ----------
        params : tuple or list
            Parameter names to return indices for.
        
        Returns
        -------
        list of int
        
        """
        return [ self.theory.external_parameters.index(p) for p in params ]

    def sub_Fisher(self, *params, **kwargs):
        """
        Returns the Fisher Information matrix for a subset of the parameters,
        params. By default assumes all other parameters are set to zero and the
        corresponding entries of the full Fisher information (FI) is used.
        The marginalise option makes use of the full FI, defining a new FI
        minimised over other directions in parameter space.
        
        Parameters
        ----------
        params : tuple or list
            Parameter names to return Fisher information matrix for.
        kwargs : dict, optional
            Additional options to specify. Currently supported:
            'marginalise' : bool, default=False
                Marginalise over unspecified parameter directions.
        
        Returns
        -------
        numpy.ndarray
            2D array ( len(`params`), len(`params`) ) representing Fisher 
            information in requested parameter space.
        
        Raises
        ------
        SingularFisherInformationError
            If global Fisher information is singular, marginalised information 
            cannot be obtained.
        
        """

        # special case for 1 parameter model
        if np.ndim(self.F)==0:
            return self.F

        iparams = self.get_index(*params)

        marg = kwargs.get('marginalise', False)

        if marg:
            # marginalised over others: inverse of sub-matrix of U
            if not self.singular_fisher_information:
                try:
                    return np.linalg.inv( self.U[ np.ix_(iparams, iparams) ])
                except LinAlgError:
                    raise SingularFisherInformationError(
                      'Cannot get marginalised sub-Fisher information. '
                      'Sub-matrix with indices {} of U is singular.'.format(iparams)
                    )
            else:
                raise SingularFisherInformationError(
                  'Cannot get marginalised sub-Fisher information. '
                  'Original Fisher information is singular.'
                )

        else:
            # set others to zero: sub-matrix of full Fisher information
            return self.F[ np.ix_(iparams, iparams) ]

    def covariance_matrix(self, *params, **kwargs):
        """
        Returns the covariance matrix for a subset of the parameters,
        params, obtained as the inverse of the Fisher Information (FI) matrix
        returned by `sub_Fisher()`.
        
        Parameters
        ----------
        params : tuple or list
            Parameter names to return covariance matrix for.
        kwargs : dict, optional
            Keyword argument to pass to `sub_Fisher()`.
        
        Returns
        -------
        numpy.ndarray
            2D array ( len(`params`), len(`params`) ) representing covariance 
            matrix in requested parameter space.
        
        """
        if not params:
            return self.U
        else:
            return np.linalg.inv( self.sub_Fisher(*params, **kwargs) )

    def correlation_matrix(self, *params, **kwargs):
        """
        Returns the correlation matrix for a subset of the parameters,
        params, obtained from the covariance matrix returned by
        covariance_matrix().
        
        Parameters
        ----------
        params : tuple or list
            Parameter names to return correlation matrix for.
        kwargs : dict, optional
            Keyword argument to pass to `sub_Fisher()`.
        
        Returns
        -------
        numpy.ndarray
            2D array ( len(`params`), len(`params`) ) representing correlation
            matrix in requested parameter space.
        
        """
        cov  = self.covariance_matrix(*params, **kwargs)
        sd = np.sqrt(np.diag(cov))

        return cov/np.outer(sd,sd)

    def standard_deviation(self, param, marginalise=False):
        """
        Return the standard deviation (square root of the inverse of the
        Fisher information) around the best fit value for parameter, param.
        
        Parameters
        ----------
        param : str
            Name of theory class external parameter.
        marginalise : bool, default=False
            Marginalise over other directions.
        
        Returns
        -------
        float
            Value of standard deviation, i.e., 1-sigma error on `param`. 
            Returns np.nan if `marginalise`=True and Fisher information is 
            singular.
        
        """
        iparam = self.theory.external_parameters.index(param)
        try:
            F_ii = self.sub_Fisher(param, marginalise=marginalise).item()
        except SingularFisherInformationError:
            F_ii = np.nan

        return 1./np.sqrt( F_ii )

    def get_bestfit(self, *params, **kwargs):
        """
        Return best fit point in parameter subspace specified by params argument.
        Behaviour with respect to other parameters of the model not contained in
        subspace is specified by the marginalise option.
        
        Parameters
        ----------
        params : tuple or list
            Parameter names to return Fisher information matrix for.
        kwargs : dict, optional
            Additional options to specify. Currently supported:
            'marginalise' : bool, default=False
                Marginalise over unspecified parameter directions.
        
        Returns
        -------
        numpy.ndarray
            Best fit points for parameters `params`, marginalised/profiled or 
            not depending on 'marginalise' option. Returns np.nan values if 
            `marginalise`=True and  Fisher information is singular.
        
        Warns
        -----
        SingularFisherInformationWarning
            If Fisher information is singular.
        
        """

        marg = kwargs.get('marginalise', False)

        if marg or np.ndim(self.F)==0:
            # marginalise: just return global best fit point
            return np.array([self.bestfit[p] for p in params])
        else:
            # set others to zero: get best fit point from new FI
            iparams = self.get_index(*params)
            F_b = self.sub_Fisher(*params, marginalise=False)
            w_b = np.array([self.W[i] for i in iparams])
            try:
                return w_b.dot( np.linalg.inv(F_b) )
            except LinAlgError as e:
                warnings.warn(
                  'Cannot get new bestfit point. '
                  'Sub-Fisher information for {} is singular.'
                  '"{}"'.format(params, e) ,
                  SingularFisherInformationWarning
                )
                return np.full(len(params), np.nan)

    def get_interval(self, param, **kwargs):
        """
        Return n-sigma confidence interval for parameter param.
        
        Parameters
        ----------
        param : str
            Parameter name to return interval for.
        kwargs : dict, optional
            Additional options to specify. Currently supported:
            'marginalise' : bool, default=False
                Marginalise over unspecified parameter directions.
            'nsigma' : int, default=2
                Number of standard deviations for return interval for.
        
        Returns
        -------
        tuple
            (lower, upper) interval edges.
        
        """
        num_sd = kwargs.get('sigma', 2)
        marg = kwargs.get('marginalise', False)
        sd = self.standard_deviation(param, marginalise=marg)
        central = self.get_bestfit(param, marginalise=marg).item()
        return central - num_sd*sd, central + num_sd*sd


    def get_chi2_min(self, *params, **kwargs):
        """Get the value of the minimum of the chi^2 function for the parameter 
        subspace specified by params argument, i.e., at the best-fit point.
        
        Parameters
        ----------
        param : str
            Parameter name to return interval for.
        kwargs : dict, optional
            Additional options to specify. Currently supported:
            'marginalise' : bool, default=False
                Marginalise over unspecified parameter directions.
        
        Returns
        -------
        float
            Chi^2 value at best-fit point.
        
        """
        if kwargs.get('marginalise', False) or np.ndim(self.F)==0:
            return self.chi2_min_val

        else:
            F_b = self.sub_Fisher(*params, marginalise=False)
            bestfit = self.get_bestfit(*params,  marginalise=False)
            return self.chi2_0 - bestfit.dot( F_b ).dot(bestfit)

    def chisquare_function(self, *params, **kwargs):
        """
        Return chisquare function in a parameter subspace specified by params.

        Function will take a number of arguments corresponding to the number 
        of parameters specified.
        
        Parameters
        ----------
        params : tuple or list
            Parameter names to return chisquared function for.
        kwargs : dict, optional
            Additional options to specify. Currently supported:
            'marginalise' : bool, default=True
                Marginalise over unspecified parameter directions.
            'delta_chisquare' : bool, default=False
                Return delta chi^2 function instead of absolute chi^2 function
        
        Returns
        -------
        function
            Chi^2 function for the parameter subspace.

        """
        doc = 'chi-squared function in ({}) space'.format(', '.join(str(params)))
        nargs = len(params)

        # get lower dimensional FI matrix, best-fit point & chi^2 minimum
        F_b = self.sub_Fisher(*params, **kwargs)
        bestfit = self.get_bestfit(*params, **kwargs)

        if kwargs.get('marginalise', True):
            doc = 'Marginalised ' + doc

        if kwargs.get('delta_chisquare', False):
            chisq_min = 0.
        else:
            chisq_min = self.get_chi2_min(*params, **kwargs)

        def chisquare(*vals):
            """{}""".format(doc)
            ngot = len(vals)
            if not ngot == nargs:
                msg = 'chisquare function expects {} arguments, got {}.'.format
                raise RuntimeError( msg(nargs, ngot) )

            db = np.array(vals) - bestfit

            return chisq_min + db.dot(F_b).dot(db)

        return chisquare

    def PCA(self, *params, tol=None, regularized=None):
        """
        Perform Principal Component Analysis (PCA) on Fisher information
        matrix entries corresponding to parameter (sub)space specified by params.
        If no params are specified, procedure defaults to all parameters of
        associated theory instance ordered according to its `external_parameters`
        attribute.
        
        Parameters
        ----------
        params : tuple or list
            Parameter names defining subspace over which to perform PCA.
        tol : float, default=None
            Precision to which to round entries of the FI. Number of digits is
            set by 'tol' times the minimum non-zero absolute value. Useful for 
            stabilising singular FI matrices when identifying flat directions.
        regularize : float, default=None
            Add a constant identity matrix times `regularized` to the Fisher 
            information. Useful for identifying blind directions as their 
            corresponding eigenvalues will scale proportionally to this 
            parameter.
        
        Returns
        -------
          fisher : numpy.ndarray
            2D array of Fisher Information matrix entries
          eig  : numpy.array
            1D array of eigenvalues of associated Fisher Information matrix.
          evec : numpy.ndarray
            2D array of whose rows are the corresponding eigenvectors with
            eigenvalues eig.
        
        """
        if not params:
            params = self.theory.external_parameters

        # get Fisher Information for parameter space
        fisher = self.sub_Fisher(*params, marginalise=False)
        
        if tol is not None:
            # round FI to a number of digits
            atol = np.amin( np.abs(fisher[np.nonzero(fisher)]) )*tol
            fisher = np.round( fisher, -int(np.floor(np.log10(atol))) )
        
        if regularized is not None:
            fisher += np.identity(len(params))*regularized
            
        # get eigenvalues & eigenvectors
        eig, evec = np.linalg.eigh(fisher)

        # sort in increasing order in absolute value
        isort = np.argsort(np.abs(eig))

        return fisher, eig[isort], (evec.T)[isort]


#-----------------------------------------------------------------------------------------------------
#Metropolis Hastings

class FitterMetropolisHastings(FitterBase):
    """
    Fitter for use with MultiNest. Constructs a 
    `fitmaker.fitlib.likelihood.ChiSquareLikelihood` for symmetric 
    errors and `fitmaker.fitlib.likelihood.AsymErrLikelihood` if not.

    
    Attributes
    ----------
    arg_theorykwargs : dict
        Keyword args to pass to theory class constructor.
    arg_symmErr : bool, default=True
        Whether to treat errors as symmetric.
    arg_symmetrise_method : str, default='average'
        Method to used to symmetrise errors.
    
    """
    def  __init__(
      self,
      arg_obsgroup=None,
      arg_theory=None,
      arg_theorykwargs=None,
      arg_chisquareLH=None,
      arg_symmErr=True,
      arg_symmetrise_method='average'
    ):
        """
        Parameters
        ----------
        args_obsgroup : fitmaker.fitlib.observables.ObsGroup, default=None
            `ObsGroup` instance containing fit data.
        args_theory: fitmaker.fitlib.theory.TheoryBase subclass, default=None
            Theory class to confront data with.
        arg_theorykwargs : dict , default=None
            Keyword arguments to pass to theory class constructor.
        arg_chisquareLH : fitmaker.fitlib.likelihood.ChiSquareLikelihood, default=None
            Chi^2 likelihood instance to feed directly, avoiding its construction 
            from input data, theory, errors etc.
        arg_symmErr : bool, default=True
            Whether to treat errors as symmetric.
        arg_symmetrise_method : str, default='average'
            Method to used to symmetrise errors.
    
        """
        super(FitterMetropolisHastings, self).__init__(arg_obsgroup, arg_theory, arg_chisquareLH)
        self.arg_theorykwargs = arg_theorykwargs
        self.arg_symmErr = arg_symmErr
        self.arg_symmetrise_method = arg_symmetrise_method

    def likelihood_func(self, *x):
        """
        If likelihood not given then create chi squared likelihood from 
        ObsGroup and theory.
        
        Parameters
        ----------
        x : tuple or list of floats
            Arguments for chi^2 function.
        
        Returns
        -------
        float
            Value of chi^2 function.
        
        """
        if self.LH == None and self.theory != None and self.obsgroup != None:
            obs_name_vec, obs_val_vec, obs_err_vec, obs_corrMatrix = self.obsgroup.get_obs_vectors(
              symmetrise_errors=self.arg_symmErr, 
              symmetrise_method=self.arg_symmetrise_method
            )
            THprediction = BoundPrediction(self.theory, *obs_name_vec,**self.arg_theorykwargs)
            if(self.arg_symmErr==True):
                return ChiSquareLikelihood(THprediction, np.array(obs_val_vec), np.array(obs_err_vec), np.array(obs_corrMatrix))(*x)

            else:
                return AsymmErrLikelihood(THprediction, np.array(obs_val_vec), np.array(obs_err_vec), np.array(obs_corrMatrix))(*x)

    def mcmc_multinest(
      self, 
      prior, 
      nlivepoints=400, 
      verb=False, 
      outputdir = 'samples_output_chains', 
      outputfile = "defaultout", 
      **kwargs
    ):
        """Multinest MCMC sampler - use this to sample efficiently from 
        multimodal likelihood.
    
    
        Parameters
        ----------
        prior : numpy.ndarray
            Ranges for flat priors assumed for theory parameters,
        outputdir : str, detault = 'samples_output_chains'
            Directory to store results of pymultinest.run()
        outputfile : str, default= "defaultout"
            Prefix for all outputfile. They will take the form 
            `outputdir`/`outputfile-XXXX.YYY`.
        
        Keyword options passed to pymultinest.run()
        -------------------------------------------
        nlivepoints : int, default=400
            Number of live points to feed to multinest run.
        verb : bool, default=False
            Verbose setting for multinest run.
        kwargs : dicts
            Any other arguments passed to pymultinest.run()
        
        Raises
        ------
        ImportError
            If pymultinest could not be imported.
    
        """
        if not _has_pymultinest:
            raise ImportError(
              'pymultinest could not be imported, function unavailable. '
              'Error at import attempt: {}'.format(_no_pymultinest_error)
            )

        print("Running multinest")

        #Make directory for outputs
        try: os.mkdir(outputdir)
        except OSError: pass
        prefix = f"{outputdir}/{outputfile}-"

        #print param names for multinest plotter
        parameters = self.theory.external_parameters
        nparam = np.size(parameters)

        print("multinest nparam is {0}".format(nparam))
        with open('%sparams.json' % prefix, 'w') as f:
            json.dump(parameters, f, indent=2)

        #Uniform prior in (-1,1)
        def myprior(cube, ndim, nparam):
            for i in range(nparam):
                cube[i] = cube[i]*(prior[i,1]-prior[i,0]) + prior[i,0]
            
        obs_name_vec, obs_val_vec, obs_err_vec, obs_corrMatrix = self.obsgroup.get_obs_vectors(symmetrise_errors=self.arg_symmErr, symmetrise_method=self.arg_symmetrise_method)

        THprediction = BoundPrediction(self.theory, *obs_name_vec,**self.arg_theorykwargs)

        if(self.arg_symmErr==True):
            chisquarefunc =  ChiSquareLikelihood(THprediction, np.array(obs_val_vec), np.array(obs_err_vec), np.array(obs_corrMatrix))

        else:
            print("Asymmetric errors not compatible with multinest yet!")
            print("using Gaussian likelihood with symmetrised errors")
            chisquarefunc =  ChiSquareLikelihood(THprediction, np.array(obs_val_vec), np.array(obs_err_vec), np.array(obs_corrMatrix))

        def loglike(cube, ndim, nparams, lnew):
        #Chi-squared log likelihood
            cubelist = [cube[i] for i in range(ndim)]
            chisquare = chisquarefunc(*cubelist)

            return -0.5*chisquare
        
        #Run multinest sampler
        result = pymultinest.run(
          LogLikelihood = loglike,
          Prior = myprior,
          n_dims = np.size(self.theory.external_parameters),
          outputfiles_basename = prefix,
          verbose = verb,
          sampling_efficiency = 0.8,
          n_live_points = nlivepoints,
          # evidence_tolerance = 0.01,
          const_efficiency_mode=False,
          resume=False,
          # importance_nested_sampling=False,
          importance_nested_sampling=True,
          **kwargs
        )
        return result

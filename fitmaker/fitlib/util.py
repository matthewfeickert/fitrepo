"""
``fitlib.util``
=====================

Collection of utility functions and decorators used in various fitmaker modules.

Functions
---------
chunks
duplicates
is_non_string_iterable

Decorators
----------
decorator_with_arguments

"""
################################################################################
def chunks(l, n):
    """Yield successive n-sized chunks from l.
    
    Parameters
    ----------
    l : iterable
        Object to yield successive chunks of.
    n : int
        Size of successive chunks.
    
    Yields
    ------
    iterable, same type as `l`
        `n`-sized chunk of `l`.
    
    """
    for i in xrange(0, len(l), n):
        yield l[i:i + n]

def duplicates(l):
    """Return list of duplicate elements in list l.
    
    Parameters
    ----------
    l : list
        List to check for duplicates in.

    Returns
    -------
    list
        Duplicate items in `l`.
    
    """
    seen = set()
    seen_add = seen.add
    # adds all elements it doesn't know yet to seen and all others to seen_twice
    seen_twice = set( x for x in l if x in seen or seen_add(x) )
    # turn the set into a list (as requested)
    return list( seen_twice )

import six
import collections
import sys
if ((sys.version_info[0] >= 3) and (sys.version_info[1] >= 10)):
    def is_non_string_iterable(arg):
        return (
            isinstance(arg, collections.abc.Iterable)
            and not isinstance(arg, six.string_types)
        )
else:
    def is_non_string_iterable(arg):
        return (
            isinstance(arg, collections.Iterable)
            and not isinstance(arg, six.string_types)
        )
is_non_string_iterable.__doc__ = """Checks if an object is an iterable but not a string.

Parameters
----------
arg
    object to check

Returns
-------
bool

"""

################################################################################
# useful decorators
def decorator_with_arguments(decorator):
    '''
    Decorator for a decorator allowing it to take arguments. Used for 
    `fitmaker.theory.internal_parameter` and `fitmaker.theory.prediction`.
    
    Parameters
    ----------
    decorator : callable
        Decorator that you would like to allow to take arguments.
        
    '''
    def layer(*args, **kwargs):
        def repl(f):
            return decorator(f, *args, **kwargs)
        return repl

    return layer
    
